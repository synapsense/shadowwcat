Care and Feeding instructions for the shadowwcat project
--------------------------------------------------------

Shadoww CAT is the Shadoww appliance cluster administration tool. It is a standalone, Windows only desktop application,
implemented in .NET 3.51, C#, and Winforms.

Development environment: Visual Studio (VS) 2012 or higher. Free editions of VS should work (tried once, encountered no problems).

Setup and dev use
-----------------
1. Install VS 2013 (2012 will also work with no project file conversions). Choose "C# Development" from the list presented the first time you launch VS.

2. In Tools->Options->Environment-Keyboard, optionally choose "Visual C# 2005" keyboard mapping (Used to
specify the 'Handy' commands in this document).

3. Other options I found useful:
  Tools->Options->Text Editor->C# -- enable line numbers.
  
Outside of this, I made no other customizations to the OOTB VS install.

To launch a development session, either double click shadowwcat.sln or select it from within VS.

  Handy commands
    F6 - build solution
    F5 - launch debugger
	F9 - set breakpoint
	F10 - step over
	F11 - step into
	
	F12 - 'go to definition' (place cursor anywhere within a symbol first)
	F7 - open code file for a form.
	Shift+F7 - open form in design mode.

The shadowwcat source project is available from git://git.synapsense.int/shadowwcat.git.
	
Code Taxonomy
-------------
  shadowwcat (solution)
    |
	shadowwcat (project)
	|
	IPAddressControlLib (3rd party project)
	|
	Renci.SshNet (3rd party project)
	|
	Renci.SshNet.NET35 (3rd party project)
	|
	tar-cs (3rd party project)
	
  The solution consists of five projects. Four of these are OSS libraries, included in their entirety and unmodified. See Readme.txt for
  download address and licensing information.
  
  The projects are:
    - IPAddressControlLib - A Winforms "user control" (a custom control containing GUI and code logic components). It is utilized on
	  any form where IP address input is needed.
	
	- Renci.SshNet.NET35, Renci.SshNet - A set of class libraries that implement SSH, SCP, and who knows what else. We're using it for the first two.
	  **Note: The second project - Renci.SshNet - is present in the solution parent directory but not loaded as part of the solution. It
	  is needed only to satisfy source code dependencies between the two Renci projects. It is NOT built as part of the solution.
	  
	- tar-cs - A class library offering Tar file format reading and writing. Used to extract version information from appliance firmware
	  packages.
	
	- shadowwcat - Comprised of all of the code written locally. Discussed in more detail below.
	
  The shadowwcat project (as opposed to the shadowwcat solution) is the "anchor" project -- dependent on all other projects.
  Build output resides in this project's 'bin\Debug' or 'bin\Release' subdirectory (depending on the build configuration selected). Note
  that 3rd party project build output is automatically copied to this build location (See VS Project References to understand how this
  works).
  
  Notable files:
  
  shadowwcat->Properties:
    AssemblyInfo.cs - Shadoww Cat's version is created based on settings in this file. See comments in file for more information.
  
  
  Other files:
    Two text files - a license agreement (license.rtf), and third party product info (Readme.txt) - reside in the top level solution directory.
	Both are copied to the build output directory whenever a build occurs (via a post-build step in the shadowwcat project).
	
	
Running, Debugging, Troubleshooting
-----------------------------------

All files created at runtime reside in the "application global common data" folder. On Windows 7 this is C:\ProgramData\SynapSense Corporation\ShadowwCat\1.0.
The subdirectories under C:\ProgramData are all deduced from settings in AssemblyInfo.cs. The log file and cluster config files reside here.

A single registry entry is created whenever a "Full platform upgrade" is performed -
[HKEY_LOCAL_MACHINE\SOFTWARE\SynapSense Corporation\ShadowwCat\1.0]ResumeAfterUpgrade. Its value set is '0' or '1'.

FAKEES environment variable. If set to '1', tells Shadoww Cat it is running on an ES. Set to '0' to force non-ES mode.

shadowwcat.log is a good place to look for error information. In particular, if a remote command returns an error, the exact command line that was issued is
recorded in the log (along with all command return output). It is useful to manually reinvoke a command directly on an appliance (via an ssh client login),
using say, sh -x to obtain more information about where a problem is occurring.

To build from the command-line, use the provided ant build. Type "ant -p" to see the available targets:

Main targets:

 build-debug    Build debug configuration.
 build-release  Build release configuration; create distribution zip.
 clean          Delete build output.
Default target: build-debug