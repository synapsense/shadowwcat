﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Management;
using Microsoft.Win32;
using System.IO;
using System.Text.RegularExpressions;
using shadowwcat.Utils;

namespace shadowwcat.Data
{
    /// <summary>
    /// Network information retriever for the current system. Most of the values obtained here are used to
    /// prepopulate form controls for cluster configuration.
    /// This class also performs a handul of Environment Server specific routines - better suited to a separate class, perhaps, but all
    /// interaction between Shadoww Cat and the ES is contained here.
    /// </summary>
    class ESHostInfo
    {
        internal struct NETWORK_INFO
        {
            internal string macAddress;
            internal string ipAddress;
            internal string netmask;
            internal string defaultGateway;
            internal bool isDHCPEnabled;
            internal string dhcpServerIP;
            internal string[] dnsServerIPs;

        }

        internal class NetworkInfoDictionary : Dictionary<string, NETWORK_INFO> { };

        private bool isPartOfDomain = false;
        private string shortHostName;
        private string domainName = "";
        private NetworkInfoDictionary networkInfo = new NetworkInfoDictionary();

        internal struct HOST_IP
        {
            internal string name;
            internal string ip;
        };

        private HOST_IP[] ntpServers;

        internal ESHostInfo()
        {
            RetrieveNetworkInfo();
            RetrieveTimeServerInfo();
        }

        internal string FullyQualifiedHostName
        {
            get {
                if (domainName.Length > 0)
                {
                    return string.Format("{0}.{1}", shortHostName, domainName);
                }
                return string.Format("{0}", shortHostName);
            }
        }
        internal string ShortHostName
        {
            get { return shortHostName; }
            set { shortHostName = value; }
        }
        internal string DomainName
        {
            get { return domainName; }
            set { domainName = ""; }
        }
        internal bool IsES
        {
            get { return IsEnvironmentalServer(); }
        }
        internal bool IsResumeAfterUpgrade
        {
            get { return isResumeAfterUpgrade(); }
            set { setResumeAfterUpgrade(value); }
        }
        internal string EnvironmentalServerVersion
        {
            get { return environmentalServerVersion(); }
        }
        internal NetworkInfoDictionary NetworkInfo
        {
            get { return networkInfo; }
        }
        internal HOST_IP[] NTPServers
        {
            get { return ntpServers; }
        }
        internal string[] NTPHostNames
        {
            get {
                List<string> lst = new List<string>();
                foreach (HOST_IP info in ntpServers)
                {
                    lst.Add(info.name);
                }
                return lst.ToArray();
            } 
        }
        internal string[] NTPIPAddresses
        {
            get
            {
                List<string> lst = new List<string>();
                foreach (HOST_IP info in ntpServers)
                {
                    lst.Add(info.ip);
                }
                return lst.ToArray();
            }
        }

        private void RetrieveTimeServerInfo()
        {
            System.Diagnostics.Process p;
            p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "w32tm";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;

            p.StartInfo.Arguments = " /query /source";
            p.Start();
            p.WaitForExit();

            string serverStr = p.StandardOutput.ReadLine();

            if (serverStr == null || serverStr.Contains("The following error"))
            {
                const string Rootkey = @"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\W32Time";
                serverStr = (string)Registry.GetValue(Rootkey + @"\Parameters", "NtpServer", String.Empty);
            }

            string[] servers = serverStr.Split(',');
            List<HOST_IP> listServers = new List<HOST_IP>();
            foreach (string server in servers)
            {
                HOST_IP info = NSLookup(server);
                if (info.name != null)
                {
                    listServers.Add(info);
                }
            }
            ntpServers = listServers.ToArray();
        }

        public static HOST_IP NSLookup(string name)
        {
            HOST_IP server = new HOST_IP();

            System.Diagnostics.Process p;
            p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "nslookup";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;

            p.StartInfo.Arguments = " " + name;
            p.Start();
            p.WaitForExit();

            Logger.Log(Logger.Levels.Info, "'nslookup " + name + "' returned ");
            string output = p.StandardOutput.ReadLine();
            Logger.Log(Logger.Levels.Info, output);
            int index;
            while ((output != null) && (index = output.IndexOf("Name")) == -1)
            {
                output = p.StandardOutput.ReadLine();
                Logger.Log(Logger.Levels.Info, output);
            }

            if (output == null)
            {
                return server;
            }

            // At host name line
            index = output.IndexOf(":");
            if (index != -1)
            {
                server.name = output.Substring(index + 1).Trim();
            }

            while ((index = output.IndexOf("Address")) == -1)
            {
                output = p.StandardOutput.ReadLine();
                Logger.Log(Logger.Levels.Info, output);
            }

            // At ip address line
            index = output.IndexOf(":");
            if (index != -1)
            {
                server.ip = output.Substring(index + 1).Trim();
            }

            return server;
        }

        private void logAllWMIObjectInfo(ManagementObject mo)
        {
            foreach (PropertyData pD in mo.Properties)
            {
                this.logWMIProperty(pD.Name, pD.Value);
            }
        }

        private void logWMIProperty(string paramName, Object param)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("    " + paramName + ": ");
            if (param != null)
            {
                if (param is string[])
                {
                    string[] arr = (string[])param;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (i > 0)
                        {
                            sb.Append(", ");
                        }
                        sb.Append(arr[i]);
                    }
                } else
                {
                    sb.Append(param.ToString());
                }
            } else
            {
                sb.Append("NULL");
            }
            Logger.Log(Logger.Levels.Info, sb.ToString());
        }

        private void RetrieveNetworkInfo()
        {
            Logger.Log(Logger.Levels.Info, "==== RETRIEVING NETWORK INFORMATION ====");

            string interfaceIndex;
            string macAddress;
            string WMIPath = @"\\" + Dns.GetHostName() + @"\root\cimv2";
            ConnectionOptions WMIOptions = new ConnectionOptions();
            ManagementScope WMIScope = new ManagementScope(WMIPath, WMIOptions);
            ObjectQuery WMIQuery = new ObjectQuery();
            ManagementObjectSearcher WMISearcher = new ManagementObjectSearcher();

            try
            {
                WMIScope.Connect();

                // Get some basic network identity info.
                WMIQuery.QueryString = "SELECT * FROM Win32_ComputerSystem";
                WMISearcher.Scope = WMIScope;
                WMISearcher.Query = WMIQuery;
                ManagementObjectCollection queryCollection = WMISearcher.Get();
                foreach (ManagementObject m in queryCollection)
                {
                    shortHostName = m["DNSHostName"].ToString();
                    isPartOfDomain = (bool)m["PartOfDomain"];
                    if (isPartOfDomain)
                    {
                        domainName = m["Domain"].ToString();
                    }

                    Logger.Log(Logger.Levels.Info, "Found ComputerSystem");
                    //this.logAllWMIObjectInfo(m);
                    this.logWMIProperty("DNSHostName", m["DNSHostName"]);
                    this.logWMIProperty("Domain", m["Domain"]);
                    this.logWMIProperty("PartOfDomain", m["PartOfDomain"]);
                }

                // Find the InterfaceIndex for the default adapter.
                WMIQuery.QueryString = @"Select * from Win32_IP4RouteTable WHERE Destination='0.0.0.0'";
                WMISearcher.Query = WMIQuery;
                ManagementObjectCollection routeTables = WMISearcher.Get();
                foreach (ManagementObject rT in routeTables)
                {
                    Logger.Log(Logger.Levels.Info, "Found IP4RouteTable");
                    this.logWMIProperty("InterfaceIndex", rT["InterfaceIndex"]);
                    //this.logAllWMIObjectInfo(rT);

                    if (rT["InterfaceIndex"] != null)
                    {
                        interfaceIndex = rT["InterfaceIndex"].ToString();
                        // Based on InterfaceIndex, retrieve the corresponding MAC addr.
                        WMIQuery.QueryString = string.Format("Select * from Win32_NetworkAdapter WHERE InterfaceIndex = '{0}'", interfaceIndex);
                        WMISearcher.Query = WMIQuery;
                        ManagementObjectCollection networkAdapters = WMISearcher.Get();
                        foreach (ManagementObject nA in networkAdapters)
                        {
                            Logger.Log(Logger.Levels.Info, "Found NetworkAdapter");
                            this.logWMIProperty("MACAddress", nA["MACAddress"]);
                            //this.logAllWMIObjectInfo(nA);

                            if (nA["MACAddress"] != null)
                            {
                                macAddress = nA["MACAddress"].ToString();
                                // Get the configuration values for the default network adapter via MAC addr.
                                WMIQuery.QueryString = string.Format("Select * from Win32_NetworkAdapterConfiguration where MACAddress = '{0}'", macAddress);
                                WMISearcher.Query = WMIQuery;
                                ManagementObjectCollection networkConfigs = WMISearcher.Get();
                                foreach (ManagementObject n in networkConfigs)
                                {
                                    Logger.Log(Logger.Levels.Info, "Found NetworkAdapterConfiguration");
                                    //this.logAllWMIObjectInfo(n);
                                    this.logWMIProperty("IPAddress", n["IPAddress"]);
                                    this.logWMIProperty("IPSubnet", n["IPSubnet"]);
                                    this.logWMIProperty("DefaultIPGateway", n["DefaultIPGateway"]);
                                    this.logWMIProperty("DNSServerSearchOrder", n["DNSServerSearchOrder"]);
                                    this.logWMIProperty("DNSDomainSuffixSearchOrder", n["DNSDomainSuffixSearchOrder"]);
                                    this.logWMIProperty("DHCPEnabled", n["DHCPEnabled"]);
                                    this.logWMIProperty("DHCPServer", n["DHCPServer"]);

                                    if (n["IPAddress"] != null &&
                                        n["IPSubnet"] != null &&
                                        n["DefaultIPGateway"] != null)
                                    {
                                        string[] ipNetmasks = (string[])n["IPSubnet"];
                                        string[] ipAddresses = (string[])n["IPAddress"];
                                        bool isDHCPEnabled = (n["DHCPEnabled"] != null) ? (bool)n["DHCPEnabled"] : false;
                                        string dhcpServerIP = isDHCPEnabled ? (string)n["DHCPServer"] : null;
                                        string[] dnsServerIPs = (string[])n["DNSServerSearchOrder"];
                                        string[] defaultGatewayIPs = (string[])n["DefaultIPGateway"];

                                        if (!isPartOfDomain)
                                        {
                                            Object param = n["DNSDomainSuffixSearchOrder"];
                                            if (param is string[])
                                            {
                                                if (((string[]) param).Length > 0)
                                                {
                                                    domainName = ((string[])param)[0];
                                                }
                                            }
                                            else if (param is string)
                                            {
                                                domainName = param.ToString();
                                            }
                                        }

                                        string pattern = @"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}";
                                        Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                                        int count = ipAddresses.Length;
                                        foreach (string ip in ipAddresses)
                                        {
                                            MatchCollection matches = rgx.Matches(ip);
                                            if (matches.Count > 0)
                                            {
                                                NETWORK_INFO ni = new NETWORK_INFO();
                                                ni.macAddress = macAddress;
                                                ni.ipAddress = ip;
                                                ni.isDHCPEnabled = isDHCPEnabled;
                                                ni.dhcpServerIP = dhcpServerIP;
                                                ni.dnsServerIPs = dnsServerIPs;

                                                foreach (string netmask in ipNetmasks)
                                                {
                                                    matches = rgx.Matches(netmask);
                                                    if (matches.Count > 0)
                                                    {
                                                        ni.netmask = netmask;
                                                        break;
                                                    }
                                                }

                                                foreach (string gateway in defaultGatewayIPs)
                                                {
                                                    matches = rgx.Matches(gateway);
                                                    if (matches.Count > 0)
                                                    {
                                                        ni.defaultGateway = gateway;
                                                        break;
                                                    }
                                                }

                                                if (!string.IsNullOrEmpty(ni.netmask) && !string.IsNullOrEmpty(ni.defaultGateway))
                                                {
                                                    if (!networkInfo.ContainsKey(ni.ipAddress))
                                                    {
                                                        Logger.Log(Logger.Levels.Info, string.Format("Adding IPAddress {0} to NetworkInfo", ni.ipAddress));
                                                        networkInfo.Add(ni.ipAddress, ni);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                Logger.Log(Logger.Levels.Info, "==== ERROR RETRIEVING NETWORK INFORMATION ====");
                throw new NotSupportedException(string.Format("Could not retrieve host network information. Reason: {0}", e.Message));
            }

            if (networkInfo.Count == 0)
            {
                Logger.Log(Logger.Levels.Info, "==== ERROR RETRIEVING NETWORK INFORMATION ====");
                throw new NotSupportedException("Could not retrieve host network information. Reason: Could not find valid network adapter.");
            }

            Logger.Log(Logger.Levels.Info, "==== SUCCESSFULLY RETRIEVED NETWORK INFORMATION ====");
        }

        /// <summary>
        /// Detects if ES is present on system. This is fragile logic, but I could not think of a better way to check. If the name of this
        /// service changes in ES, we're hosed.
        /// </summary>
        /// <returns></returns>
        private bool IsEnvironmentalServer()
        {

#if DEBUG
            return (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\SSES-MASTER", "ImagePath", null) != null);
#else
            return (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\SYNAPSERVER", "ImagePath", null) != null);
#endif
        }
        private string environmentalServerVersion()
        {
            string version = string.Empty;

            if (IsEnvironmentalServer())
            {
                // Note that this will return 6.9.9 for all pre-7.0 versions of ES because this registry item was introduced in 7.0. This is what we want.
                version = Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\SynapSense Corporation\SynapSoft", "ProductVersion", "6.9.9") as string;
            }
            return (version);
        }
        private void setResumeAfterUpgrade(bool value)
        {
            Registry.SetValue(AppSession.CommonDataRegistry, "ResumeAfterUpgrade", (value ? "1" : "0"));
        }
        private bool isResumeAfterUpgrade()
        {
            string value = Registry.GetValue(AppSession.CommonDataRegistry, "ResumeAfterUpgrade", "0") as string;

            return (value == "1");
        }
        internal string Dump()
        {
            string s;
            s = string.Format("ShortHostName = {0}\n", ShortHostName);
            s += string.Format("DomainName = {0}\n", DomainName);
            s += string.Format("FullyQualifiedHostName = {0}\n", FullyQualifiedHostName);
            s += string.Format("Is an ES = {0}\n", IsES);
            s += string.Format("Is part of domain = {0}\n", isPartOfDomain);
            s += string.Format("NTP HostName = {0}\n", NTPHostNames);
            s += string.Format("NTP IP Addresses = {0}\n", NTPIPAddresses);

            foreach (NETWORK_INFO ni in NetworkInfo.Values)
            {
                s += string.Format("IP Address = {0}\n", ni.ipAddress);
                s += string.Format("Netmask = {0}\n", ni.netmask);
                s += string.Format("Default gateway = {0}\n", ni.defaultGateway);
                s += string.Format("Is DHCP enabled = {0}\n", ni.isDHCPEnabled);
                s += string.Format("DHCP Server = {0}\n", ni.dhcpServerIP);
                s += string.Format("DNS Server = {0}\n", ni.dnsServerIPs);
            }

            return s;
        }
    }
}
