﻿using shadowwcat.ApplianceOps;
using shadowwcat.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shadowwcat.Data
{
    class CommandRunnerInfo
    {
        internal Command ctrl1 { get; set; }
        internal Command ctrl2 { get; set; }
        internal List<CommandInfo> commands { get; set; }

        internal string taskName { get; set; }
        internal string taskDetails { get; set; }
        internal string startButtonName { get; set; }
        internal string customSuccessInstructions { get; set; }
        internal string customErrorInstructions { get; set; }

        public CommandRunnerInfo() : this(new Command(), new Command())
        {
        }

        public CommandRunnerInfo(Command c1, Command c2)
        {
            ctrl1 = c1;
            ctrl2 = c2;

            // Default to these values since they are usually what is needed
            ctrl1.ApplianceHostname = AppSession.ClusterConfig.Appliance_1_Hostname;
            ctrl1.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_1_Port1_IP;
            ctrl1.AppliancePassword = AppSession.Appliance1Password;
            ctrl1.FirmwareImageFilename = AppSession.ApplianceFirmwareFile;
            ctrl1.SiblingHostname = AppSession.ClusterConfig.Appliance_2_Hostname;

            // Default to these values since they are usually what is needed
            ctrl2.ApplianceHostname = AppSession.ClusterConfig.Appliance_2_Hostname;
            ctrl2.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_2_Port1_IP;
            ctrl2.AppliancePassword = AppSession.Appliance2Password;
            ctrl2.FirmwareImageFilename = AppSession.ApplianceFirmwareFile;
            ctrl2.SiblingHostname = AppSession.ClusterConfig.Appliance_1_Hostname;

            commands = new List<CommandInfo>();
        }

        internal void AddCommand(CommandInfo ci)
        {
            commands.Add(ci);
        }

    }
}
