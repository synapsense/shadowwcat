﻿using shadowwcat.ApplianceOps;
using shadowwcat.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shadowwcat.Data
{
    /// <summary>
    /// This is the sole owner of Shadoww Cat application state. It is used to communicate data and state between objects - in particular, dialogs.
    /// 
    /// Application data is composed of three main categories:
    /// 1. Application() settings.
    /// 2. An instance of ClusterConfig().
    /// 3. An instance of ESHostInfo().
    /// 
    /// All setters and getters of properties that cross object boundaries should use AppSession.
    /// 
    /// All users of ClusterConfig() and ESHostInfo() methods should use AppSession. (Note: ClusterConfig uses a private
    /// instance of ESHostInfo to obtain a few default host network values).
    /// </summary>
    internal static class AppSession
    {
        private static ESHostInfo hostInfo;
        private static ClusterConfig clusterConfig;
        private static string commonDataPath; //location of cluster config file, log files, etc..
        private static Microsoft.Win32.RegistryKey commonDataRegistry;

        internal struct UpgradeInfo
        {
            internal string ApplianceHostName;
            internal string RunningVersion;
            internal string UpgradeToVersion;
            internal bool IsRestorationTarget;
        }

        internal enum VERIFY_TYPE
        {
            FIRMWARE_ONLY_UPGRADE = 0,
            FULL_UPGRADE,
            RESUME_FULL_UPGRADE,
            RESTORE_CLUSTER
        }

        internal static CommandRunnerInfo commandRunnerInfo { get; set; }

        internal static UpgradeInfo c1UpgradeInfo;
        internal static UpgradeInfo c2UpgradeInfo;

        internal static void Init(string CommonDataPath, Microsoft.Win32.RegistryKey CommonDataRegistry)
        {
            commonDataPath = CommonDataPath;
            commonDataRegistry = CommonDataRegistry;

            hostInfo = new ESHostInfo();
            clusterConfig = new ClusterConfig(commonDataPath, hostInfo);
        }
        internal static bool IsEnvironmentalServer
        {
            get { return hostInfo.IsES; }
        }
        internal static string EnvironmentalServerVersion
        {
            get { return hostInfo.EnvironmentalServerVersion; }
        }
        internal static bool IsResumeAfterUpgrade
        {
            get { return hostInfo.IsResumeAfterUpgrade; }
            set { hostInfo.IsResumeAfterUpgrade = value; }
        }
        /// <summary>
        /// Did user select Single or Bridged network configuration?
        /// </summary>
        internal static bool IsConfigureClusterBridged { get; set; }
        internal static bool UseDNS { get; set; }
        internal static bool AppliancesHaveSamePassword { get; set; }
        internal static bool RememberAppliancePasswordForSession { get; set; }

        internal static ClusterConfig ClusterConfig
        {
            get { return clusterConfig; }
        }
        internal static string ShadowwCatVersion { get; set; }

        internal static string CommonDataPath
        {
            get { return commonDataPath; }
        }
        internal static string CommonDataRegistry
        {
            get { return commonDataRegistry.ToString(); }
        }
        internal static string Appliance1Password { get; set; }
        internal static string Appliance2Password { get; set; }
        internal static string ApplianceFirmwareFile { get; set; }
        internal static VERIFY_TYPE verifyType { get; set; }

        internal struct ChangePasswordInfo
        {
            internal string hostName;
            internal string IPAddress;
            internal string oldPassword;
            internal string newPassword;
        }

        internal static ChangePasswordInfo changePasswordInfo;
    }
}
