﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shadowwcat.Data
{
    /// <summary>
    /// The canonical set of config items that comprise a control cluster config file.
    /// DO NOT ALTER THESE NAMES. The enum member names map directly to the key names written to the
    /// cluster config file.
    /// </summary>
    internal enum ClusterConfigItem
    {
        es_hostname,             //Always overriden by system.
        es_ip,                   //Always overriden by system.
        timeserver,              //Use value if set in file, otherwise use system's.
        nameserver,              //Use value if set in file, otherwise use system's.
        net1_dhcp,
        net1_netmask,            //Use value if set in file, otherwise use system's.
        net1_gateway,            //Use value if set in file, otherwise use system's.
        net2_enabled,
        net2_dhcp,
        net2_netmask,            //Use value if set in file, otherwise use system's.
        appliance_1_hostname,
        appliance_1_port1_ip,
        appliance_1_port2_ip,
        appliance_2_hostname,
        appliance_2_port1_ip,
        appliance_2_port2_ip
    };

    interface IConfigItem
    {
        dynamic Value { get; set; }
        string UserFriendlyString { get; }
        bool AllowDefault { get; set; }

        string SaveToFile();
        void ReadFromFile(string fileValue);
        bool IsValidFormat();
        bool IsValidValue(string[] cmp);
        bool IsDefault();
    }


}
