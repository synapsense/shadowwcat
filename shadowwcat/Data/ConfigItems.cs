﻿using shadowwcat.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shadowwcat.Data
{
    class ConfigItem<T>
    {
        private T configSetting;
        private string userFriendlyString = "";
        protected bool runValidation = true;
        protected T defaultVal = default(T);
        private bool defaultAllowed = false;

        protected ConfigItem(T defaultValue, string userString, bool runValidation, bool defaultAllowed)
        {
            Default = defaultValue;
            Value = defaultValue;
            this.userFriendlyString = userString;
            this.runValidation = runValidation;
            this.defaultAllowed = defaultAllowed;
        }

        public dynamic Value
        {
            get { return this.configSetting; }
            set {
                if (IsValidFormat())
                    this.configSetting = value;
                else
                    throw new InvalidOperationException("Invalid format for configuration setting");
            }
        }

        public string UserFriendlyString
        {
            get { return this.userFriendlyString; }
        }

        public bool AllowDefault
        {
            get { return this.defaultAllowed; }
            set { this.defaultAllowed = value; }
        }

        protected T Default
        {
            get { return this.defaultVal; }
            set { this.defaultVal = value; }
        }

        public bool IsValidFormat()
        {
            return true;
        }

        protected bool ValidateFormat()
        {
            if (IsDefault())
            {
                if (!this.defaultAllowed)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsDefault()
        {
            return this.configSetting.Equals(this.Default);
        }
    }

    class HostNameArrayConfigItem : StringArrayConfigItem
    {
        public HostNameArrayConfigItem(string userString, bool runValidation, bool defaultAllowed) :
            base(userString, runValidation, defaultAllowed)
        {
        }

        new public bool IsValidFormat()
        {
            bool valid = base.IsValidFormat();
            if (valid)
            {
                foreach (String name in Value)
                {
                    if (!Utilities.VerifyIPorHostName(name))
                    {
                        valid = false;
                        break;
                    }
                }
            }

            return valid;
        }

        new public bool IsValidValue(string[] cmp)
        {
            if (!runValidation || IsDefault())
            {
                return true;
            }
            else
            {
                return base.IsValidValue(cmp);
            }
        }

    }

    class HostNameConfigItem : StringConfigItem
    {
        public HostNameConfigItem(string userString, bool runValidation, bool defaultAllowed) :
            base(userString, runValidation, defaultAllowed)
        {
        }

        new public bool IsValidFormat()
        {
            return base.IsValidFormat() && Utilities.VerifyIPorHostName(Value);
        }
    }

    class IPAddressConfigItem : StringConfigItem
    {
        public IPAddressConfigItem(string userString, bool runValidation, bool defaultAllowed) :
            base(userString, runValidation, defaultAllowed)
        {
        }

        new public bool IsValidFormat()
        {
            return base.IsValidFormat() && Utilities.VerifyIPAddress(Value);
        }
    }

    class StringConfigItem : ConfigItem<string>, IConfigItem
    {
        public StringConfigItem(string userString, bool runValidation, bool defaultAllowed) :
            base(default(string), userString, runValidation, defaultAllowed)
        {
        }

        new public bool IsValidFormat()
        {
            return base.IsValidFormat();
        }

        public bool IsValidValue(string[] cmp)
        {
            if (!runValidation || IsDefault())
            {
                return true;
            }
            else
            {
                return (cmp != null) ? cmp.Contains((string)Value) : true;
            }
        }

        public void ReadFromFile(string fileValue)
        {
            Value = fileValue;
        }

        public string SaveToFile()
        {
            return Value;
        }
    }

    class StringArrayConfigItem : ConfigItem<string[]>, IConfigItem
    {
        public StringArrayConfigItem(string userString, bool runValidation, bool defaultAllowed) :
            base(new string[] { }, userString, runValidation, defaultAllowed)
        {
        }

        new public bool IsValidFormat()
        {
            bool valid = base.IsValidFormat();
            if (valid)
            {
                foreach (String name in Value)
                {
                    if (IsDefault())
                    {
                        if (!AllowDefault)
                        {
                            valid = false;
                            break;
                        }
                    }
                }
            }

            return valid;
        }

        public bool IsValidValue(string[] cmp)
        {
            if (!runValidation || IsDefault())
            {
                return true;
            }
            else
            {
                if (cmp != null)
                {
                    // if config item is not null, need to verify it
                    bool valid = true;
                    foreach (string str in Value)
                    {
                        if (!cmp.Contains(str))
                        {
                            valid = false;
                        }
                    }

                    return valid;
                }
                else
                {
                    return true;
                }
            }
        }

        public void ReadFromFile(string fileValue)
        {
            if (fileValue.Trim().Length != 0)
            {
                Value = fileValue.Split(',');
            }
        }

        public string SaveToFile()
        {
            return string.Join(",", Value);
        }
    }

    class BooleanConfigItem : ConfigItem<bool>, IConfigItem
    {
        public BooleanConfigItem(bool value, string userString) :
            base(value, userString, false, true)
        {
        }

        new protected bool IsValidFormat()
        {
            return true;
        }

        public bool IsValidValue(string[] cmp)
        {
            return true;
        }

        public string SaveToFile()
        {
            return (Value ? "1" : "0");
        }

        public void ReadFromFile(string fileValue)
        {
            Value = Convert.ToBoolean(Convert.ToInt32(fileValue));
        }
    }
}