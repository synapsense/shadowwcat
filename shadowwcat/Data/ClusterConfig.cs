﻿using Microsoft.Win32;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace shadowwcat.Data
{
    /// <summary>
    /// Creates and maintains the cluster configuration file (by convention called cluster_config.rc), passed to each control
    /// appliance during the configuration process.
    /// 
    /// There are two flavors of config file:
    /// - Last successful - persists the last good cluster configuration operation performed.
    /// - Provisional - persists a cluster config that has not yet been deployed in a successful configuration operation.
    /// 
    /// </summary>

    internal class Configuration
    {
        private bool validated;
        private IConfigItem cfgItem;

        internal Configuration(IConfigItem item)
        {
            this.validated = false;
            this.cfgItem = item;
        }

        internal bool Validated
        {
            get { return validated; }
            set { validated = value; }
        }

        internal IConfigItem CfgItem
        {
            get { return cfgItem; }
        }
    }

    internal class ClusterConfig
    {
        private Dictionary<ClusterConfigItem, Configuration> configItems = new Dictionary<ClusterConfigItem, Configuration>()
        {
            { ClusterConfigItem.es_hostname, new Configuration(new HostNameConfigItem("Environment Server Host Name", true, false)) },
            { ClusterConfigItem.es_ip, new Configuration(new IPAddressConfigItem("Environment Server IP Address", true, false)) },
            { ClusterConfigItem.timeserver, new Configuration(new HostNameArrayConfigItem("NTP Server", true, false)) },
            { ClusterConfigItem.nameserver, new Configuration(new HostNameArrayConfigItem("DNS Server", true, true)) },
            { ClusterConfigItem.net1_gateway, new Configuration(new IPAddressConfigItem("Default Gateway", true, false)) },
            { ClusterConfigItem.net1_netmask, new Configuration(new IPAddressConfigItem("Network 1 Netmask", true, false)) },
            { ClusterConfigItem.net1_dhcp, new Configuration(new BooleanConfigItem(false, "Network 1 DHCP")) },
            { ClusterConfigItem.net2_enabled, new Configuration(new BooleanConfigItem(false, "Network 2 Enabled")) },
            { ClusterConfigItem.net2_netmask, new Configuration(new IPAddressConfigItem("Network 2 Netmask", true, true)) },
            { ClusterConfigItem.net2_dhcp, new Configuration(new BooleanConfigItem(false, "Network 2 DHCP")) },
            { ClusterConfigItem.appliance_1_hostname, new Configuration(new HostNameConfigItem("Appliance 1 Host Name", false, false)) },
            { ClusterConfigItem.appliance_1_port1_ip, new Configuration(new IPAddressConfigItem("Appliance 1 Port 1", false, false)) },
            { ClusterConfigItem.appliance_1_port2_ip, new Configuration(new IPAddressConfigItem("Appliance 1 Port 2", false, true)) },
            { ClusterConfigItem.appliance_2_hostname, new Configuration(new HostNameConfigItem("Appliance 2 Host Name", false, true)) },
            { ClusterConfigItem.appliance_2_port1_ip, new Configuration(new IPAddressConfigItem("Appliance 2 Port 1", false, false)) },
            { ClusterConfigItem.appliance_2_port2_ip, new Configuration(new IPAddressConfigItem("Appliance 2 Port 2", false, true)) },
        };

        private static readonly string clusterConfigFileShortname = @"cluster_config.rc";
        private static readonly string sortableFormat = "s"; // see DateTime format specifiers.
        private static readonly string successfullyDeployedMsg = "Successfully deployed on";
        private static readonly string versionMsg = "ShadowwCat version:";
        private ESHostInfo hostInfo;
        private string lastSuccessfulConfigFile = string.Empty;
        private string provisionalConfigFile = string.Empty;
        private bool isConfigurationChanged = false;


        internal ClusterConfig(string CommonDataPath, ESHostInfo HostInfo)
        {
            // Sanity check. We should always write all values, even empty, to the control cluster config file used by the appliance.
            if (Enum.GetNames(typeof(ClusterConfigItem)).Length != configItems.Count)
            {
                throw new Exception("Internal error in ClusterConfig(): The number of cluster config items does not match the number of config enum items.");
            }
            ClusterConfigFilename = CommonDataPath + @"\" + clusterConfigFileShortname;
            lastSuccessfulConfigFile = ClusterConfigFilename + "_lastsuccessful";
            provisionalConfigFile = ClusterConfigFilename + "_provisional";
            hostInfo = HostInfo;
        }

        internal ESHostInfo.NetworkInfoDictionary NetworkInfo
        {
            get { return hostInfo.NetworkInfo; }
        }

        internal ESHostInfo HostInfo
        {
            get { return hostInfo; }
        }

        /// <summary>
        /// Write out the cluster config file. Name and location of file is not user settable.
        /// </summary>
        /// <param name=""></param>
        internal void Save()
        {
            if (!this.Net2_Enabled)
            {
                this.Net2_DHCP = false;
                this.Net2_Netmask = "";
                this.Appliance_1_Port2_IP = "";
                this.Appliance_2_Port2_IP = "";
            }

            if (File.Exists(ClusterConfigFilename))
            {
                File.Delete(ClusterConfigFilename);
                // No longer backing up config file - for now.
                //File.Move(ClusterConfigFilename, string.Format("{0}_{1}.backup", ClusterConfigFilename,
                //    DateTime.Now.ToString(sortableFormat).Replace(':', '.')));
            }
            using (StreamWriter fs = File.CreateText(ClusterConfigFilename))
            {
                fs.NewLine = "\n"; //Very important. The appliance expects UNIX style line enders.
                fs.WriteLine(string.Format("# Created: {0}", DateTime.Now.ToString(sortableFormat)));
                fs.WriteLine("#");
                fs.WriteLine(string.Format("# {0} {1}", versionMsg, AppSession.ShadowwCatVersion));
                fs.WriteLine("#");
                fs.WriteLine("#  WARNING - do not modify manually! This file is generated.");
                fs.WriteLine("#");

                foreach (ClusterConfigItem key in configItems.Keys)
                {
                    IConfigItem value = configItems[key].CfgItem;
                    fs.WriteLine(string.Format("{0}=\"{1}\"", key.ToString(), value.SaveToFile()));
                }
            }
            Logger.Log(Logger.Levels.Info, "Saved cluster configuration file " + ClusterConfigFilename);
            isConfigurationChanged = false;
        }

        internal void SaveAsLastSuccessful()
        {
            if (File.Exists(lastSuccessfulConfigFile))
            {
                File.Delete(lastSuccessfulConfigFile);
            }
            File.Copy(ClusterConfigFilename, lastSuccessfulConfigFile);
            string[] lines = File.ReadAllLines(ClusterConfigFilename);
            //Prepend update datetime and deployment status
            using (StreamWriter fs = new StreamWriter(lastSuccessfulConfigFile))
            {
                fs.NewLine = "\n"; //Very important. The appliance expects UNIX style line enders.
                fs.WriteLine(string.Format("# {0} {1}", successfullyDeployedMsg, DateTime.Now.ToString(sortableFormat)));
                foreach (string aLine in lines)
                {
                    fs.WriteLine(aLine);
                }
            }
            Logger.Log(Logger.Levels.Info, "Saved as last successful configuration file " + lastSuccessfulConfigFile);
            WasSuccessfullyDeployed = true;
        }

        internal void SaveAsProvisional()
        {
            this.Save();
            if (File.Exists(provisionalConfigFile))
            {
                File.Delete(provisionalConfigFile);
            }
            File.Copy(ClusterConfigFilename, provisionalConfigFile);
            Logger.Log(Logger.Levels.Info, "Saved as provisional configuration file " + provisionalConfigFile);
            WasSuccessfullyDeployed = false;
        }

        internal void LoadNewProvisional()
        {
            if (File.Exists(provisionalConfigFile))
            {
                File.Delete(provisionalConfigFile);
            }
            this.Load(provisionalConfigFile);
        }

        internal void LoadProvisional()
        {
            WasSuccessfullyDeployed = false;
            this.Load(provisionalConfigFile);
        }

        internal void LoadLastSuccessful()
        {
            if (File.Exists(lastSuccessfulConfigFile))
            {
                this.Load(lastSuccessfulConfigFile);
            }
        }

        private void Load(string configFile)
        {
            char[] charsToTrim = { '"', '\\' };
            string trimmedValue = String.Empty;
            bool loadedFromFile = false;
            WasSuccessfullyDeployed = false;

            if (File.Exists(configFile))
            {
                loadedFromFile = true;
                Logger.Log(Logger.Levels.Info, "Loading " + configFile);
                string[] lines = File.ReadAllLines(configFile);
                foreach (string aLine in lines)
                {
                    aLine.Trim();
                    if (aLine.IndexOf('#') == 0) //Throw out all commented lines.
                    {
                        if (aLine.Contains(successfullyDeployedMsg))
                        {
                            WasSuccessfullyDeployed = true;
                        }
                        continue;
                    }
                    else if (aLine.Contains("="))
                    {
                        var key = aLine.Substring(0, aLine.IndexOf('='));
                        var value = aLine.Substring(aLine.IndexOf('=')+1);
                        if (!String.IsNullOrEmpty(value))
                        {
                            trimmedValue = value.Trim(charsToTrim);
                        }
                        //Map the key value in the config file to a ClusterConfigItem enum.
                        ClusterConfigItem i = (ClusterConfigItem)Enum.Parse(typeof(ClusterConfigItem), key);
                        if (Enum.IsDefined(typeof(ClusterConfigItem), i))
                        {
                            configItems[i].CfgItem.ReadFromFile(trimmedValue);
                        }
                        else
                        {
                            //Bad juju. We found an item in the config file with no corresponding enum.
                            //We might want to do a count here to determine if we got the # of items expected.
                        }
                    }
                }
            }
            else
            {
                Logger.Log(Logger.Levels.Info, "Creating new cluster configuration file - " + configFile);
            }

            ESHostInfo.NETWORK_INFO ni;
            try
            {
                ni = hostInfo.NetworkInfo.First().Value;
            }
            catch (InvalidOperationException)
            {
                ni = new ESHostInfo.NETWORK_INFO();
            }

            if (loadedFromFile)
            {
                // Verify all items in ClusterConfigItem. If any are invalid, show error indicating 
                // saved cluster config has been modified, and somehow indicate which fields have changed.
                if (!VerifyConfig(configItems, hostInfo))
                {

                    // Not all config items were valid.
                    // Find the invalid ones, build the error string, and set required settings to defaults.
                    string invalidSettings = "";

                    foreach (KeyValuePair<ClusterConfigItem, Configuration> item in configItems)
                    {
                        ClusterConfigItem key = item.Key;
                        IConfigItem cfgItem = item.Value.CfgItem;

                        if (!item.Value.Validated && !cfgItem.IsDefault())
                        {
                            if (invalidSettings == "")
                            {
                                invalidSettings += cfgItem.UserFriendlyString;
                            }
                            else
                            {
                                invalidSettings += ", " + cfgItem.UserFriendlyString;
                            }

                            switch (key)
                            {
                                case ClusterConfigItem.es_hostname:
                                    Update(key, hostInfo.FullyQualifiedHostName);
                                    isConfigurationChanged = true;
                                    break;
                                case ClusterConfigItem.timeserver:
                                    string[] ntp = (configItems[ClusterConfigItem.nameserver].CfgItem.Value.Length == 0) ? hostInfo.NTPIPAddresses : hostInfo.NTPHostNames;
                                    if (ntp.Length == 0)
                                    {
                                        Update(key, new string[] { ni.ipAddress });
                                    } else
                                    {
                                        Update(key, ntp);
                                    }
                                    break;
                                case ClusterConfigItem.es_ip:
                                    Update(key, ni.ipAddress);
                                    break;
                                case ClusterConfigItem.net1_netmask:
                                    Update(key, ni.netmask);
                                    break;
                                case ClusterConfigItem.net2_netmask:
                                    Update(key, ni.netmask);
                                    break;
                                case ClusterConfigItem.net1_gateway:
                                    Update(key, ni.defaultGateway);
                                    break;
                                case ClusterConfigItem.nameserver:
                                    Update(key, ni.dnsServerIPs != null ? ni.dnsServerIPs : new string[] { });
                                    break;
                            }
                        }
                    }

                    string strMessage = "The saved cluster configuration contained settings [" + invalidSettings + "] that were no longer valid. They have been changed to current network settings.";
                    MessageBox.Show(strMessage, "Control Admin Tool Warning");
                    Logger.Log(Logger.Levels.Info, strMessage);
                }
            }
            else
            {
                Update(ClusterConfigItem.es_ip, ni.ipAddress);
                Update(ClusterConfigItem.timeserver, hostInfo.NTPHostNames);
                Update(ClusterConfigItem.nameserver, ni.dnsServerIPs);
                Update(ClusterConfigItem.net1_dhcp, false);
                Update(ClusterConfigItem.net1_netmask, ni.netmask);
                Update(ClusterConfigItem.net1_gateway, ni.defaultGateway);
                Update(ClusterConfigItem.net2_dhcp, false);
                Update(ClusterConfigItem.net2_netmask, ni.netmask);
                Update(ClusterConfigItem.net2_enabled, true);
            }

            AppSession.IsConfigureClusterBridged = GetConfigValue(ClusterConfigItem.net2_enabled);
            AppSession.UseDNS = GetConfigValue(ClusterConfigItem.nameserver).Length > 0;
            AppSession.AppliancesHaveSamePassword = false;
        }

        private dynamic GetConfigValue(ClusterConfigItem key)
        {
            return configItems[key].CfgItem.Value;
        }

        /// <summary>
        /// Update a config item's value, but only if it has changed.
        /// </summary>
        /// <param name="Item"></param>
        /// <param name="Value"></param>
        private void Update(ClusterConfigItem Item, dynamic Value)
        {
            if (configItems[Item].CfgItem.Value != Value)
            {
                configItems[Item].CfgItem.Value = Value;
                isConfigurationChanged = true;
            }
        }

        private static Boolean VerifyConfig(Dictionary<ClusterConfigItem, Configuration> config, ESHostInfo hostInfo)
        {
            ESHostInfo.NetworkInfoDictionary networks = hostInfo.NetworkInfo;
            foreach (ClusterConfigItem key in config.Keys)
            {
                string[] validValues = GetValidationValues(hostInfo, key);
                config[key].Validated |= config[key].CfgItem.IsValidValue(validValues);
            }

            return isConfigValidated(config);
        }

        private static Boolean isConfigValidated(Dictionary<ClusterConfigItem, Configuration> config)
        {
            foreach (ClusterConfigItem item in config.Keys)
            {
                if (!config[item].Validated)
                {
                    return false;
                }
            }

            return true;
        }

        private static string[] GetValidationValues(ESHostInfo hostInfo, ClusterConfigItem key)
        {
            List<string> items = new List<string>();
            ESHostInfo.NetworkInfoDictionary networks = hostInfo.NetworkInfo;

            switch (key)
            {
                case ClusterConfigItem.es_hostname:
                    items.Add(hostInfo.FullyQualifiedHostName);
                    break;
                case ClusterConfigItem.timeserver:
                    items.AddRange(hostInfo.NTPHostNames);
                    items.AddRange(hostInfo.NTPIPAddresses);
                    break;
            }

            foreach (ESHostInfo.NETWORK_INFO ni in networks.Values)
            {
                switch (key)
                {
                    case ClusterConfigItem.timeserver:
                        items.Add(ni.ipAddress);
                        break;
                    case ClusterConfigItem.es_ip:
                        items.Add(ni.ipAddress);
                        break;
                    case ClusterConfigItem.net1_netmask:
                    case ClusterConfigItem.net2_netmask:
                        items.Add(ni.netmask);
                        break;
                    case ClusterConfigItem.net1_gateway:
                        items.Add(ni.defaultGateway);
                        break;
                    case ClusterConfigItem.nameserver:
                        items.AddRange(ni.dnsServerIPs);
                        break;
                }
            }

            return items.ToArray();
        }

        public string ClusterConfigFilename { get; private set; }

        public string ES_Hostname
        {
            get { return GetConfigValue(ClusterConfigItem.es_hostname); }
        }

        public string ES_IPAddress
        {
            get { return GetConfigValue(ClusterConfigItem.es_ip); }
            set { Update(ClusterConfigItem.es_ip, value); }
        }
        public string[] Timeserver
        {
            get { return GetConfigValue(ClusterConfigItem.timeserver); }
            set { Update(ClusterConfigItem.timeserver, value); }
        }
        public string[] Nameserver
        {
            get { return GetConfigValue(ClusterConfigItem.nameserver); }
            set { Update(ClusterConfigItem.nameserver, value); }
        }
        public bool Net1_DHCP
        {
            get { return GetConfigValue(ClusterConfigItem.net1_dhcp); }
            set { Update(ClusterConfigItem.net1_dhcp, value); }
        }
        public string Net1_Netmask
        {
            get { return GetConfigValue(ClusterConfigItem.net1_netmask); }
            set { Update(ClusterConfigItem.net1_netmask, value); }
        }
        public string Net1_Gateway
        {
            get { return GetConfigValue(ClusterConfigItem.net1_gateway); }
            set { Update(ClusterConfigItem.net1_gateway, value); }
        }
        public bool Net2_Enabled
        {
            get { return GetConfigValue(ClusterConfigItem.net2_enabled); }
            set { Update(ClusterConfigItem.net2_enabled, value); }
        }
        public bool Net2_DHCP
        {
            get { return GetConfigValue(ClusterConfigItem.net2_dhcp); }
            set { Update(ClusterConfigItem.net2_dhcp, value); }
        }
        public string Net2_Netmask
        {
            get { return GetConfigValue(ClusterConfigItem.net2_netmask); }
            set { Update(ClusterConfigItem.net2_netmask, value); }
        }
        public string Appliance_1_Hostname
        {
            get { return GetConfigValue(ClusterConfigItem.appliance_1_hostname); }
            set { Update(ClusterConfigItem.appliance_1_hostname, value); }
        }
        public string Appliance_1_Port1_IP
        {
            get { return GetConfigValue(ClusterConfigItem.appliance_1_port1_ip); }
            set { Update(ClusterConfigItem.appliance_1_port1_ip, value); }
        }
        public string Appliance_1_Port2_IP
        {
            get { return GetConfigValue(ClusterConfigItem.appliance_1_port2_ip); }
            set { Update(ClusterConfigItem.appliance_1_port2_ip, value); }
        }
        public string Appliance_2_Hostname
        {
            get { return GetConfigValue(ClusterConfigItem.appliance_2_hostname); }
            set { Update(ClusterConfigItem.appliance_2_hostname, value); }
        }
        public string Appliance_2_Port1_IP
        {
            get { return GetConfigValue(ClusterConfigItem.appliance_2_port1_ip); }
            set { Update(ClusterConfigItem.appliance_2_port1_ip, value); }
        }
        public string Appliance_2_Port2_IP
        {
            get { return GetConfigValue(ClusterConfigItem.appliance_2_port2_ip); }
            set { Update(ClusterConfigItem.appliance_2_port2_ip, value); }
        }
        /// <summary>
        /// Caller can use to decide if a file save is warranted. Might prompt the user...TBD
        /// </summary>
        public bool WasSuccessfullyDeployed { get; private set; }
        public bool IsSuccessfulAvailable
        {
            get { return File.Exists(lastSuccessfulConfigFile); } 
        }

        public string InstallDir()
        {
            string value64 = string.Empty;
            string value32 = string.Empty;
            string synapsoftKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\SynapSoft 7.0.0";

            // Check for the SynapSoft installed as a 64-bit application
            RegistryKey localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
            localKey = localKey.OpenSubKey(synapsoftKey);
            if (localKey != null)
            {
                value64 = localKey.GetValue("InstallLocation", "").ToString();
                if (!string.IsNullOrEmpty(value64))
                {
                    return value64;
                }
            }

            // Check for the SynapSoft installed as a 32-bit application
            RegistryKey localKey32 = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);
            localKey32 = localKey32.OpenSubKey(synapsoftKey);
            if (localKey32 != null)
            {
                value32 = localKey32.GetValue("InstallLocation", "").ToString();
                if (!string.IsNullOrEmpty(value32))
                {
                    return value32;
                }
            }

            return null;
        }

    }
}
