﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shadowwcat.Dialogs
{
    class CommandInfo
    {
        public CommandInfo(string p, Action action1, Action action2)
        {
            this.CommandName = p;
            this.Action1 = action1;
            this.Action2 = action2;
            Results = string.Empty;
        }
        public string CommandName { get; set; }
        public string Results { get; set; }
        public Action Action1 { get; set; }
        public Action Action2 { get; set; }
    }
}
