﻿namespace shadowwcat.Dialogs
{
    partial class RestoreCluster3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestoreCluster3));
            this.btnNext = new System.Windows.Forms.Button();
            this.lblCluster1Connect = new System.Windows.Forms.Label();
            this.lblCluster2Connect = new System.Windows.Forms.Label();
            this.lblCluster1Run = new System.Windows.Forms.Label();
            this.lblCluster2Run = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblCluster1ConnectStatus = new System.Windows.Forms.Label();
            this.lblCluster2ConnectStatus = new System.Windows.Forms.Label();
            this.lblCluster1RunVersion = new System.Windows.Forms.Label();
            this.lblCluster1RunStatus = new System.Windows.Forms.Label();
            this.lblCluster2RunVersion = new System.Windows.Forms.Label();
            this.lblCluster2RunStatus = new System.Windows.Forms.Label();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.bw = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(482, 337);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblCluster1Connect
            // 
            this.lblCluster1Connect.AutoSize = true;
            this.lblCluster1Connect.Location = new System.Drawing.Point(12, 15);
            this.lblCluster1Connect.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.lblCluster1Connect.Name = "lblCluster1Connect";
            this.lblCluster1Connect.Size = new System.Drawing.Size(174, 17);
            this.lblCluster1Connect.TabIndex = 0;
            this.lblCluster1Connect.Text = "Connection to controller 1:";
            // 
            // lblCluster2Connect
            // 
            this.lblCluster2Connect.AutoSize = true;
            this.lblCluster2Connect.Location = new System.Drawing.Point(12, 47);
            this.lblCluster2Connect.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.lblCluster2Connect.Name = "lblCluster2Connect";
            this.lblCluster2Connect.Size = new System.Drawing.Size(174, 17);
            this.lblCluster2Connect.TabIndex = 0;
            this.lblCluster2Connect.Text = "Connection to controller 2:";
            // 
            // lblCluster1Run
            // 
            this.lblCluster1Run.AutoSize = true;
            this.lblCluster1Run.Location = new System.Drawing.Point(12, 94);
            this.lblCluster1Run.Margin = new System.Windows.Forms.Padding(0, 15, 10, 15);
            this.lblCluster1Run.Name = "lblCluster1Run";
            this.lblCluster1Run.Size = new System.Drawing.Size(85, 17);
            this.lblCluster1Run.TabIndex = 0;
            this.lblCluster1Run.Text = "Controller 1:";
            // 
            // lblCluster2Run
            // 
            this.lblCluster2Run.AutoSize = true;
            this.lblCluster2Run.Location = new System.Drawing.Point(12, 115);
            this.lblCluster2Run.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.lblCluster2Run.Name = "lblCluster2Run";
            this.lblCluster2Run.Size = new System.Drawing.Size(85, 17);
            this.lblCluster2Run.TabIndex = 0;
            this.lblCluster2Run.Text = "Controller 2:";
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(367, 337);
            this.btnBack.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 4;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 337);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblCluster1ConnectStatus
            // 
            this.lblCluster1ConnectStatus.AutoSize = true;
            this.lblCluster1ConnectStatus.Location = new System.Drawing.Point(196, 15);
            this.lblCluster1ConnectStatus.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lblCluster1ConnectStatus.Name = "lblCluster1ConnectStatus";
            this.lblCluster1ConnectStatus.Size = new System.Drawing.Size(121, 17);
            this.lblCluster1ConnectStatus.TabIndex = 0;
            this.lblCluster1ConnectStatus.Text = "Connection status";
            // 
            // lblCluster2ConnectStatus
            // 
            this.lblCluster2ConnectStatus.AutoSize = true;
            this.lblCluster2ConnectStatus.Location = new System.Drawing.Point(196, 47);
            this.lblCluster2ConnectStatus.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lblCluster2ConnectStatus.Name = "lblCluster2ConnectStatus";
            this.lblCluster2ConnectStatus.Size = new System.Drawing.Size(121, 17);
            this.lblCluster2ConnectStatus.TabIndex = 0;
            this.lblCluster2ConnectStatus.Text = "Connection status";
            // 
            // lblCluster1RunVersion
            // 
            this.lblCluster1RunVersion.AutoSize = true;
            this.lblCluster1RunVersion.Location = new System.Drawing.Point(107, 94);
            this.lblCluster1RunVersion.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.lblCluster1RunVersion.Name = "lblCluster1RunVersion";
            this.lblCluster1RunVersion.Size = new System.Drawing.Size(56, 17);
            this.lblCluster1RunVersion.TabIndex = 0;
            this.lblCluster1RunVersion.Text = "Version";
            // 
            // lblCluster1RunStatus
            // 
            this.lblCluster1RunStatus.AutoSize = true;
            this.lblCluster1RunStatus.Location = new System.Drawing.Point(173, 94);
            this.lblCluster1RunStatus.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lblCluster1RunStatus.Name = "lblCluster1RunStatus";
            this.lblCluster1RunStatus.Size = new System.Drawing.Size(48, 17);
            this.lblCluster1RunStatus.TabIndex = 0;
            this.lblCluster1RunStatus.Text = "Status";
            // 
            // lblCluster2RunVersion
            // 
            this.lblCluster2RunVersion.AutoSize = true;
            this.lblCluster2RunVersion.Location = new System.Drawing.Point(107, 115);
            this.lblCluster2RunVersion.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.lblCluster2RunVersion.Name = "lblCluster2RunVersion";
            this.lblCluster2RunVersion.Size = new System.Drawing.Size(56, 17);
            this.lblCluster2RunVersion.TabIndex = 0;
            this.lblCluster2RunVersion.Text = "Version";
            // 
            // lblCluster2RunStatus
            // 
            this.lblCluster2RunStatus.AutoSize = true;
            this.lblCluster2RunStatus.Location = new System.Drawing.Point(173, 115);
            this.lblCluster2RunStatus.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lblCluster2RunStatus.Name = "lblCluster2RunStatus";
            this.lblCluster2RunStatus.Size = new System.Drawing.Size(48, 17);
            this.lblCluster2RunStatus.TabIndex = 0;
            this.lblCluster2RunStatus.Text = "Status";
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStatus.Location = new System.Drawing.Point(12, 162);
            this.txtStatus.Margin = new System.Windows.Forms.Padding(0, 15, 0, 15);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatus.Size = new System.Drawing.Size(567, 160);
            this.txtStatus.TabIndex = 2;
            // 
            // bw
            // 
            this.bw.WorkerReportsProgress = true;
            this.bw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_DoWork);
            this.bw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw_RunWorkerCompleted);
            // 
            // RestoreCluster3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(597, 380);
            this.ControlBox = false;
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.lblCluster2RunStatus);
            this.Controls.Add(this.lblCluster2RunVersion);
            this.Controls.Add(this.lblCluster1RunStatus);
            this.Controls.Add(this.lblCluster1RunVersion);
            this.Controls.Add(this.lblCluster2ConnectStatus);
            this.Controls.Add(this.lblCluster1ConnectStatus);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblCluster2Run);
            this.Controls.Add(this.lblCluster1Run);
            this.Controls.Add(this.lblCluster2Connect);
            this.Controls.Add(this.lblCluster1Connect);
            this.Controls.Add(this.btnNext);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RestoreCluster3";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Restore Cluster - Controller connection status";
            this.Load += new System.EventHandler(this.RestoreCluster3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblCluster1Connect;
        private System.Windows.Forms.Label lblCluster2Connect;
        private System.Windows.Forms.Label lblCluster1Run;
        private System.Windows.Forms.Label lblCluster2Run;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblCluster1ConnectStatus;
        private System.Windows.Forms.Label lblCluster2ConnectStatus;
        private System.Windows.Forms.Label lblCluster1RunVersion;
        private System.Windows.Forms.Label lblCluster1RunStatus;
        private System.Windows.Forms.Label lblCluster2RunVersion;
        private System.Windows.Forms.Label lblCluster2RunStatus;
        private System.Windows.Forms.TextBox txtStatus;
        private System.ComponentModel.BackgroundWorker bw;
    }
}