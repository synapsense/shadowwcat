﻿namespace shadowwcat.Dialogs
{
    partial class ConfigureClusterPort1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureClusterPort1));
            this.label1 = new System.Windows.Forms.Label();
            this.mtxtPort1DefaultGateway = new IPAddressControlLib.IPAddressControl();
            this.label2 = new System.Windows.Forms.Label();
            this.mtxtController2Port1IPAddress = new IPAddressControlLib.IPAddressControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtController2Hostname = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtxtPort1Netmask = new IPAddressControlLib.IPAddressControl();
            this.txtController1Hostname = new System.Windows.Forms.TextBox();
            this.mtxtController1Port1IPAddress = new IPAddressControlLib.IPAddressControl();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lnkResetValues = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Default Gateway:";
            // 
            // mtxtPort1DefaultGateway
            // 
            this.mtxtPort1DefaultGateway.AllowInternalTab = false;
            this.mtxtPort1DefaultGateway.AutoHeight = true;
            this.mtxtPort1DefaultGateway.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtPort1DefaultGateway.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtPort1DefaultGateway.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtPort1DefaultGateway.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtxtPort1DefaultGateway.Location = new System.Drawing.Point(141, 52);
            this.mtxtPort1DefaultGateway.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.mtxtPort1DefaultGateway.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtPort1DefaultGateway.Name = "mtxtPort1DefaultGateway";
            this.mtxtPort1DefaultGateway.ReadOnly = false;
            this.mtxtPort1DefaultGateway.Size = new System.Drawing.Size(115, 22);
            this.mtxtPort1DefaultGateway.TabIndex = 5;
            this.mtxtPort1DefaultGateway.Text = "...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Subnet mask:";
            // 
            // mtxtController2Port1IPAddress
            // 
            this.mtxtController2Port1IPAddress.AllowInternalTab = false;
            this.mtxtController2Port1IPAddress.AutoHeight = true;
            this.mtxtController2Port1IPAddress.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtController2Port1IPAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtController2Port1IPAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtController2Port1IPAddress.Location = new System.Drawing.Point(129, 76);
            this.mtxtController2Port1IPAddress.Margin = new System.Windows.Forms.Padding(0);
            this.mtxtController2Port1IPAddress.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtController2Port1IPAddress.Name = "mtxtController2Port1IPAddress";
            this.mtxtController2Port1IPAddress.ReadOnly = false;
            this.mtxtController2Port1IPAddress.Size = new System.Drawing.Size(115, 22);
            this.mtxtController2Port1IPAddress.TabIndex = 11;
            this.mtxtController2Port1IPAddress.Text = "...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(43, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Hostname:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 79);
            this.label4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Port 1 IP:";
            // 
            // txtController2Hostname
            // 
            this.txtController2Hostname.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtController2Hostname.Location = new System.Drawing.Point(129, 39);
            this.txtController2Hostname.Margin = new System.Windows.Forms.Padding(0, 10, 0, 15);
            this.txtController2Hostname.Name = "txtController2Hostname";
            this.txtController2Hostname.Size = new System.Drawing.Size(249, 22);
            this.txtController2Hostname.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtxtController2Port1IPAddress);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtController2Hostname);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 233);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(15, 15, 15, 0);
            this.groupBox1.Size = new System.Drawing.Size(393, 113);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controller 2";
            // 
            // mtxtPort1Netmask
            // 
            this.mtxtPort1Netmask.AllowInternalTab = false;
            this.mtxtPort1Netmask.AutoHeight = true;
            this.mtxtPort1Netmask.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtPort1Netmask.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtPort1Netmask.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtPort1Netmask.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtxtPort1Netmask.Location = new System.Drawing.Point(141, 15);
            this.mtxtPort1Netmask.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.mtxtPort1Netmask.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtPort1Netmask.Name = "mtxtPort1Netmask";
            this.mtxtPort1Netmask.ReadOnly = false;
            this.mtxtPort1Netmask.Size = new System.Drawing.Size(115, 22);
            this.mtxtPort1Netmask.TabIndex = 4;
            this.mtxtPort1Netmask.Text = "...";
            // 
            // txtController1Hostname
            // 
            this.txtController1Hostname.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtController1Hostname.Location = new System.Drawing.Point(129, 39);
            this.txtController1Hostname.Margin = new System.Windows.Forms.Padding(0, 10, 0, 15);
            this.txtController1Hostname.Name = "txtController1Hostname";
            this.txtController1Hostname.Size = new System.Drawing.Size(249, 22);
            this.txtController1Hostname.TabIndex = 7;
            // 
            // mtxtController1Port1IPAddress
            // 
            this.mtxtController1Port1IPAddress.AllowInternalTab = false;
            this.mtxtController1Port1IPAddress.AutoHeight = true;
            this.mtxtController1Port1IPAddress.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtController1Port1IPAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtController1Port1IPAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtController1Port1IPAddress.Location = new System.Drawing.Point(129, 76);
            this.mtxtController1Port1IPAddress.Margin = new System.Windows.Forms.Padding(0);
            this.mtxtController1Port1IPAddress.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtController1Port1IPAddress.Name = "mtxtController1Port1IPAddress";
            this.mtxtController1Port1IPAddress.ReadOnly = false;
            this.mtxtController1Port1IPAddress.Size = new System.Drawing.Size(115, 22);
            this.mtxtController1Port1IPAddress.TabIndex = 8;
            this.mtxtController1Port1IPAddress.Text = "...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 42);
            this.label5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Hostname:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(53, 79);
            this.label6.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Port 1 IP:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtController1Hostname);
            this.groupBox2.Controls.Add(this.mtxtController1Port1IPAddress);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 104);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0, 15, 15, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(15, 15, 15, 0);
            this.groupBox2.Size = new System.Drawing.Size(393, 113);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Controller 1";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(502, 532);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 20;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(382, 532);
            this.btnBack.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 21;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 532);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 22;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lnkResetValues
            // 
            this.lnkResetValues.Location = new System.Drawing.Point(262, 538);
            this.lnkResetValues.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.lnkResetValues.Name = "lnkResetValues";
            this.lnkResetValues.Size = new System.Drawing.Size(92, 17);
            this.lnkResetValues.TabIndex = 24;
            this.lnkResetValues.TabStop = true;
            this.lnkResetValues.Text = "Reset Values";
            this.lnkResetValues.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkResetValues_LinkClicked);
            // 
            // ConfigureClusterPort1
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(617, 575);
            this.ControlBox = false;
            this.Controls.Add(this.lnkResetValues);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.mtxtPort1Netmask);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.mtxtPort1DefaultGateway);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBack);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ConfigureClusterPort1";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure cluster - IT network settings (controller port 1)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private IPAddressControlLib.IPAddressControl mtxtPort1DefaultGateway;
        private System.Windows.Forms.Label label2;
        private IPAddressControlLib.IPAddressControl mtxtController2Port1IPAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtController2Hostname;
        private System.Windows.Forms.GroupBox groupBox1;
        private IPAddressControlLib.IPAddressControl mtxtPort1Netmask;
        private System.Windows.Forms.TextBox txtController1Hostname;
        private IPAddressControlLib.IPAddressControl mtxtController1Port1IPAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.LinkLabel lnkResetValues;
    }
}