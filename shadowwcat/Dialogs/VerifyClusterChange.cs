﻿using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace shadowwcat.Dialogs
{
    public partial class VerifyClusterChange : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.VerifyClusterChange;

        public VerifyClusterChange()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnNext;

                String wizardName = "";

                if (AppSession.verifyType == AppSession.VERIFY_TYPE.RESUME_FULL_UPGRADE)
                {
                    wizardName = "Resume upgrade";
                    label1.Text = "Ready to perform the last phase of a full platform upgrade.Ensure that the Environmental Server was successfully upgraded, is up and running, and that all maps have been exported." + Environment.NewLine +
                        Environment.NewLine +
                        "This process takes 7 to 10 minutes.";

                    label2.Hide();
                    lstAppliances.Hide();
                    label3.Hide();
                    lblFirmwareImage.Hide();
                }
                else
                {
                    if (AppSession.verifyType == AppSession.VERIFY_TYPE.FULL_UPGRADE)
                    {
                        wizardName = "Upgrade firmware";
                        label1.Text = "Ready to perform the first phase of a full platform upgrade." + Environment.NewLine +
                            Environment.NewLine +
                            "This phase takes 2 to 5 minutes.";
                    }
                    else if (AppSession.verifyType == AppSession.VERIFY_TYPE.FIRMWARE_ONLY_UPGRADE)
                    {
                        wizardName = "Upgrade firmware";
                        label1.Text = "Ready to upgrade appliance firmware." + Environment.NewLine +
                            Environment.NewLine +
                            "This process takes 6 to 7 minutes.";
                    }
                    else if (AppSession.verifyType == AppSession.VERIFY_TYPE.RESTORE_CLUSTER)
                    {
                        wizardName = "Restore cluster";
                        label1.Text = "Ready to restore the cluster." + Environment.NewLine +
                            Environment.NewLine +
                            "This process takes 7 to 10 minutes." + Environment.NewLine +
                            Environment.NewLine +
                            "When complete, cluster reliability will be restored. This operation cannot be interrupted.";
                    }
                    else
                    {
                        label1.Text = "";
                    }

                    label2.Show();
                    lstAppliances.Show();
                    label3.Show();
                    lblFirmwareImage.Show();


                    lstAppliances.Items.Clear();
                    lstAppliances.Items.Add(AppSession.ClusterConfig.Appliance_1_Hostname);
                    lstAppliances.Items.Add(AppSession.ClusterConfig.Appliance_2_Hostname);
                    lblFirmwareImage.Text = AppSession.ApplianceFirmwareFile;
                }

                this.Text = String.Format("{0} - Verify information", wizardName);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!Navigator.Execute(this, Navigator.TaskRequest.EnterAppliancePasswords))
            {
                return;
            }

            AppSession.commandRunnerInfo = new CommandRunnerInfo();

            Navigator.DialogName next = Navigator.DialogName.RunCommands;

            switch (AppSession.verifyType)
            {
                case AppSession.VERIFY_TYPE.RESTORE_CLUSTER:
                    AppSession.commandRunnerInfo = SetupRestoreCommand();
                    break;
                case AppSession.VERIFY_TYPE.FULL_UPGRADE:
                    AppSession.commandRunnerInfo = SetupFullUpgradeCommand();
                    break;
                case AppSession.VERIFY_TYPE.FIRMWARE_ONLY_UPGRADE:
                    AppSession.commandRunnerInfo = SetupFirmwareUpgradeCommand();
                    break;
                case AppSession.VERIFY_TYPE.RESUME_FULL_UPGRADE:
                    AppSession.commandRunnerInfo = SetupResumeFullUpgradeCommand();
                    break;
            }

            Navigator.GoTo(Navigator.DialogName.VerifyClusterChange, next);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.DialogName previous = Navigator.DialogName.VerifyClusterChange;

            switch (AppSession.verifyType)
            {
                case AppSession.VERIFY_TYPE.RESTORE_CLUSTER:
                    previous = Navigator.DialogName.RestoreCluster4;
                    break;
                case AppSession.VERIFY_TYPE.RESUME_FULL_UPGRADE:
                    previous = Navigator.DialogName.MainForm;
                    break;
                default:
                    previous = Navigator.DialogName.UpgradeFirmware2;
                    break;
            }

            Navigator.GoTo(Navigator.DialogName.VerifyClusterChange, previous);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        private CommandRunnerInfo SetupFirmwareUpgradeCommand()
        {
            CommandRunnerInfo cmdInfo = new CommandRunnerInfo();

            cmdInfo.taskName = "Appliance Firmware Upgrade";
            cmdInfo.taskDetails = "A firmware only upgrade";
            cmdInfo.startButtonName = "Start Firmware Upgrade";
            cmdInfo.customSuccessInstructions = "";
            cmdInfo.customErrorInstructions = "";

            cmdInfo.AddCommand(new CommandInfo("1. Test appliance connectivity...", new Action(cmdInfo.ctrl1.TestConnection), new Action(cmdInfo.ctrl2.TestConnection)));
            cmdInfo.AddCommand(new CommandInfo("2. Copy the appliance firmware files...", new Action(cmdInfo.ctrl1.CopyFirmwareImage), new Action(cmdInfo.ctrl2.CopyFirmwareImage)));
            cmdInfo.AddCommand(new CommandInfo("3. Applying upgrade package to appliance 1...", new Action(cmdInfo.ctrl1.ApplyFirmwareUpgrade), null));
            cmdInfo.AddCommand(new CommandInfo("4. Wait for appliance 1 to stabilize...", new Action(cmdInfo.ctrl1.WaitForApplianceRestart), null));
            cmdInfo.AddCommand(new CommandInfo("5. Applying upgrade package to appliance 2...", null, new Action(cmdInfo.ctrl2.ApplyFirmwareUpgrade)));
            cmdInfo.AddCommand(new CommandInfo("6. Wait for appliance 2 to stabilize...", null, new Action(cmdInfo.ctrl2.WaitForApplianceRestart)));
            cmdInfo.AddCommand(new CommandInfo("7. Checking cluster overall status...", new Action(cmdInfo.ctrl1.CommandClusterStable), new Action(cmdInfo.ctrl2.CommandClusterStable)));

            return cmdInfo;
        }

        private CommandRunnerInfo SetupFullUpgradeCommand()
        {
            CommandRunnerInfo cmdInfo = new CommandRunnerInfo();

            cmdInfo.taskName = "Appliance Upgrades";
            cmdInfo.taskDetails = "Phase 1 of a full platform upgrade";
            cmdInfo.startButtonName = "Start Full Upgrade";
            cmdInfo.customSuccessInstructions = "Exit the SynapSense ContolAdmin tool, perform the Environmental Server upgrade, and then rerun this tool and select 'Resume after full upgrade'.";
            cmdInfo.customErrorInstructions = "";

            cmdInfo.AddCommand(new CommandInfo("1. Test appliance connectivity...", new Action(cmdInfo.ctrl1.TestConnection), new Action(cmdInfo.ctrl2.TestConnection)));
            cmdInfo.AddCommand(new CommandInfo("2. Copy the appliance firmware files...", new Action(cmdInfo.ctrl1.CopyFirmwareImage), new Action(cmdInfo.ctrl2.CopyFirmwareImage)));
            cmdInfo.AddCommand(new CommandInfo("3. Pause ES data synchronization...", new Action(cmdInfo.ctrl1.CommandPauseESSync), null));

            return cmdInfo;
        }

        private CommandRunnerInfo SetupResumeFullUpgradeCommand()
        {
            CommandRunnerInfo cmdInfo = new CommandRunnerInfo();

            cmdInfo.taskName = "Resuming appliance upgrades";
            cmdInfo.taskDetails = "Phase 2 of a full platform upgrade";
            cmdInfo.startButtonName = "Resume Full Upgrade";
            cmdInfo.customSuccessInstructions = "";
            cmdInfo.customErrorInstructions = "";

            cmdInfo.AddCommand(new CommandInfo("1. Test appliance connectivity...", new Action(cmdInfo.ctrl1.TestConnection), new Action(cmdInfo.ctrl2.TestConnection)));
            cmdInfo.AddCommand(new CommandInfo("2. Pause ES data synchronization...", new Action(cmdInfo.ctrl1.CommandPauseESSync), null));
            cmdInfo.AddCommand(new CommandInfo("3. Stop clustering on appliance 2...", null, new Action(cmdInfo.ctrl2.CommandStopShadoww)));
            cmdInfo.AddCommand(new CommandInfo("4. Switch appliance 2 to isolated profile...", null, new Action(cmdInfo.ctrl2.CommandProfileIsolated)));
            cmdInfo.AddCommand(new CommandInfo("5. Applying upgrade package to appliance 2...", null, new Action(cmdInfo.ctrl2.ApplyFirmwareUpgrade)));

            cmdInfo.AddCommand(new CommandInfo("6. Wait for appliance 2 to stabilize...", null, new Action(cmdInfo.ctrl2.WaitForApplianceRestart)));

            cmdInfo.AddCommand(new CommandInfo("7. Checking cluster overall health...", null, new Action(cmdInfo.ctrl2.CommandClusterStable)));
            cmdInfo.AddCommand(new CommandInfo("8. Stop clustering on appliance 1...", new Action(cmdInfo.ctrl1.CommandStopShadoww), null));

            cmdInfo.AddCommand(new CommandInfo("9. Enable cluster profile on appliance 2...", null, new Action(cmdInfo.ctrl2.CommandProfileClustered)));
            cmdInfo.AddCommand(new CommandInfo("10. Resume clustering on appliance 2...", null, new Action(cmdInfo.ctrl2.CommandResumeClustering)));

            cmdInfo.AddCommand(new CommandInfo("11. Resume data sync with Environmental Server...", null, new Action(cmdInfo.ctrl2.CommandResumeESSync)));

            cmdInfo.AddCommand(new CommandInfo("12. Applying upgrade package to appliance 1...", new Action(cmdInfo.ctrl1.ApplyFirmwareUpgrade), null));
            cmdInfo.AddCommand(new CommandInfo("13. Wait for appliance 1 to stabilize...", new Action(cmdInfo.ctrl1.WaitForApplianceRestart), null));
            cmdInfo.AddCommand(new CommandInfo("14. Checking cluster overall status...", new Action(cmdInfo.ctrl1.CommandClusterStable), new Action(cmdInfo.ctrl2.CommandClusterStable)));

            return cmdInfo;
        }

        private CommandRunnerInfo SetupRestoreCommand()
        {
            CommandRunnerInfo cmdInfo = new CommandRunnerInfo();

            // Which appliance needs to be restored?
            if (AppSession.c1UpgradeInfo.IsRestorationTarget)
            {
                // commandRunnerInfo ctrl1 & 2 were initialized correctly
            }
            else
            {
                Command c1 = cmdInfo.ctrl1;
                Command c2 = cmdInfo.ctrl2;
                cmdInfo = new CommandRunnerInfo(c2, c1);
            }

            cmdInfo.taskName = "Cluster restoration";
            cmdInfo.taskDetails = "Reconfiguration after appliance replacement or repair";
            cmdInfo.startButtonName = "Start Cluster Restoration";
            cmdInfo.customSuccessInstructions = "";
            cmdInfo.customErrorInstructions = "";

            cmdInfo.AddCommand(new CommandInfo("1. Test appliance connectivity...", new Action(cmdInfo.ctrl1.TestConnection), new Action(cmdInfo.ctrl2.TestConnection)));
            cmdInfo.AddCommand(new CommandInfo("2. Copy the appliance firmware file...", new Action(cmdInfo.ctrl1.CopyFirmwareImage), null));
            cmdInfo.AddCommand(new CommandInfo("3. Apply the upgrade package...", new Action(cmdInfo.ctrl1.ApplyFirmwareUpgrade), null));
            cmdInfo.AddCommand(new CommandInfo("4. Wait for appliance to stabilize...", new Action(cmdInfo.ctrl1.WaitForApplianceRestart), null));
            cmdInfo.AddCommand(new CommandInfo("5. Copy the cluster config file...", new Action(cmdInfo.ctrl1.CopyClusterConfigurationFile), null));
            cmdInfo.AddCommand(new CommandInfo("6. Validate appliance configuration...", new Action(cmdInfo.ctrl1.ValidateApplianceConfiguration), null));
            cmdInfo.AddCommand(new CommandInfo("7. Test appliance configuration...", new Action(cmdInfo.ctrl1.TestApplianceConfiguration), null));
            cmdInfo.AddCommand(new CommandInfo("8. Generate appliance configuration...", new Action(cmdInfo.ctrl1.GenerateApplianceConfiguration), null));
            cmdInfo.AddCommand(new CommandInfo("9. Wait for appliance to stabilize...", new Action(cmdInfo.ctrl1.WaitForApplianceRestart), null));
            cmdInfo.AddCommand(new CommandInfo("10. Checking cluster overall health...", new Action(cmdInfo.ctrl1.CommandClusterStable), new Action(cmdInfo.ctrl2.CommandClusterStable)));

            return cmdInfo;
        }
    }
}
