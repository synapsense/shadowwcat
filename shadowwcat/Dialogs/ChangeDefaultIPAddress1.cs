﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ChangeDefaultIPAddress1 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ChangeDefaultIPAddress1;

        public ChangeDefaultIPAddress1()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnNext;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Navigator.Next(me);
        }

    }
}
