﻿namespace shadowwcat.Dialogs
{
    partial class ChangeDefaultIPAddress3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.gbCurrentSettings = new System.Windows.Forms.GroupBox();
            this.mtxtNetmaskCurrent = new IPAddressControlLib.IPAddressControl();
            this.mtxtIPAddressCurrent = new IPAddressControlLib.IPAddressControl();
            this.lblNetmaskCurrent = new System.Windows.Forms.Label();
            this.lblIPAddressCurrent = new System.Windows.Forms.Label();
            this.gbNewSettings = new System.Windows.Forms.GroupBox();
            this.mtxtNetmaskNew = new IPAddressControlLib.IPAddressControl();
            this.mtxtIPAddressNew = new IPAddressControlLib.IPAddressControl();
            this.lblNetmaskNew = new System.Windows.Forms.Label();
            this.lblIPAddressNew = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbCurrentSettings.SuspendLayout();
            this.gbNewSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 487);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Location = new System.Drawing.Point(302, 487);
            this.btnBack.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Location = new System.Drawing.Point(417, 487);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // gbCurrentSettings
            // 
            this.gbCurrentSettings.AutoSize = true;
            this.gbCurrentSettings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbCurrentSettings.Controls.Add(this.mtxtNetmaskCurrent);
            this.gbCurrentSettings.Controls.Add(this.mtxtIPAddressCurrent);
            this.gbCurrentSettings.Controls.Add(this.lblNetmaskCurrent);
            this.gbCurrentSettings.Controls.Add(this.lblIPAddressCurrent);
            this.gbCurrentSettings.Location = new System.Drawing.Point(15, 15);
            this.gbCurrentSettings.Margin = new System.Windows.Forms.Padding(0, 0, 0, 30);
            this.gbCurrentSettings.Name = "gbCurrentSettings";
            this.gbCurrentSettings.Padding = new System.Windows.Forms.Padding(15, 30, 15, 15);
            this.gbCurrentSettings.Size = new System.Drawing.Size(235, 134);
            this.gbCurrentSettings.TabIndex = 0;
            this.gbCurrentSettings.TabStop = false;
            this.gbCurrentSettings.Text = "Current (Factory Defaults)";
            // 
            // mtxtNetmaskCurrent
            // 
            this.mtxtNetmaskCurrent.AllowInternalTab = false;
            this.mtxtNetmaskCurrent.AutoHeight = true;
            this.mtxtNetmaskCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtNetmaskCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtNetmaskCurrent.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtNetmaskCurrent.Location = new System.Drawing.Point(105, 82);
            this.mtxtNetmaskCurrent.Margin = new System.Windows.Forms.Padding(0);
            this.mtxtNetmaskCurrent.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtNetmaskCurrent.Name = "mtxtNetmaskCurrent";
            this.mtxtNetmaskCurrent.ReadOnly = false;
            this.mtxtNetmaskCurrent.Size = new System.Drawing.Size(115, 22);
            this.mtxtNetmaskCurrent.TabIndex = 2;
            this.mtxtNetmaskCurrent.Text = "...";
            this.mtxtNetmaskCurrent.Click += new System.EventHandler(this.mtxtNetmaskCurrent_Click);
            // 
            // mtxtIPAddressCurrent
            // 
            this.mtxtIPAddressCurrent.AllowInternalTab = false;
            this.mtxtIPAddressCurrent.AutoHeight = true;
            this.mtxtIPAddressCurrent.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtIPAddressCurrent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtIPAddressCurrent.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtIPAddressCurrent.Location = new System.Drawing.Point(105, 45);
            this.mtxtIPAddressCurrent.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.mtxtIPAddressCurrent.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtIPAddressCurrent.Name = "mtxtIPAddressCurrent";
            this.mtxtIPAddressCurrent.ReadOnly = false;
            this.mtxtIPAddressCurrent.Size = new System.Drawing.Size(115, 22);
            this.mtxtIPAddressCurrent.TabIndex = 1;
            this.mtxtIPAddressCurrent.Text = "...";
            this.mtxtIPAddressCurrent.Click += new System.EventHandler(this.mtxtIPAddressCurrent_Click);
            // 
            // lblNetmaskCurrent
            // 
            this.lblNetmaskCurrent.AutoSize = true;
            this.lblNetmaskCurrent.Location = new System.Drawing.Point(1, 85);
            this.lblNetmaskCurrent.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lblNetmaskCurrent.Name = "lblNetmaskCurrent";
            this.lblNetmaskCurrent.Size = new System.Drawing.Size(94, 17);
            this.lblNetmaskCurrent.TabIndex = 0;
            this.lblNetmaskCurrent.Text = "Subnet Mask:";
            // 
            // lblIPAddressCurrent
            // 
            this.lblIPAddressCurrent.AutoSize = true;
            this.lblIPAddressCurrent.Location = new System.Drawing.Point(15, 48);
            this.lblIPAddressCurrent.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.lblIPAddressCurrent.Name = "lblIPAddressCurrent";
            this.lblIPAddressCurrent.Size = new System.Drawing.Size(80, 17);
            this.lblIPAddressCurrent.TabIndex = 0;
            this.lblIPAddressCurrent.Text = "IP Address:";
            // 
            // gbNewSettings
            // 
            this.gbNewSettings.AutoSize = true;
            this.gbNewSettings.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gbNewSettings.Controls.Add(this.mtxtNetmaskNew);
            this.gbNewSettings.Controls.Add(this.mtxtIPAddressNew);
            this.gbNewSettings.Controls.Add(this.lblNetmaskNew);
            this.gbNewSettings.Controls.Add(this.lblIPAddressNew);
            this.gbNewSettings.Location = new System.Drawing.Point(282, 15);
            this.gbNewSettings.Margin = new System.Windows.Forms.Padding(0, 0, 0, 30);
            this.gbNewSettings.Name = "gbNewSettings";
            this.gbNewSettings.Padding = new System.Windows.Forms.Padding(15, 30, 15, 15);
            this.gbNewSettings.Size = new System.Drawing.Size(235, 134);
            this.gbNewSettings.TabIndex = 0;
            this.gbNewSettings.TabStop = false;
            this.gbNewSettings.Text = "New Port 1 Settings";
            // 
            // mtxtNetmaskNew
            // 
            this.mtxtNetmaskNew.AllowInternalTab = false;
            this.mtxtNetmaskNew.AutoHeight = true;
            this.mtxtNetmaskNew.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtNetmaskNew.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtNetmaskNew.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtNetmaskNew.Location = new System.Drawing.Point(105, 82);
            this.mtxtNetmaskNew.Margin = new System.Windows.Forms.Padding(0);
            this.mtxtNetmaskNew.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtNetmaskNew.Name = "mtxtNetmaskNew";
            this.mtxtNetmaskNew.ReadOnly = false;
            this.mtxtNetmaskNew.Size = new System.Drawing.Size(115, 22);
            this.mtxtNetmaskNew.TabIndex = 4;
            this.mtxtNetmaskNew.Text = "...";
            // 
            // mtxtIPAddressNew
            // 
            this.mtxtIPAddressNew.AllowInternalTab = false;
            this.mtxtIPAddressNew.AutoHeight = true;
            this.mtxtIPAddressNew.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtIPAddressNew.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtIPAddressNew.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtIPAddressNew.Location = new System.Drawing.Point(105, 45);
            this.mtxtIPAddressNew.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.mtxtIPAddressNew.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtIPAddressNew.Name = "mtxtIPAddressNew";
            this.mtxtIPAddressNew.ReadOnly = false;
            this.mtxtIPAddressNew.Size = new System.Drawing.Size(115, 22);
            this.mtxtIPAddressNew.TabIndex = 3;
            this.mtxtIPAddressNew.Text = "...";
            // 
            // lblNetmaskNew
            // 
            this.lblNetmaskNew.AutoSize = true;
            this.lblNetmaskNew.Location = new System.Drawing.Point(1, 85);
            this.lblNetmaskNew.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lblNetmaskNew.Name = "lblNetmaskNew";
            this.lblNetmaskNew.Size = new System.Drawing.Size(94, 17);
            this.lblNetmaskNew.TabIndex = 0;
            this.lblNetmaskNew.Text = "Subnet Mask:";
            // 
            // lblIPAddressNew
            // 
            this.lblIPAddressNew.AutoSize = true;
            this.lblIPAddressNew.Location = new System.Drawing.Point(15, 48);
            this.lblIPAddressNew.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.lblIPAddressNew.Name = "lblIPAddressNew";
            this.lblIPAddressNew.Size = new System.Drawing.Size(80, 17);
            this.lblIPAddressNew.TabIndex = 0;
            this.lblIPAddressNew.Text = "IP Address:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 179);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(423, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "NOTE: Port 2 and other settings can be configured in a later step.";
            // 
            // ChangeDefaultIPAddress3
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(532, 530);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbNewSettings);
            this.Controls.Add(this.gbCurrentSettings);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ChangeDefaultIPAddress3";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change default IP address - Enter IP address";
            this.Load += new System.EventHandler(this.ChangeDefaultIPAddress3_Load);
            this.gbCurrentSettings.ResumeLayout(false);
            this.gbCurrentSettings.PerformLayout();
            this.gbNewSettings.ResumeLayout(false);
            this.gbNewSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.GroupBox gbCurrentSettings;
        private System.Windows.Forms.GroupBox gbNewSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNetmaskCurrent;
        private System.Windows.Forms.Label lblIPAddressCurrent;
        private System.Windows.Forms.Label lblNetmaskNew;
        private System.Windows.Forms.Label lblIPAddressNew;
        private IPAddressControlLib.IPAddressControl mtxtIPAddressCurrent;
        private IPAddressControlLib.IPAddressControl mtxtNetmaskCurrent;
        private IPAddressControlLib.IPAddressControl mtxtIPAddressNew;
        private IPAddressControlLib.IPAddressControl mtxtNetmaskNew;
    }
}