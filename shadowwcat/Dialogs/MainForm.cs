﻿using shadowwcat.Data;
using shadowwcat.Dialogs;
using shadowwcat.Utils;
using System;
using System.IO;
using System.ServiceProcess;
using System.Windows.Forms;

namespace shadowwcat
{
    public partial class MainForm : Form
    {
        private Boolean validInstall = true;

        public MainForm()
        {
            InitializeComponent();
            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                SetFormState();
            }
        }

        private Boolean maybeStartService(string name, int timeoutMs)
        {
            ServiceController svc = new ServiceController(name);

            if (svc.Status == ServiceControllerStatus.Stopped)
            {
                Logger.Log(Logger.Levels.Info, name + " is not running, will try to start.");
                try
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMs);

                    svc.Start();
                    svc.WaitForStatus(ServiceControllerStatus.Running, timeout);
                    Logger.Log(Logger.Levels.Info, name + " has been started.");
                }
                catch
                {
                    Logger.Log(Logger.Levels.Info, "Could not start " + name);
                    return false;
                }
            }

            return true;
        }

        private Boolean RequiredServicesRunning()
        {
#if DEBUG
            if (!maybeStartService("ssdb-master", 10000) || !maybeStartService("SSES-MASTER", 5000) || !maybeStartService("ssdm-master", 1000))
#else
            if (!maybeStartService("SynapSense DB", 10000) || !maybeStartService("SYNAPSERVER", 5000) || !maybeStartService("synapdm", 1000))
#endif
            {
                return false;
            }

            return true;
        }

        private Boolean CheckDirectoryForControlTransport(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Logger.Log(Logger.Levels.Info, directory + "doesn't exist.");
                return false;
            }

            string[] files = Directory.GetFiles(directory);
            foreach (string fileName in files)
            {
                if (fileName.Contains("dm-control-transport") && fileName.EndsWith(".jar"))
                {
                    Logger.Log(Logger.Levels.Info, "Found 'dm-control-transport*.jar' in " + directory + ".");
                    return true;
                }
            }

            Logger.Log(Logger.Levels.Info, directory + "doesn't contain 'dm-control-transport*.jar'.");
            return false;
        }

        private bool ControlTransportInstalled()
        {
#if DEBUG
            Logger.Log(Logger.Levels.Info, "Always assume DEBUG version has control transport installed.");
            return true;
#else
            string installDir = AppSession.ClusterConfig.InstallDir();
            Logger.Log(Logger.Levels.Info, "Synapsoft installation directory = " + installDir + ".");
            if (string.IsNullOrEmpty(installDir))
            {
                return false;
            }

            if (!File.Exists(installDir + "/SynapDM/conf/controltransport.properties")) {
                Logger.Log(Logger.Levels.Info, "/SynapDM/conf/controltransport.properties doesn't exist.");
                return false;
            }

            if (this.CheckDirectoryForControlTransport(installDir + "/SynapDM/lib"))
            {
                return true;
            }

            if (this.CheckDirectoryForControlTransport(installDir + "/SynapDM/lib/dm-control-transport"))
            {
                return true;
            }

            return false;
#endif
        }

        private void MainForm_Shown(Object sender, EventArgs e)
        {
            ESHostInfo hostInfo = AppSession.ClusterConfig.HostInfo;
            if (string.IsNullOrEmpty(hostInfo.DomainName))
            {
                this.validInstall = false;

                string strMessage = "The Environmental Server host name must include a domain. Please correct this and re-run the Control Admin Tool.";
                MessageBox.Show(strMessage, "Control Admin Tool Error");
                Logger.Log(Logger.Levels.Error, strMessage);
            }

            if (!ControlTransportInstalled())
            {
                this.validInstall = false;

                string strMessage = "The Control Transport must be installed so ActiveControl can communicate with the Environmental Server. Please correct this and re-run the Control Admin Tool.";
                MessageBox.Show(strMessage, "Control Admin Tool Error");
                Logger.Log(Logger.Levels.Error, strMessage);
            }

            if (!RequiredServicesRunning())
            {
                this.validInstall = false;

                string strMessage = "The Device Manager is not running, so ActiveControl will not be able to communicate with the Environmental Server. Please correct this and re-run the Control Admin Tool.";
                MessageBox.Show(strMessage, "Control Admin Tool Error");
                Logger.Log(Logger.Levels.Error, strMessage);
            }

            if (!this.validInstall)
            {
                SetFormState();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (AppSession.ClusterConfig.IsSuccessfulAvailable)
            {
                AppSession.ClusterConfig.LoadLastSuccessful();
            }
            else
            {
                AppSession.ClusterConfig.LoadProvisional();
            }

            toolTip1.SetToolTip(btnSerialConsole, "Interact with the Serial Console for an appliance.");
            toolTip1.SetToolTip(btnChangePassword, "Change the appliance's password used for configuration changes. May be run from a directly connected laptop\r\nor the Environmental Server.");
            toolTip1.SetToolTip(btnClusterHealth, "Test the control appliances and network configuration for common problems. The report can be saved for use\r\nin further diagnostics.");
            toolTip1.SetToolTip(btnConfigureCluster, "Complete the appliance configuration so that a pair of appliances can begin executing the control function.\r\nAppliances must be attached to the network and have received their initial IP address.");
            toolTip1.SetToolTip(btnRestoreCluster, "Configure a new control appliance to join the running control cluster. The new appliance must have the correct\r\nPort 1 IP address and be connected to the network.");
            toolTip1.SetToolTip(btnResumeUpgrade, "Complete the cluster upgrade process following a successful upgrade of the Environmental Server.");
            toolTip1.SetToolTip(btnUpgradeCluster, "Perform either a firmware-only cluster upgrade or a full platform upgrade.");
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            Navigator.GoTo(Navigator.DialogName.MainForm, Navigator.DialogName.ChangePassword);
        }

        private void btnConfigureCluster_Click(object sender, EventArgs e)
        {
            Navigator.GoTo(Navigator.DialogName.MainForm, Navigator.DialogName.ConfigureCluster1);
        }

        private void btnUpgradeCluster_Click(object sender, EventArgs e)
        {
            Navigator.GoTo(Navigator.DialogName.MainForm, Navigator.DialogName.UpgradeFirmware1);
        }

        private void btnResumeUpgrade_Click(object sender, EventArgs e)
        {
            AppSession.verifyType = AppSession.VERIFY_TYPE.RESUME_FULL_UPGRADE;
            Navigator.GoTo(Navigator.DialogName.MainForm, Navigator.DialogName.VerifyClusterChange);
        }

        private void btnClusterHealth_Click(object sender, EventArgs e)
        {
            Navigator.GoTo(Navigator.DialogName.MainForm, Navigator.DialogName.ClusterHealth);
        }

        private void btnRestoreCluster_Click(object sender, EventArgs e)
        {
            Navigator.GoTo(Navigator.DialogName.MainForm, Navigator.DialogName.RestoreCluster1);
        }

        public void SetFormState()
        {
            ESHostInfo hostInfo = AppSession.ClusterConfig.HostInfo;


            this.btnSerialConsole.Enabled = this.validInstall && !AppSession.IsResumeAfterUpgrade;
            this.btnChangePassword.Enabled = this.validInstall && !AppSession.IsResumeAfterUpgrade;
            this.btnConfigureCluster.Enabled = this.validInstall && AppSession.IsEnvironmentalServer && !AppSession.IsResumeAfterUpgrade;
            this.btnClusterHealth.Enabled = this.validInstall && AppSession.IsEnvironmentalServer && !AppSession.IsResumeAfterUpgrade;
            this.btnRestoreCluster.Enabled = this.validInstall && AppSession.IsEnvironmentalServer && !AppSession.IsResumeAfterUpgrade;
            this.btnResumeUpgrade.Enabled = this.validInstall && AppSession.IsEnvironmentalServer && AppSession.IsResumeAfterUpgrade;
            this.btnUpgradeCluster.Enabled = this.validInstall && AppSession.ClusterConfig.IsSuccessfulAvailable && AppSession.IsEnvironmentalServer && !AppSession.IsResumeAfterUpgrade;
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
        }

        private void lnkAbout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form about = new About();
            about.ShowDialog();
        }

        private void btnSerialConsole_Click(object sender, EventArgs e)
        {
            Form serialConsole = new SerialConsole1();
            serialConsole.Show();
        }
    }
}
