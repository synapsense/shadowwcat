﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using shadowwcat.Utils;

namespace shadowwcat.Dialogs
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnOK;
            }
        }


        private void About_Load(object sender, EventArgs e)
        {
            rtfText.Rtf = File.ReadAllText("license.rtf");
        }

        private void lnk3rdparty_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("notepad", "Readme.txt");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Could not retrieve 3rd party software info file");
                Logger.Log(Logger.Levels.Error, exc.Message);
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {

        }
    }
}
