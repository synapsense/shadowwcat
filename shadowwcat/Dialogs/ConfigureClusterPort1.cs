﻿using IPAddressControlLib;
using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ConfigureClusterPort1 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ConfigureClusterPort1;

        public ConfigureClusterPort1()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                SetFormValues();
                this.ActiveControl = mtxtPort1Netmask;
            }
        }

        private void SetFormValues()
        {

            ESHostInfo.NetworkInfoDictionary dict = AppSession.ClusterConfig.NetworkInfo;
            ESHostInfo.NETWORK_INFO ni;
            bool foundIP = dict.TryGetValue(AppSession.ClusterConfig.ES_IPAddress, out ni);

            mtxtPort1DefaultGateway.Text = string.IsNullOrEmpty(AppSession.ClusterConfig.Net1_Gateway) ? ni.defaultGateway : AppSession.ClusterConfig.Net1_Gateway;
            mtxtPort1Netmask.Text = string.IsNullOrEmpty(AppSession.ClusterConfig.Net1_Netmask) ? ni.netmask : AppSession.ClusterConfig.Net1_Netmask;

            txtController1Hostname.Text = AppSession.ClusterConfig.Appliance_1_Hostname;
            mtxtController1Port1IPAddress.Text = AppSession.ClusterConfig.Appliance_1_Port1_IP;

            txtController2Hostname.Text = AppSession.ClusterConfig.Appliance_2_Hostname;
            mtxtController2Port1IPAddress.Text = AppSession.ClusterConfig.Appliance_2_Port1_IP;

            mtxtController1Port1IPAddress.Enabled = !AppSession.UseDNS;
            mtxtController2Port1IPAddress.Enabled = !AppSession.UseDNS;
        }

        private bool ValidFormData()
        {
            errorProvider1.Clear();

            if (!Utilities.VerifyIPAddress(Utilities.IPToString(mtxtPort1DefaultGateway)))
            {
                errorProvider1.SetError(mtxtPort1DefaultGateway, "Default gateway is empty, or not a valid IP address.");
                return false;
            }

            if (!Utilities.VerifyIPAddress(Utilities.IPToString(mtxtPort1Netmask)))
            {
                errorProvider1.SetError(mtxtPort1Netmask, "Subnet mask is empty, or not a valid IP address");
                return false;
            }

            if (!Utilities.VerifyHostName(txtController1Hostname.Text))
            {
                errorProvider1.SetError(txtController1Hostname, "Invalid host name.");
                return false;
            }

            if (!Utilities.VerifyHostName(txtController2Hostname.Text))
            {
                errorProvider1.SetError(txtController2Hostname, "Invalid host name.");
                return false;
            }

            if (!AppSession.UseDNS)
            {
                if (!Utilities.VerifyIPAddress(Utilities.IPToString(mtxtController1Port1IPAddress)))
                {
                    errorProvider1.SetError(mtxtController1Port1IPAddress, "IP address is invalid.");
                    return false;
                }

                if (!Utilities.VerifyIPAddress(Utilities.IPToString(mtxtController2Port1IPAddress)))
                {
                    errorProvider1.SetError(mtxtController2Port1IPAddress, "IP address is invalid.");
                    return false;
                }
            }

            return true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (ValidFormData())
            {
                AppSession.ClusterConfig.Appliance_1_Hostname = txtController1Hostname.Text;
                AppSession.ClusterConfig.Appliance_2_Hostname = txtController2Hostname.Text;
                AppSession.ClusterConfig.Net1_Gateway = Utilities.IPToString(mtxtPort1DefaultGateway);
                AppSession.ClusterConfig.Net1_Netmask = Utilities.IPToString(mtxtPort1Netmask);
                AppSession.ClusterConfig.Net1_DHCP = false;
                AppSession.ClusterConfig.Appliance_1_Port1_IP = AppSession.UseDNS ? "" : Utilities.IPToString(mtxtController1Port1IPAddress);
                AppSession.ClusterConfig.Appliance_2_Port1_IP = AppSession.UseDNS ? "" : Utilities.IPToString(mtxtController2Port1IPAddress);

                if (AppSession.IsConfigureClusterBridged)
                {
                    Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterPort2);
                }
                else if (!AppSession.UseDNS) // Not using DNS. Update hosts file. MANDATORY.
                {
                    if (!HostFileGen.CreateUpdatedHostsFile()) // Create a prestaged hosts file.
                    {
                        MessageBox.Show(HostFileGen.ErrorMessage);
                        return;
                    }

                    Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterReviewHosts);
                }
                else
                {
                    Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterReview);
                }
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.Back(me);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        private void lnkResetValues_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Utilities.ResetValues(this.me);
        }

    }
}
