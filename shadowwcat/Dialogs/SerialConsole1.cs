﻿using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class SerialConsole1 : Form
    {
        private int lineCount = 0;

        public SerialConsole1()
        {
            InitializeComponent();

            _serialPort.ReceivedBytesThreshold = 6;

            this.FormClosed += new FormClosedEventHandler(this.Form_Closed);
        }

        private void SerialConsole1_Load(object sender, EventArgs e)
        {
            this.ActiveControl = btnDone;

            SetFormValues();
        }

        private void Form_Closed(object sender, FormClosedEventArgs e)
        {
            _serialPort.Close();
        }

        private void SetFormValues()
        {
            cmboPortName.Items.Clear();
            foreach (string s in SerialPort.GetPortNames())
            {
                cmboPortName.Items.Add(s);
            }
            cmboPortName.SelectedIndex= 0;
        }

        private void cmboPortName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_serialPort.IsOpen)
            {
                _serialPort.Close();
                txtPortOutput.Text = "";
                lineCount = 0;
                errorProvider1.Clear();

                this.Text = "Serial Console";
            }

            string portName = cmboPortName.SelectedItem.ToString();
            _serialPort.PortName = portName;
            try
            {
                _serialPort.Open();
                _serialPort.WriteLine("");

                this.Text = "Serial Console - " + portName;
                txtToSend.Focus();
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show(portName + " is already open. Please choose a different port.");
            }
            catch (TimeoutException)
            {
                MessageBox.Show(portName + " does not seem to be connected to an Active Control appliance. Please choose a different port.");
            }
        }

        private void textToSend_TextChanged(object sender, EventArgs e)
        {
            btnSend.Enabled = true;  //(txtToSend.TextLength > 0);
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            _serialPort.WriteLine(txtToSend.Text);
            txtToSend.ResetText();
            txtToSend.Focus();
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (_serialPort.IsOpen)
            {
                try
                {
                    string message = _serialPort.ReadLine();
                    txtPortOutput.AppendText(message);
                }
                catch (TimeoutException)
                {
                    try
                    {
                        string message = _serialPort.ReadExisting();
                        message += _serialPort.NewLine;
                        txtPortOutput.AppendText(message);
                    }
                    catch (TimeoutException) {}
                }

                lineCount++;
                if (lineCount >= 550)
                {
                    // Remove top 100 lines
                    string AllText = txtPortOutput.Text;
                    int index = 0;
                    int removed = 0;
                    while ((index = AllText.IndexOf(_serialPort.NewLine, index)) != -1 && removed <= 100)
                    {
                        removed++;
                    }
                    txtPortOutput.Text = AllText.Substring(index + 1);
                }

                txtPortOutput.Select(txtPortOutput.TextLength, 0);
                txtPortOutput.ScrollToCaret();
            }
        }

        private void _serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            string error = _serialPort.ReadExisting();
            Logger.Log(Logger.Levels.Info, "_serialPort_ErrorReceived(): " + error);
            errorProvider1.SetError(txtPortOutput, error);
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
