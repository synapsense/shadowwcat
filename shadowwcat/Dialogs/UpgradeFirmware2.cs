﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using shadowwcat.Data;
using System.IO;
using shadowwcat.ApplianceOps;

namespace shadowwcat.Dialogs
{
    public partial class UpgradeFirmware2 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.UpgradeFirmware2;

        public UpgradeFirmware2()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = txtFirmwareImageFilename;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.Back(me);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtFirmwareImageFilename.Clear();
            lblVersionInfo.Text = "Select a firmware image file and Upgrade Type, then press Next.";
            pictureBoxStop.Visible = false;
            Navigator.Cancel(me);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            AppSession.ApplianceFirmwareFile = txtFirmwareImageFilename.Text;
            AppSession.verifyType = rbFullPlatformUpgrade.Checked == true ? AppSession.VERIFY_TYPE.FULL_UPGRADE : AppSession.VERIFY_TYPE.FIRMWARE_ONLY_UPGRADE;

            Navigator.Next(me);
        }

        private void UpgradeFirmware2_Load(object sender, EventArgs e)
        {
            rbFirmwareOnlyUpgrade.Checked = true;
            pictureBoxStop.Visible = false;
        }

        private void btnBrowseFirmware_Click(object sender, EventArgs e)
        {
            if (openFileDialogFirmware.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFirmwareImageFilename.Text = openFileDialogFirmware.FileName;
                if (!File.Exists(txtFirmwareImageFilename.Text)) //This is redundant for now since we're only taking return values from the file chooser. Could change, though.
                {
                    lblVersionInfo.Text = string.Format("Name '{0}' is invalid. Try again.", txtFirmwareImageFilename.Text);
                    btnNext.Enabled = false;
                }
                else
                {
                    VersionCheck();
                }
            }
        }

        private void UpgradeFirmware2_Activated(object sender, EventArgs e)
        {
        }

        private void VersionCheck()
        {
            btnNext.Enabled = false;
            string FirmwareFileVersion = string.Empty;
            pictureBoxStop.Visible = false;

            if (String.IsNullOrEmpty(txtFirmwareImageFilename.Text))
            {
                lblVersionInfo.Text = "Select a firmware image file and Upgrade Type, then press Next.";
            }
            else
            {
                if (Utils.VersionOps.IsFWVersionCompatible(txtFirmwareImageFilename.Text))
                {
                    lblVersionInfo.Text = string.Format("This control cluster firmware image (v{0}) is compatible with current Environmental Server v{1}.",
                        Utils.VersionOps.FirmwareVersion, AppSession.EnvironmentalServerVersion);
                    if (rbFirmwareOnlyUpgrade.Checked)
                    {
                        lblVersionInfo.Text += " A firmware only upgrade is possible.";
                    }
                    else if (rbFullPlatformUpgrade.Checked)
                    {
                        lblVersionInfo.Text += " A full platform upgrade is possible.";
                    }
                    btnNext.Enabled = true;
                }
                else
                {
                    if (rbFirmwareOnlyUpgrade.Checked)
                    {
                        pictureBoxStop.Visible = true;
                        lblVersionInfo.Text = string.Format("{0}. Choose a different firmware image file.", Utils.VersionOps.ErrorMessage);
                    }
                    else if (rbFullPlatformUpgrade.Checked)
                    {
                        lblVersionInfo.Text = string.Format("This control cluster firmware image (v{0}) requires you to upgrade the Environmental Server to v{1} or later as part of the full platform upgrade.",
                            Utils.VersionOps.FirmwareVersion, Utils.VersionOps.FirmwareESMinVersion);
                    }
                }
            }
        }

        private void rbFirmwareOnlyUpgrade_CheckedChanged(object sender, EventArgs e)
        {
            VersionCheck();
        }

        private void rbFullPlatformUpgrade_CheckedChanged(object sender, EventArgs e)
        {
            VersionCheck();
        }

    }
}
