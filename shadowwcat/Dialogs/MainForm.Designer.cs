﻿namespace shadowwcat
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.btnConfigureCluster = new System.Windows.Forms.Button();
            this.btnUpgradeCluster = new System.Windows.Forms.Button();
            this.btnResumeUpgrade = new System.Windows.Forms.Button();
            this.btnRestoreCluster = new System.Windows.Forms.Button();
            this.btnClusterHealth = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lnkAbout = new System.Windows.Forms.LinkLabel();
            this.btnSerialConsole = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePassword.FlatAppearance.BorderSize = 0;
            this.btnChangePassword.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnChangePassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePassword.Location = new System.Drawing.Point(93, 32);
            this.btnChangePassword.Margin = new System.Windows.Forms.Padding(85, 4, 85, 4);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(293, 31);
            this.btnChangePassword.TabIndex = 1;
            this.btnChangePassword.Text = "Change &password >";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // btnConfigureCluster
            // 
            this.btnConfigureCluster.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfigureCluster.FlatAppearance.BorderSize = 0;
            this.btnConfigureCluster.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnConfigureCluster.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfigureCluster.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfigureCluster.Location = new System.Drawing.Point(93, 82);
            this.btnConfigureCluster.Margin = new System.Windows.Forms.Padding(85, 4, 85, 4);
            this.btnConfigureCluster.Name = "btnConfigureCluster";
            this.btnConfigureCluster.Size = new System.Drawing.Size(293, 31);
            this.btnConfigureCluster.TabIndex = 2;
            this.btnConfigureCluster.Text = "&Configure cluster >";
            this.btnConfigureCluster.UseVisualStyleBackColor = true;
            this.btnConfigureCluster.Click += new System.EventHandler(this.btnConfigureCluster_Click);
            // 
            // btnUpgradeCluster
            // 
            this.btnUpgradeCluster.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpgradeCluster.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnUpgradeCluster.FlatAppearance.BorderSize = 0;
            this.btnUpgradeCluster.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnUpgradeCluster.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpgradeCluster.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpgradeCluster.Location = new System.Drawing.Point(93, 131);
            this.btnUpgradeCluster.Margin = new System.Windows.Forms.Padding(85, 4, 85, 4);
            this.btnUpgradeCluster.Name = "btnUpgradeCluster";
            this.btnUpgradeCluster.Size = new System.Drawing.Size(293, 31);
            this.btnUpgradeCluster.TabIndex = 3;
            this.btnUpgradeCluster.Text = "&Upgrade cluster >";
            this.btnUpgradeCluster.UseVisualStyleBackColor = false;
            this.btnUpgradeCluster.Click += new System.EventHandler(this.btnUpgradeCluster_Click);
            // 
            // btnResumeUpgrade
            // 
            this.btnResumeUpgrade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResumeUpgrade.FlatAppearance.BorderSize = 0;
            this.btnResumeUpgrade.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnResumeUpgrade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResumeUpgrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResumeUpgrade.Location = new System.Drawing.Point(93, 180);
            this.btnResumeUpgrade.Margin = new System.Windows.Forms.Padding(85, 4, 85, 4);
            this.btnResumeUpgrade.Name = "btnResumeUpgrade";
            this.btnResumeUpgrade.Size = new System.Drawing.Size(293, 31);
            this.btnResumeUpgrade.TabIndex = 4;
            this.btnResumeUpgrade.Text = "&Resume after full upgrade >";
            this.btnResumeUpgrade.UseVisualStyleBackColor = true;
            this.btnResumeUpgrade.Click += new System.EventHandler(this.btnResumeUpgrade_Click);
            // 
            // btnRestoreCluster
            // 
            this.btnRestoreCluster.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestoreCluster.FlatAppearance.BorderSize = 0;
            this.btnRestoreCluster.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnRestoreCluster.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRestoreCluster.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRestoreCluster.Location = new System.Drawing.Point(93, 276);
            this.btnRestoreCluster.Margin = new System.Windows.Forms.Padding(85, 4, 85, 4);
            this.btnRestoreCluster.Name = "btnRestoreCluster";
            this.btnRestoreCluster.Size = new System.Drawing.Size(293, 31);
            this.btnRestoreCluster.TabIndex = 6;
            this.btnRestoreCluster.Text = "Rest&ore cluster >";
            this.btnRestoreCluster.UseVisualStyleBackColor = true;
            this.btnRestoreCluster.Click += new System.EventHandler(this.btnRestoreCluster_Click);
            // 
            // btnClusterHealth
            // 
            this.btnClusterHealth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClusterHealth.FlatAppearance.BorderSize = 0;
            this.btnClusterHealth.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnClusterHealth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClusterHealth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClusterHealth.Location = new System.Drawing.Point(93, 228);
            this.btnClusterHealth.Margin = new System.Windows.Forms.Padding(85, 4, 85, 4);
            this.btnClusterHealth.Name = "btnClusterHealth";
            this.btnClusterHealth.Size = new System.Drawing.Size(293, 31);
            this.btnClusterHealth.TabIndex = 5;
            this.btnClusterHealth.Text = "Cluster &health report >";
            this.btnClusterHealth.UseVisualStyleBackColor = true;
            this.btnClusterHealth.Click += new System.EventHandler(this.btnClusterHealth_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(64, 396);
            this.label1.Margin = new System.Windows.Forms.Padding(55, 0, 55, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(355, 57);
            this.label1.TabIndex = 15;
            this.label1.Text = "The SynapSense ControlAdmin tool is used for the initial setup, administration, a" +
    "nd upgrade of control appliance clusters.";
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 20000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // lnkAbout
            // 
            this.lnkAbout.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lnkAbout.AutoSize = true;
            this.lnkAbout.Location = new System.Drawing.Point(217, 484);
            this.lnkAbout.Margin = new System.Windows.Forms.Padding(15, 25, 15, 15);
            this.lnkAbout.Name = "lnkAbout";
            this.lnkAbout.Size = new System.Drawing.Size(45, 17);
            this.lnkAbout.TabIndex = 16;
            this.lnkAbout.TabStop = true;
            this.lnkAbout.Text = "&About";
            this.lnkAbout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAbout_LinkClicked);
            // 
            // btnSerialConsole
            // 
            this.btnSerialConsole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSerialConsole.FlatAppearance.BorderSize = 0;
            this.btnSerialConsole.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSerialConsole.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSerialConsole.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSerialConsole.Location = new System.Drawing.Point(93, 315);
            this.btnSerialConsole.Margin = new System.Windows.Forms.Padding(85, 4, 85, 4);
            this.btnSerialConsole.Name = "btnSerialConsole";
            this.btnSerialConsole.Size = new System.Drawing.Size(293, 31);
            this.btnSerialConsole.TabIndex = 17;
            this.btnSerialConsole.Text = "&Serial console >";
            this.btnSerialConsole.UseVisualStyleBackColor = true;
            this.btnSerialConsole.Click += new System.EventHandler(this.btnSerialConsole_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(483, 526);
            this.Controls.Add(this.btnSerialConsole);
            this.Controls.Add(this.lnkAbout);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClusterHealth);
            this.Controls.Add(this.btnRestoreCluster);
            this.Controls.Add(this.btnResumeUpgrade);
            this.Controls.Add(this.btnUpgradeCluster);
            this.Controls.Add(this.btnConfigureCluster);
            this.Controls.Add(this.btnChangePassword);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SynapSense ControlAdmin";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.Button btnConfigureCluster;
        private System.Windows.Forms.Button btnUpgradeCluster;
        private System.Windows.Forms.Button btnResumeUpgrade;
        private System.Windows.Forms.Button btnRestoreCluster;
        private System.Windows.Forms.Button btnClusterHealth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.LinkLabel lnkAbout;
        private System.Windows.Forms.Button btnSerialConsole;
    }
}

