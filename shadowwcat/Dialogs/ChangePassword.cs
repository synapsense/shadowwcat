﻿using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ChangePassword : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ChangePassword;

        public ChangePassword()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = txtIPAddress;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            bool badPassword = false;

            if (string.IsNullOrEmpty(txtOldPassword.Text))
            {
                errorProviderPassword.SetError(txtOldPassword, "Password cannot be blank.");
                badPassword = true;
            }
            if (string.IsNullOrEmpty(txtNewPassword.Text) ||                
                (!chkShowPassword.Checked && string.IsNullOrEmpty(txtNewPasswordConfirm.Text)) || 
                ((!chkShowPassword.Checked &&  (txtNewPassword.Text != txtNewPasswordConfirm.Text))))
            {
                errorProviderPassword.SetError(txtNewPassword, "Passwords must match and cannot be blank.");
                badPassword = true;
            }

            if (!badPassword)
            {
                errorProviderIPAddress.Clear();
                errorProviderPassword.Clear();

                if (!IsValidIPAddress(txtIPAddress.Text, out message))
                {
                    errorProviderIPAddress.SetError(txtIPAddress, message);
                }
                else
                {
                    AppSession.changePasswordInfo.hostName = txtIPAddress.Text;
                    AppSession.changePasswordInfo.IPAddress = txtIPAddress.Text;
                    AppSession.changePasswordInfo.oldPassword = txtOldPassword.Text;
                    AppSession.changePasswordInfo.newPassword = txtNewPassword.Text;

                    ClearFormData();
                    Navigator.Next(me);

                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFormData();
            Navigator.Cancel(me);
        }

        private void txtIPAddress_Leave(object sender, EventArgs e)
        {
            if (!txtIPAddress.Blank)
            {
                errorProviderIPAddress.SetError(txtIPAddress, "");
            }
        }

        private bool IsValidIPAddress(string IPAddress, out string message)
        {
            bool returnStatus = false;
            message = string.Empty;

            var ip = (txtIPAddress.Blank ? "" : txtIPAddress.Text);
            if (string.IsNullOrEmpty(ip))
            {
                message = "IP address is invalid.";
            }
            else
            {
                if (!Network.IsValidIPFormat(ip))
                {
                    message = "IP address has invalid format.";
                }
                else
                {
                    if (!(returnStatus = Network.IsPingable(ip)))
                    {
                        message = "Connect failed.";
                    }
                }
            }
            return returnStatus;
        }

        private void ClearFormData()
        {
            txtIPAddress.Clear();
            txtOldPassword.Clear();
            txtNewPassword.Clear();
            txtNewPasswordConfirm.Clear();

            errorProviderIPAddress.Clear();
            errorProviderPassword.Clear();
        }

        private void chkShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShowPassword.Checked)
            {
                txtOldPassword.UseSystemPasswordChar = false;
                txtOldPassword.PasswordChar = '\0';
                txtNewPassword.UseSystemPasswordChar = false;
                txtNewPassword.PasswordChar = '\0';
                txtNewPasswordConfirm.Text = "";
                txtNewPasswordConfirm.Enabled = false;
                txtNewPasswordConfirm.UseSystemPasswordChar = false;
                txtNewPasswordConfirm.PasswordChar = '\0';
            }
            else
            {
                txtNewPasswordConfirm.Enabled = true;
                txtNewPassword.UseSystemPasswordChar = true;
                txtNewPasswordConfirm.UseSystemPasswordChar = true;
                txtOldPassword.UseSystemPasswordChar = true;
            }
        }

        private void txtOldPassword_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtOldPassword.Text))
            {
                errorProviderPassword.SetError(txtOldPassword, "");
            }
        }

        private void txtNewPasswordConfirm_Leave(object sender, EventArgs e)
        {
            errorProviderPassword.SetError(txtNewPassword, "");
        }

    }
}
