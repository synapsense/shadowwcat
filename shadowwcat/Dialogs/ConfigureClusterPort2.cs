﻿using IPAddressControlLib;
using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ConfigureClusterPort2 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ConfigureClusterPort2;

        public ConfigureClusterPort2()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = mtxtPort2Netmask;

                SetFormValues();
            }
        }

        private void SetFormValues()
        {
            ESHostInfo.NetworkInfoDictionary dict = AppSession.ClusterConfig.NetworkInfo;
            ESHostInfo.NETWORK_INFO ni;
            dict.TryGetValue(AppSession.ClusterConfig.ES_IPAddress, out ni);

            mtxtPort2Netmask.Text = string.IsNullOrEmpty(AppSession.ClusterConfig.Net2_Netmask) ? ni.netmask : AppSession.ClusterConfig.Net2_Netmask;
            mtxtController1Port2IPAddress.Text = AppSession.ClusterConfig.Appliance_1_Port2_IP;
            mtxtController2Port2IPAddress.Text = AppSession.ClusterConfig.Appliance_2_Port2_IP;
        }

        private bool ValidFormData()
        {
            errorProvider1.Clear();

            if (!Utilities.VerifyIPAddress(Utilities.IPToString(mtxtPort2Netmask)))
            {
                errorProvider1.SetError(mtxtPort2Netmask, "Subnet mask is empty, or not a valid IP address");
                return false;
            }

            if (!Utilities.VerifyIPAddress(Utilities.IPToString(mtxtController1Port2IPAddress)))
            {
                errorProvider1.SetError(mtxtController1Port2IPAddress, "IP address is invalid.");
                return false;
            }

            if (!Utilities.VerifyIPAddress(Utilities.IPToString(mtxtController2Port2IPAddress)))
            {
                errorProvider1.SetError(mtxtController2Port2IPAddress, "IP address is invalid.");
                return false;
            }

            return true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (ValidFormData())
            {
                AppSession.ClusterConfig.Net2_Netmask = IPToString(mtxtPort2Netmask);
                AppSession.ClusterConfig.Net2_DHCP = false;
                AppSession.ClusterConfig.Appliance_1_Port2_IP = IPToString(mtxtController1Port2IPAddress);
                AppSession.ClusterConfig.Appliance_2_Port2_IP = IPToString(mtxtController2Port2IPAddress);

                if (!AppSession.UseDNS) // Not using DNS. Update hosts file. MANDATORY.
                {
                    if (!HostFileGen.CreateUpdatedHostsFile()) // Create a prestaged hosts file.
                    {
                        MessageBox.Show(HostFileGen.ErrorMessage);
                        return;
                    }

                    Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterReviewHosts);
                } else
                {
                    Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterReview);
                }
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.Back(me);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        /// <summary>
        /// The IPAddressControl customer control returns the string "..." when no value is entered by user. Need to really return an empty string.
        /// </summary>
        /// <param name="Control"></param>
        /// <returns></returns>
        private string IPToString(IPAddressControl Control)
        {
            return (Control.Blank ? "" : Control.Text);
        }

        private void lnkResetValues_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Utilities.ResetValues(this.me);
        }
    }
}
