﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using shadowwcat.Data;

namespace shadowwcat.Dialogs
{
    public partial class PasswordPrompt : Form
    {
        public PasswordPrompt()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = txtAppliance1Password;

            }
        }

        private void PasswordPrompt_Load(object sender, EventArgs e)
        {
            lblAppliance1Password.Text = AppSession.ClusterConfig.Appliance_1_Hostname;
            lblAppliance2Password.Text = AppSession.ClusterConfig.Appliance_2_Hostname;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            bool validationPassed = true;

            if (String.IsNullOrEmpty(txtAppliance1Password.Text))
            {
                errorProvider.SetError(txtAppliance1Password, "Password cannot be blank.");
                validationPassed = false;
            }

            if (!chkSamePwd.Checked)
            {
                if (String.IsNullOrEmpty(txtAppliance2Password.Text))
                {
                    errorProvider.SetError(txtAppliance2Password, "Password cannot be blank.");
                    validationPassed = false;
                }
            }
            else
            {
                txtAppliance2Password.Text = txtAppliance1Password.Text;
            }

            if (validationPassed)
            {
                AppSession.Appliance1Password = txtAppliance1Password.Text;
                AppSession.Appliance2Password = txtAppliance2Password.Text;
                AppSession.AppliancesHaveSamePassword = chkSamePwd.Checked;
                AppSession.RememberAppliancePasswordForSession = chkRememberPwd.Checked;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void PasswordPrompt_Activated(object sender, EventArgs e)
        {
            chkRememberPwd.Checked = AppSession.RememberAppliancePasswordForSession;

            if (chkRememberPwd.Checked)
            {
                txtAppliance1Password.Text = AppSession.Appliance1Password;
                txtAppliance2Password.Text = AppSession.Appliance2Password;
            }
            else
            {
                txtAppliance1Password.Clear();
                txtAppliance2Password.Clear();
            }

            // Do here so will clear appliance 2 password if using same password
            chkSamePwd.Checked = AppSession.AppliancesHaveSamePassword;

            errorProvider.SetError(txtAppliance1Password, "");
            errorProvider.SetError(txtAppliance2Password, "");
        }

        private void txtAppliance1Password_Enter(object sender, EventArgs e)
        {
            errorProvider.SetError(txtAppliance1Password, "");
        }

        private void txtAppliance2Password_Enter(object sender, EventArgs e)
        {
            errorProvider.SetError(txtAppliance2Password, "");
        }

        private void lblAppliance1Password_Click(object sender, EventArgs e)
        {

        }

        private void chkSamePwd_CheckedChanged(object sender, EventArgs e)
        {
            txtAppliance2Password.Enabled = !chkSamePwd.Checked;
            txtAppliance2Password.Text = "";
        }

        private void lblAppliance2Password_Click(object sender, EventArgs e)
        {

        }
    }
}
