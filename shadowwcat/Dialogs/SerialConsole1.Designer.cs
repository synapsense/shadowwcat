﻿namespace shadowwcat.Dialogs
{
    partial class SerialConsole1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SerialConsole1));
            this.txtPortOutput = new System.Windows.Forms.RichTextBox();
            this.cmboPortName = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtToSend = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this._serialPort = new System.IO.Ports.SerialPort(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPortOutput
            // 
            this.txtPortOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPortOutput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtPortOutput.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPortOutput.ForeColor = System.Drawing.Color.Yellow;
            this.txtPortOutput.Location = new System.Drawing.Point(19, 49);
            this.txtPortOutput.Margin = new System.Windows.Forms.Padding(10);
            this.txtPortOutput.Name = "txtPortOutput";
            this.txtPortOutput.ReadOnly = true;
            this.txtPortOutput.Size = new System.Drawing.Size(544, 356);
            this.txtPortOutput.TabIndex = 0;
            this.txtPortOutput.Text = "";
            // 
            // cmboPortName
            // 
            this.cmboPortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboPortName.FormattingEnabled = true;
            this.cmboPortName.Location = new System.Drawing.Point(151, 12);
            this.cmboPortName.Name = "cmboPortName";
            this.cmboPortName.Size = new System.Drawing.Size(121, 24);
            this.cmboPortName.TabIndex = 1;
            this.cmboPortName.SelectedIndexChanged += new System.EventHandler(this.cmboPortName_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Serial Port Name:";
            // 
            // txtToSend
            // 
            this.txtToSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtToSend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtToSend.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToSend.ForeColor = System.Drawing.Color.White;
            this.txtToSend.Location = new System.Drawing.Point(19, 420);
            this.txtToSend.Margin = new System.Windows.Forms.Padding(10);
            this.txtToSend.Name = "txtToSend";
            this.txtToSend.Size = new System.Drawing.Size(428, 25);
            this.txtToSend.TabIndex = 3;
            this.txtToSend.TextChanged += new System.EventHandler(this.textToSend_TextChanged);
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Location = new System.Drawing.Point(463, 418);
            this.btnSend.Margin = new System.Windows.Forms.Padding(0);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(100, 28);
            this.btnSend.TabIndex = 51;
            this.btnSend.Text = "&Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnDone
            // 
            this.btnDone.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnDone.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDone.Location = new System.Drawing.Point(242, 488);
            this.btnDone.Margin = new System.Windows.Forms.Padding(0);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(100, 28);
            this.btnDone.TabIndex = 52;
            this.btnDone.Text = "&Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // _serialPort
            // 
            this._serialPort.ReadTimeout = 100;
            this._serialPort.WriteTimeout = 100;
            this._serialPort.ErrorReceived += new System.IO.Ports.SerialErrorReceivedEventHandler(this._serialPort_ErrorReceived);
            this._serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this._serialPort_DataReceived);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // SerialConsole1
            // 
            this.AcceptButton = this.btnSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnDone;
            this.ClientSize = new System.Drawing.Size(582, 525);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtToSend);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmboPortName);
            this.Controls.Add(this.txtPortOutput);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1000, 1000);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "SerialConsole1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Serial Console";
            this.Load += new System.EventHandler(this.SerialConsole1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox txtPortOutput;
        private System.Windows.Forms.ComboBox cmboPortName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtToSend;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnDone;
        private System.IO.Ports.SerialPort _serialPort;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}