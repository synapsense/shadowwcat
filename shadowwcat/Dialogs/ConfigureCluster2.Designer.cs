﻿namespace shadowwcat.Dialogs
{
    partial class ConfigureCluster2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureCluster2));
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblWarning = new System.Windows.Forms.Label();
            this.txtESHostname = new System.Windows.Forms.TextBox();
            this.lblESHostname = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lnkResetValues = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmboESIPAddress = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lstNTP = new System.Windows.Forms.ListView();
            this.hostName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ipAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lstDNS2 = new System.Windows.Forms.ListView();
            this.dnsIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 532);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 52;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblWarning
            // 
            this.lblWarning.Location = new System.Drawing.Point(15, 283);
            this.lblWarning.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(566, 51);
            this.lblWarning.TabIndex = 0;
            this.lblWarning.Text = "Warning: If NTP is not available, the clock can \"drift\" over time and be minutes " +
    "or hours off. Manually resetting it can cause severe problems within Active Cont" +
    "rol.";
            // 
            // txtESHostname
            // 
            this.txtESHostname.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtESHostname.Location = new System.Drawing.Point(174, 15);
            this.txtESHostname.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.txtESHostname.Name = "txtESHostname";
            this.txtESHostname.ReadOnly = true;
            this.txtESHostname.Size = new System.Drawing.Size(250, 22);
            this.txtESHostname.TabIndex = 1;
            // 
            // lblESHostname
            // 
            this.lblESHostname.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblESHostname.Location = new System.Drawing.Point(66, 18);
            this.lblESHostname.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lblESHostname.Name = "lblESHostname";
            this.lblESHostname.Size = new System.Drawing.Size(98, 17);
            this.lblESHostname.TabIndex = 0;
            this.lblESHostname.Text = "ES Hostname:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(387, 532);
            this.btnBack.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 51;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(502, 532);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 50;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lnkResetValues
            // 
            this.lnkResetValues.Location = new System.Drawing.Point(262, 538);
            this.lnkResetValues.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.lnkResetValues.Name = "lnkResetValues";
            this.lnkResetValues.Size = new System.Drawing.Size(92, 17);
            this.lnkResetValues.TabIndex = 2;
            this.lnkResetValues.TabStop = true;
            this.lnkResetValues.Text = "Reset Values";
            this.lnkResetValues.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkResetValues_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 54;
            this.label1.Text = "ES IP Address:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 341);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(569, 54);
            this.label2.TabIndex = 0;
            this.label2.Text = "For this reason, at least one time server must be checked. If the ES Server is se" +
    "lected, it will be configured as a time server, and used by Active Control appli" +
    "ances to synchronize time.";
            // 
            // cmboESIPAddress
            // 
            this.cmboESIPAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmboESIPAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmboESIPAddress.FormattingEnabled = true;
            this.cmboESIPAddress.Location = new System.Drawing.Point(174, 52);
            this.cmboESIPAddress.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.cmboESIPAddress.Name = "cmboESIPAddress";
            this.cmboESIPAddress.Size = new System.Drawing.Size(143, 24);
            this.cmboESIPAddress.Sorted = true;
            this.cmboESIPAddress.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 94);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 17);
            this.label3.TabIndex = 59;
            this.label3.Text = "Domain Name Servers:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 179);
            this.label4.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 17);
            this.label4.TabIndex = 61;
            this.label4.Text = "Time Servers:";
            // 
            // lstNTP
            // 
            this.lstNTP.CheckBoxes = true;
            this.lstNTP.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hostName,
            this.ipAddress});
            this.lstNTP.FullRowSelect = true;
            this.lstNTP.GridLines = true;
            this.lstNTP.Location = new System.Drawing.Point(174, 179);
            this.lstNTP.Name = "lstNTP";
            this.lstNTP.Size = new System.Drawing.Size(308, 89);
            this.lstNTP.TabIndex = 62;
            this.lstNTP.UseCompatibleStateImageBehavior = false;
            this.lstNTP.View = System.Windows.Forms.View.Details;
            this.lstNTP.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lstNTP_ItemChecked);
            // 
            // hostName
            // 
            this.hostName.Text = "Host Name";
            this.hostName.Width = 200;
            // 
            // ipAddress
            // 
            this.ipAddress.Text = "IP Address";
            this.ipAddress.Width = 104;
            // 
            // lstDNS2
            // 
            this.lstDNS2.CheckBoxes = true;
            this.lstDNS2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.dnsIP});
            this.lstDNS2.FullRowSelect = true;
            this.lstDNS2.GridLines = true;
            this.lstDNS2.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lstDNS2.Location = new System.Drawing.Point(174, 94);
            this.lstDNS2.Name = "lstDNS2";
            this.lstDNS2.Size = new System.Drawing.Size(250, 69);
            this.lstDNS2.TabIndex = 63;
            this.lstDNS2.UseCompatibleStateImageBehavior = false;
            this.lstDNS2.View = System.Windows.Forms.View.Details;
            this.lstDNS2.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lstDNS2_ItemChecked);
            // 
            // dnsIP
            // 
            this.dnsIP.Text = "IP Address";
            this.dnsIP.Width = 246;
            // 
            // ConfigureCluster2
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(617, 575);
            this.ControlBox = false;
            this.Controls.Add(this.lstDNS2);
            this.Controls.Add(this.lstNTP);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmboESIPAddress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtESHostname);
            this.Controls.Add(this.lnkResetValues);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.lblESHostname);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ConfigureCluster2";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure cluster - Enter time server";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.TextBox txtESHostname;
        private System.Windows.Forms.Label lblESHostname;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.LinkLabel lnkResetValues;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmboESIPAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView lstNTP;
        private System.Windows.Forms.ColumnHeader hostName;
        private System.Windows.Forms.ColumnHeader ipAddress;
        private System.Windows.Forms.ListView lstDNS2;
        private System.Windows.Forms.ColumnHeader dnsIP;
    }
}