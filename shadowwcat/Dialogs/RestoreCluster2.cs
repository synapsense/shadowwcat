﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using shadowwcat.Data;

namespace shadowwcat.Dialogs
{
    public partial class RestoreCluster2 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.RestoreCluster2;

        public RestoreCluster2()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = txtClusterConfigFile;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Navigator.Execute(this, Navigator.TaskRequest.EnterAppliancePasswords))
            {
                Navigator.Next(me);
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.Back(me);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtClusterConfigFile.Clear();
            Navigator.Cancel(me);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialogClusterConfig.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtClusterConfigFile.Text = openFileDialogClusterConfig.FileName;
            }
        }

        private void RestoreCluster2_Load(object sender, EventArgs e)
        {
            openFileDialogClusterConfig.InitialDirectory = AppSession.CommonDataPath;
        }
    }
}
