﻿namespace shadowwcat.Dialogs
{
    partial class RestoreCluster4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestoreCluster4));
            this.lblVersionInfo = new System.Windows.Forms.Label();
            this.btnBrowseFirmware = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFirmwareImageFilename = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.openFileDialogFirmware = new System.Windows.Forms.OpenFileDialog();
            this.btnBack = new System.Windows.Forms.Button();
            this.pictureBoxStop = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStop)).BeginInit();
            this.SuspendLayout();
            // 
            // lblVersionInfo
            // 
            this.lblVersionInfo.Location = new System.Drawing.Point(15, 89);
            this.lblVersionInfo.Margin = new System.Windows.Forms.Padding(0, 15, 15, 15);
            this.lblVersionInfo.Name = "lblVersionInfo";
            this.lblVersionInfo.Size = new System.Drawing.Size(437, 65);
            this.lblVersionInfo.TabIndex = 0;
            this.lblVersionInfo.Text = "Appliance firmware version {0} is required in order to restore cluster reliabilit" +
    "y.";
            // 
            // btnBrowseFirmware
            // 
            this.btnBrowseFirmware.Location = new System.Drawing.Point(482, 34);
            this.btnBrowseFirmware.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.btnBrowseFirmware.Name = "btnBrowseFirmware";
            this.btnBrowseFirmware.Size = new System.Drawing.Size(100, 28);
            this.btnBrowseFirmware.TabIndex = 2;
            this.btnBrowseFirmware.Text = "&Browse...";
            this.btnBrowseFirmware.UseVisualStyleBackColor = true;
            this.btnBrowseFirmware.Click += new System.EventHandler(this.btnBrowseFirmware_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Firmware image:";
            // 
            // txtFirmwareImageFilename
            // 
            this.txtFirmwareImageFilename.Location = new System.Drawing.Point(18, 37);
            this.txtFirmwareImageFilename.Margin = new System.Windows.Forms.Padding(0, 0, 15, 15);
            this.txtFirmwareImageFilename.Name = "txtFirmwareImageFilename";
            this.txtFirmwareImageFilename.Size = new System.Drawing.Size(449, 22);
            this.txtFirmwareImageFilename.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 337);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(482, 337);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // openFileDialogFirmware
            // 
            this.openFileDialogFirmware.FileName = "appliance_update.img";
            this.openFileDialogFirmware.Filter = "Firmware images|*_update.img|All files|*.*";
            this.openFileDialogFirmware.ReadOnlyChecked = true;
            this.openFileDialogFirmware.RestoreDirectory = true;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(367, 337);
            this.btnBack.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 11;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // pictureBoxStop
            // 
            this.pictureBoxStop.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxStop.Image")));
            this.pictureBoxStop.Location = new System.Drawing.Point(482, 92);
            this.pictureBoxStop.Margin = new System.Windows.Forms.Padding(0, 15, 0, 15);
            this.pictureBoxStop.Name = "pictureBoxStop";
            this.pictureBoxStop.Size = new System.Drawing.Size(81, 62);
            this.pictureBoxStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStop.TabIndex = 34;
            this.pictureBoxStop.TabStop = false;
            // 
            // RestoreCluster4
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(597, 380);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBoxStop);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lblVersionInfo);
            this.Controls.Add(this.btnBrowseFirmware);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFirmwareImageFilename);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNext);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RestoreCluster4";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Restore Cluster - Select firmware image";
            this.Load += new System.EventHandler(this.RestoreCluster4_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblVersionInfo;
        private System.Windows.Forms.Button btnBrowseFirmware;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFirmwareImageFilename;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.OpenFileDialog openFileDialogFirmware;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.PictureBox pictureBoxStop;
    }
}