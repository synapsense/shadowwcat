﻿using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace shadowwcat.Dialogs
{
    public partial class ConfigureCluster3 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ConfigureCluster3;
        CommandRunner cr = new CommandRunner();
        private BackgroundWorker bwPauseBeforeStarting = new System.ComponentModel.BackgroundWorker();
        private bool startBackgroundThread = true;

        public ConfigureCluster3()
        {
            InitializeComponent();
            cr.TaskName = "Cluster Configuration";
            cr.TaskDetails = "";
            cr.StartButtonName = "Start Cluster Configuration";
            cr.CustomSuccessInstructions = "";
            cr.CustomErrorInstructions = "";

            cr.Status = lblStatus;
            cr.Progress = progressBar;
            cr.ProgressFail = lblError;
            cr.CommandList = lbCommands;
            cr.Details = txtDetails;
            cr.ElapsedTime = lblElapsedTime;
            cr.StartExecution = btnDone;
            cr.Done = btnDone;
            cr.Back = btnBack;
            cr.bw = bw;

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);

            this.bwPauseBeforeStarting.DoWork += new DoWorkEventHandler(bwPauseBeforeStarting_DoWork);
            this.bwPauseBeforeStarting.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwPauseBeforeStarting_RunWorkerCompleted);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                if (btnDone.Enabled)
                {
                    Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
                }
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnDone;

                if (this.startBackgroundThread)
                {
                    this.bwPauseBeforeStarting.RunWorkerAsync();
                    this.startBackgroundThread = false;
                }
            }
        }

        private void bwPauseBeforeStarting_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            AppSession.ClusterConfig.Save();
            //Debug aid -- MessageBox.Show("OK, go and edit the config file before proceeding, then dismiss this.");
            if (CreateAndRunTask())
            {
                AppSession.ClusterConfig.SaveAsLastSuccessful();
            }
            else
            {
                AppSession.ClusterConfig.SaveAsProvisional();
            }
        }

        private void bwPauseBeforeStarting_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(100);
        }

        private bool CreateAndRunTask()
        {
            cr.ClearRunData();
            Command c1 = new shadowwcat.ApplianceOps.Command();
            c1.ApplianceHostname = AppSession.ClusterConfig.Appliance_1_Hostname;
            c1.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_1_Port1_IP;
            c1.AppliancePassword = AppSession.Appliance1Password;
            c1.FirmwareImageFilename = AppSession.ApplianceFirmwareFile;
            c1.SiblingHostname = AppSession.ClusterConfig.Appliance_2_Hostname;

            Command c2 = new shadowwcat.ApplianceOps.Command();
            c2.ApplianceHostname = AppSession.ClusterConfig.Appliance_2_Hostname;
            c2.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_2_Port1_IP;
            c2.AppliancePassword = AppSession.Appliance2Password;
            c2.FirmwareImageFilename = AppSession.ApplianceFirmwareFile;
            c2.SiblingHostname = AppSession.ClusterConfig.Appliance_1_Hostname;

            lbCommands.BeginUpdate();
            lbCommands.Items.Clear();
            lbCommands.Items.Add(new CommandInfo("1. Test appliance connectivity...", new Action(c1.TestConnection), new Action(c2.TestConnection)));
            lbCommands.Items.Add(new CommandInfo("2. Copy the cluster config files...", new Action(c1.CopyClusterConfigurationFile), new Action(c2.CopyClusterConfigurationFile)));
            lbCommands.Items.Add(new CommandInfo("3. Validate appliance configuration...", new Action(c1.ValidateApplianceConfiguration), new Action(c2.ValidateApplianceConfiguration)));
            lbCommands.Items.Add(new CommandInfo("4. Test appliance configuration...", new Action(c1.TestApplianceConfiguration), new Action(c2.TestApplianceConfiguration)));
            lbCommands.Items.Add(new CommandInfo("5. Generate appliance configuration...", new Action(c1.GenerateApplianceConfiguration), new Action(c2.GenerateApplianceConfiguration)));
            lbCommands.Items.Add(new CommandInfo("6. Restarting the appliances...", new Action(c1.WaitForApplianceRestart), new Action(c2.WaitForApplianceRestart)));
            lbCommands.Items.Add(new CommandInfo("7. Checking cluster overall health...", new Action(c1.CommandClusterStable), new Action(c2.CommandClusterStable)));
            lbCommands.EndUpdate();
            lbCommands.DisplayMember = "CommandName"; //this property gets displayed in the listbox.
            return cr.PerformTask(c1, c2);
        }

        private void ConfigureCluster3_Load(object sender, EventArgs e)
        {
            cr.ClearRunData();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.startBackgroundThread = true;
            cr.ClearRunData();
            Navigator.Close(me);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.startBackgroundThread = true;
            cr.ClearRunData();
            if (AppSession.ClusterConfig.Nameserver.Length == 0) //'No DNS' mode. Update hosts file. MANDATORY.
            {
                Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterReviewHosts);
            }
            else
            {
                Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterReview);
            }
        }


        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            Command command = (Command)e.Argument;
            Action arg = command.Run;
            try
            {
                arg();
                e.Result = !command.ErrorOccurred;
            }
            catch (Exception ce)
            {
                Logger.Log(Logger.Levels.Info, "task threw exception: " + ce.Message);
                e.Result = false;
            }
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logger.Log(Logger.Levels.Info, "task failed. e.Error = " + e.Error.Message);
            }
        }
        private void lbCommands_Click(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                cr.UpdateDetailsText(((CommandInfo)(lbCommands.SelectedItem)).Results);
            }
        }

        private void lbCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                if (lbCommands.SelectedIndex != -1)
                {
                    cr.UpdateDetailsText(((CommandInfo)(lbCommands.SelectedItem)).Results);
                }
            }
        }

        private void lblViewLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("notepad", Logger.LogFilename);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "View log file failed");
                Logger.Log(Logger.Levels.Error, exc.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

    }
}
