﻿using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    /// <summary>
    /// This is the dialog 'wizard' traffic cop. It manages all trans-dialog navigation -- i.e., the 'common functions' such as Next, Back,
    /// and Exit.
    /// 
    /// Navigator also maintains an internal list of all dialog instances - spun up at startup to improve UI performance and maintain dialog state.
    /// </summary>
    internal static class Navigator
    {
        /// <summary>
        /// A friendly name for every dialog in the application.
        /// </summary>
        internal enum DialogName : int
        {
            MainForm = 0,
            ChangeDefaultIPAddress1,
            ChangeDefaultIPAddress2,
            ChangeDefaultIPAddress3,
            ChangeDefaultIPAddress4,
            ChangePassword,
            ChangePasswordResults,
            ConfigureCluster1,
            ConfigureCluster2,
            ConfigureClusterPort1,
            ConfigureClusterPort2,
            ConfigureClusterReview,
            ConfigureClusterReviewHosts,
            ConfigureCluster3,
            ResumeFromUpgrade,
            ClusterHealth,
            RestoreCluster1,
            RestoreCluster2,
            RestoreCluster3,
            RestoreCluster4,
            UpgradeFirmware1,
            UpgradeFirmware2,
            VerifyClusterChange,
            RunCommands,
            Scratch,
            PasswordPrompt
        }

        /// <summary>
        /// Actions requested by dialogs that fall outside of normal wizard navigation (Back, Next, etc.).
        /// </summary>
        internal enum TaskRequest
        {
            ChangePassword,
            EnterAppliancePasswords
        }

        private static Dictionary<DialogName, Form> dialogs = new Dictionary<DialogName, Form>();

        private static void Init()
        {
            dialogs.Add(DialogName.MainForm, new MainForm());
            dialogs.Add(DialogName.ChangePassword, new ChangePassword());
            dialogs.Add(DialogName.ChangePasswordResults, new ChangePasswordResults());
            dialogs.Add(DialogName.Scratch, new Scratch());
            dialogs.Add(DialogName.ChangeDefaultIPAddress1, new ChangeDefaultIPAddress1());
            dialogs.Add(DialogName.ChangeDefaultIPAddress2, new ChangeDefaultIPAddress2());
            dialogs.Add(DialogName.ChangeDefaultIPAddress3, new ChangeDefaultIPAddress3());
            dialogs.Add(DialogName.ChangeDefaultIPAddress4, new ChangeDefaultIPAddress4());
            dialogs.Add(DialogName.ConfigureCluster1, new ConfigureCluster1());
            dialogs.Add(DialogName.ConfigureCluster2, new ConfigureCluster2());
            dialogs.Add(DialogName.ConfigureClusterPort1, new ConfigureClusterPort1());
            dialogs.Add(DialogName.ConfigureClusterPort2, new ConfigureClusterPort2());
            dialogs.Add(DialogName.ConfigureClusterReview, new ConfigureClusterReview());
            dialogs.Add(DialogName.ConfigureClusterReviewHosts, new ConfigureClusterReviewHosts());
            dialogs.Add(DialogName.ConfigureCluster3, new ConfigureCluster3());
            dialogs.Add(DialogName.PasswordPrompt, new PasswordPrompt());
            dialogs.Add(DialogName.RestoreCluster1, new RestoreCluster1());
            dialogs.Add(DialogName.RestoreCluster2, new RestoreCluster2());
            dialogs.Add(DialogName.RestoreCluster3, new RestoreCluster3());
            dialogs.Add(DialogName.RestoreCluster4, new RestoreCluster4());
            dialogs.Add(DialogName.ClusterHealth, new ClusterHealth());
            dialogs.Add(DialogName.UpgradeFirmware1, new UpgradeFirmware1());
            dialogs.Add(DialogName.UpgradeFirmware2, new UpgradeFirmware2());
            dialogs.Add(DialogName.VerifyClusterChange, new VerifyClusterChange());
            dialogs.Add(DialogName.RunCommands, new RunCommands());
        }


        internal static Form Run()
        {
            Init();

            //Always start with the main form for now.
            return (dialogs[DialogName.MainForm]);
        }

        /// <summary>
        /// Determine the next form to navigate to in the dialog chain.
        /// </summary>
        /// <param name="task">who called?</param>
        /// <returns>the next form</returns>
        internal static void Next(DialogName current)
        {
            Form next;
            switch (current)
            {
                case DialogName.ChangePassword:
                    next = dialogs[DialogName.ChangePasswordResults];
                    break;
                case DialogName.ChangeDefaultIPAddress1:
                    next = dialogs[DialogName.ChangeDefaultIPAddress2];
                    break;
                case DialogName.ChangeDefaultIPAddress2:
                    next = dialogs[DialogName.ChangeDefaultIPAddress3];
                    break;
                case DialogName.ConfigureCluster1:
                    next = dialogs[DialogName.ConfigureCluster2];
                    break;
                case DialogName.ConfigureCluster2:
                    next = dialogs[DialogName.ConfigureClusterPort1];
                    break;
                case DialogName.ConfigureClusterReviewHosts:
                    next = dialogs[DialogName.ConfigureClusterReview];
                    break;
                case DialogName.ChangeDefaultIPAddress3:
                    next = dialogs[DialogName.ChangeDefaultIPAddress4];
                    break;
                case DialogName.RestoreCluster1:
                    next = dialogs[DialogName.RestoreCluster2];
                    break;
                case DialogName.RestoreCluster2:
                    next = dialogs[DialogName.RestoreCluster3];
                    break;
                case DialogName.RestoreCluster3:
                    next = dialogs[DialogName.RestoreCluster4];
                    break;
                case DialogName.UpgradeFirmware1:
                    next = dialogs[DialogName.UpgradeFirmware2];
                    break;
                case DialogName.UpgradeFirmware2:
                case DialogName.RestoreCluster4:
                    next = dialogs[DialogName.VerifyClusterChange];
                    break;
                default:
                    //Do nothing. Should probably log this case, though, to identify any dialogs with orphaned next buttons.
                    MessageBox.Show("This Next button has no place to go!");
                    next = dialogs[current];
                    break;
            }

            Navigate(dialogs[current], next);
        }

        internal static void Back(DialogName current)
        {
            Form previous;
            switch (current)
            {
                case DialogName.ChangePassword:
                case DialogName.ChangeDefaultIPAddress1:
                case DialogName.ConfigureCluster1:
                case DialogName.UpgradeFirmware1:
                case DialogName.ClusterHealth:
                case DialogName.RestoreCluster1:
                    previous = dialogs[DialogName.MainForm];
                    break;
                case DialogName.ChangePasswordResults:
                    previous = dialogs[DialogName.ChangePassword];
                    break;
                case DialogName.ChangeDefaultIPAddress2:
                    previous = dialogs[DialogName.ChangeDefaultIPAddress1];
                    break;
                case DialogName.ChangeDefaultIPAddress3:
                    previous = dialogs[DialogName.ChangeDefaultIPAddress2];
                    break;
                case DialogName.ChangeDefaultIPAddress4:
                    previous = dialogs[DialogName.ChangeDefaultIPAddress3];
                    break;
                case DialogName.ConfigureCluster2:
                    previous = dialogs[DialogName.ConfigureCluster1];
                    break;
                case DialogName.ConfigureClusterPort1:
                    previous = dialogs[DialogName.ConfigureCluster2];
                    break;
                case DialogName.ConfigureClusterPort2:
                    previous = dialogs[DialogName.ConfigureClusterPort1];
                    break;
                case DialogName.ConfigureClusterReviewHosts:
                    previous = dialogs[DialogName.ConfigureClusterReview];
                    break;
                case DialogName.RestoreCluster4:
                    previous = dialogs[DialogName.RestoreCluster3];
                    break;
                case DialogName.RestoreCluster3:
                    previous = dialogs[DialogName.RestoreCluster2];
                    break;
                case DialogName.RestoreCluster2:
                    previous = dialogs[DialogName.RestoreCluster1];
                    break;
                case DialogName.UpgradeFirmware2:
                    previous = dialogs[DialogName.UpgradeFirmware1];
                    break;
                default:
                    //Do nothing. Should probably log this case, though, to identify any dialogs with orphaned back buttons.
                    MessageBox.Show("This Back button has no place to go!");
                    previous = dialogs[current];
                    break;
            }

            Navigate(dialogs[current], previous);
        }


        private static void Navigate(Form from, Form to)
        {
            from.Hide();
            to.Show();
            to.BringToFront();
        }

        internal static bool Execute(Form current, TaskRequest task)
        {
            bool result = false;

            switch (task)
            {
                case TaskRequest.EnterAppliancePasswords:
                    if (dialogs[DialogName.PasswordPrompt].ShowDialog() == DialogResult.OK)
                    {
                        result = true;
                    }
                    break;
                default:
                    break;
            }
            return result;
        }

        internal static void Cancel(DialogName current)
        {
            Navigate(dialogs[current], dialogs[DialogName.MainForm]);
        }
        internal static void Close(DialogName current)
        {
            Navigate(dialogs[current], dialogs[DialogName.MainForm]);
        }
        internal static void GoTo(DialogName current, DialogName target)
        {
            bool Allowed = false;

            if ((!AppSession.ClusterConfig.WasSuccessfullyDeployed) &&
                (target == DialogName.UpgradeFirmware1 || target == DialogName.RestoreCluster1 || target == DialogName.ClusterHealth))
            {
                if (AppSession.ClusterConfig.IsSuccessfulAvailable)
                {
                    if (MessageBox.Show("In order to access this function, your last successful configuration must be loaded. Any undeployed" +
                        " configuration changes will be lost.\r\n\r\nSwitch to last successful configuration?",
                        "Operation not allowed.", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        AppSession.ClusterConfig.LoadLastSuccessful();
                        Allowed = true;
                    }
                }
                else
                {
                    MessageBox.Show("This operation is not available because your cluster configuration has not yet been deployed successfully." +
                        " Select 'Configure Cluster' to perfom a cluster configuration.",
                        "Operation not allowed.", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                Allowed = true;
            }
            if (Allowed)
            {
                Navigate(dialogs[current], dialogs[target]);
            }
        }

        internal static void ExitApp()
        {
            AppSession.ClusterConfig.SaveAsProvisional(); //just in case there are some uncommitted changes.
            Logger.Log(Logger.Levels.Info, "==================== SHADOWWCAT exiting. ====================\r\n");
            //Application.Exit();
        }

    }
}
