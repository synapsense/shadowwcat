﻿namespace shadowwcat.Dialogs
{
    partial class ConfigureCluster1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureCluster1));
            this.rbSingle = new System.Windows.Forms.RadioButton();
            this.rbBridged = new System.Windows.Forms.RadioButton();
            this.pbSingle = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblSingle = new System.Windows.Forms.Label();
            this.lblBridged = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lnkResetValues = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pbSingle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // rbSingle
            // 
            this.rbSingle.Location = new System.Drawing.Point(327, 350);
            this.rbSingle.Margin = new System.Windows.Forms.Padding(15);
            this.rbSingle.Name = "rbSingle";
            this.rbSingle.Size = new System.Drawing.Size(123, 21);
            this.rbSingle.TabIndex = 2;
            this.rbSingle.Text = "Single Network";
            this.rbSingle.UseVisualStyleBackColor = true;
            // 
            // rbBridged
            // 
            this.rbBridged.Location = new System.Drawing.Point(30, 350);
            this.rbBridged.Margin = new System.Windows.Forms.Padding(15);
            this.rbBridged.Name = "rbBridged";
            this.rbBridged.Size = new System.Drawing.Size(133, 21);
            this.rbBridged.TabIndex = 1;
            this.rbBridged.Text = "Bridged Network";
            this.rbBridged.UseVisualStyleBackColor = true;
            // 
            // pbSingle
            // 
            this.pbSingle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSingle.Image = ((System.Drawing.Image)(resources.GetObject("pbSingle.Image")));
            this.pbSingle.Location = new System.Drawing.Point(327, 30);
            this.pbSingle.Margin = new System.Windows.Forms.Padding(15);
            this.pbSingle.Name = "pbSingle";
            this.pbSingle.Size = new System.Drawing.Size(260, 290);
            this.pbSingle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSingle.TabIndex = 2;
            this.pbSingle.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(30, 30);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(15);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(260, 290);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // lblSingle
            // 
            this.lblSingle.Location = new System.Drawing.Point(347, 386);
            this.lblSingle.Margin = new System.Windows.Forms.Padding(30, 0, 0, 15);
            this.lblSingle.Name = "lblSingle";
            this.lblSingle.Size = new System.Drawing.Size(240, 89);
            this.lblSingle.TabIndex = 0;
            this.lblSingle.Text = "ES, Appliances, Remote Gateways, and the controlled devices are all on the same n" +
    "etwork. Only Port1 will be used on the Control Appliances.";
            // 
            // lblBridged
            // 
            this.lblBridged.Location = new System.Drawing.Point(50, 386);
            this.lblBridged.Margin = new System.Windows.Forms.Padding(30, 0, 0, 15);
            this.lblBridged.Name = "lblBridged";
            this.lblBridged.Size = new System.Drawing.Size(240, 129);
            this.lblBridged.TabIndex = 0;
            this.lblBridged.Text = resources.GetString("lblBridged.Text");
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 532);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(502, 532);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lnkResetValues
            // 
            this.lnkResetValues.Location = new System.Drawing.Point(262, 538);
            this.lnkResetValues.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.lnkResetValues.Name = "lnkResetValues";
            this.lnkResetValues.Size = new System.Drawing.Size(92, 17);
            this.lnkResetValues.TabIndex = 13;
            this.lnkResetValues.TabStop = true;
            this.lnkResetValues.Text = "Reset Values";
            this.lnkResetValues.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkResetValues_LinkClicked);
            // 
            // ConfigureCluster1
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(617, 575);
            this.ControlBox = false;
            this.Controls.Add(this.lnkResetValues);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.lblBridged);
            this.Controls.Add(this.lblSingle);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pbSingle);
            this.Controls.Add(this.rbBridged);
            this.Controls.Add(this.rbSingle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ConfigureCluster1";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure cluster - Select setup type";
            ((System.ComponentModel.ISupportInitialize)(this.pbSingle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rbSingle;
        private System.Windows.Forms.RadioButton rbBridged;
        private System.Windows.Forms.PictureBox pbSingle;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblSingle;
        private System.Windows.Forms.Label lblBridged;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.LinkLabel lnkResetValues;
    }
}