﻿namespace shadowwcat.Dialogs
{
    partial class UpgradeFirmware2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpgradeFirmware2));
            this.txtFirmwareImageFilename = new System.Windows.Forms.TextBox();
            this.btnBrowseFirmware = new System.Windows.Forms.Button();
            this.lblVersionInfo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialogFirmware = new System.Windows.Forms.OpenFileDialog();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.rbFirmwareOnlyUpgrade = new System.Windows.Forms.RadioButton();
            this.rbFullPlatformUpgrade = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBoxStop = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStop)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFirmwareImageFilename
            // 
            this.txtFirmwareImageFilename.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFirmwareImageFilename.Location = new System.Drawing.Point(18, 114);
            this.txtFirmwareImageFilename.Margin = new System.Windows.Forms.Padding(15, 0, 0, 15);
            this.txtFirmwareImageFilename.Name = "txtFirmwareImageFilename";
            this.txtFirmwareImageFilename.ReadOnly = true;
            this.txtFirmwareImageFilename.Size = new System.Drawing.Size(449, 22);
            this.txtFirmwareImageFilename.TabIndex = 1;
            // 
            // btnBrowseFirmware
            // 
            this.btnBrowseFirmware.Location = new System.Drawing.Point(482, 111);
            this.btnBrowseFirmware.Margin = new System.Windows.Forms.Padding(15, 0, 0, 15);
            this.btnBrowseFirmware.Name = "btnBrowseFirmware";
            this.btnBrowseFirmware.Size = new System.Drawing.Size(100, 28);
            this.btnBrowseFirmware.TabIndex = 2;
            this.btnBrowseFirmware.Text = "&Browse...";
            this.btnBrowseFirmware.UseVisualStyleBackColor = true;
            this.btnBrowseFirmware.Click += new System.EventHandler(this.btnBrowseFirmware_Click);
            // 
            // lblVersionInfo
            // 
            this.lblVersionInfo.Location = new System.Drawing.Point(15, 15);
            this.lblVersionInfo.Margin = new System.Windows.Forms.Padding(0, 0, 15, 15);
            this.lblVersionInfo.Name = "lblVersionInfo";
            this.lblVersionInfo.Size = new System.Drawing.Size(441, 62);
            this.lblVersionInfo.TabIndex = 0;
            this.lblVersionInfo.Text = "Select a firmware image file and Upgrade Type, then press Next.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 92);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 15, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Firmware image:";
            // 
            // openFileDialogFirmware
            // 
            this.openFileDialogFirmware.FileName = "appliance_update.img";
            this.openFileDialogFirmware.Filter = "Firmware images|*_update*.img|All files|*.*";
            this.openFileDialogFirmware.ReadOnlyChecked = true;
            this.openFileDialogFirmware.RestoreDirectory = true;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(367, 337);
            this.btnBack.Margin = new System.Windows.Forms.Padding(0, 0, 15, 15);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 51;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(18, 337);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 52;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(482, 337);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 50;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // rbFirmwareOnlyUpgrade
            // 
            this.rbFirmwareOnlyUpgrade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbFirmwareOnlyUpgrade.AutoSize = true;
            this.rbFirmwareOnlyUpgrade.Location = new System.Drawing.Point(15, 30);
            this.rbFirmwareOnlyUpgrade.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.rbFirmwareOnlyUpgrade.Name = "rbFirmwareOnlyUpgrade";
            this.rbFirmwareOnlyUpgrade.Size = new System.Drawing.Size(337, 21);
            this.rbFirmwareOnlyUpgrade.TabIndex = 11;
            this.rbFirmwareOnlyUpgrade.TabStop = true;
            this.rbFirmwareOnlyUpgrade.Text = "Upgrade only the firmware running on the cluster";
            this.rbFirmwareOnlyUpgrade.UseVisualStyleBackColor = true;
            this.rbFirmwareOnlyUpgrade.CheckedChanged += new System.EventHandler(this.rbFirmwareOnlyUpgrade_CheckedChanged);
            // 
            // rbFullPlatformUpgrade
            // 
            this.rbFullPlatformUpgrade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbFullPlatformUpgrade.AutoSize = true;
            this.rbFullPlatformUpgrade.Location = new System.Drawing.Point(15, 79);
            this.rbFullPlatformUpgrade.Margin = new System.Windows.Forms.Padding(0);
            this.rbFullPlatformUpgrade.Name = "rbFullPlatformUpgrade";
            this.rbFullPlatformUpgrade.Size = new System.Drawing.Size(497, 21);
            this.rbFullPlatformUpgrade.TabIndex = 12;
            this.rbFullPlatformUpgrade.TabStop = true;
            this.rbFullPlatformUpgrade.Text = "Upgrade the Environmental server and the firmware running on the cluster";
            this.rbFullPlatformUpgrade.UseVisualStyleBackColor = true;
            this.rbFullPlatformUpgrade.CheckedChanged += new System.EventHandler(this.rbFullPlatformUpgrade_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbFirmwareOnlyUpgrade);
            this.groupBox1.Controls.Add(this.rbFullPlatformUpgrade);
            this.groupBox1.Location = new System.Drawing.Point(18, 151);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(15);
            this.groupBox1.Size = new System.Drawing.Size(564, 144);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Upgrade Type";
            // 
            // pictureBoxStop
            // 
            this.pictureBoxStop.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxStop.Image")));
            this.pictureBoxStop.Location = new System.Drawing.Point(520, 15);
            this.pictureBoxStop.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.pictureBoxStop.Name = "pictureBoxStop";
            this.pictureBoxStop.Size = new System.Drawing.Size(62, 62);
            this.pictureBoxStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxStop.TabIndex = 33;
            this.pictureBoxStop.TabStop = false;
            // 
            // UpgradeFirmware2
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(597, 380);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBoxStop);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblVersionInfo);
            this.Controls.Add(this.btnBrowseFirmware);
            this.Controls.Add(this.txtFirmwareImageFilename);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UpgradeFirmware2";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upgrade firmware - Select firmware image";
            this.Activated += new System.EventHandler(this.UpgradeFirmware2_Activated);
            this.Load += new System.EventHandler(this.UpgradeFirmware2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxStop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFirmwareImageFilename;
        private System.Windows.Forms.Button btnBrowseFirmware;
        private System.Windows.Forms.Label lblVersionInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialogFirmware;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.RadioButton rbFirmwareOnlyUpgrade;
        private System.Windows.Forms.RadioButton rbFullPlatformUpgrade;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBoxStop;
    }
}