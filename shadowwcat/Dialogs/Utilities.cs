﻿using IPAddressControlLib;
using shadowwcat.Data;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    internal static class Utilities
    {
        internal static Regex ValidIpAddressRegex = new Regex(@"^(([1-9]\.)|([1-9]\d\.)|(1\d{2}\.)|(2[0-4]\d\.)|(25[0-5]\.))(([01]?\d{1,2}\.)|(2[0-4]\d\.)|(25[0-5]\.)){2}(([01]?\d\d?)|(2[0-4][0-9])|(25[0-5]))$");
        internal static Regex ValidHostnameRegex = new Regex(@"^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])");

        internal static bool VerifyIPorHostName(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                if (ValidIpAddressRegex.IsMatch(input))
                {
                    return true;
                }
                else if (ValidHostnameRegex.IsMatch(input))
                {
                    return true;
                }
            }

            return false;
        }

        internal static bool VerifyIPAddress(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                if (ValidIpAddressRegex.IsMatch(input))
                {
                    return true;
                }
            }

            return false;
        }

        internal static bool VerifyHostName(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                if (ValidHostnameRegex.IsMatch(input))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The IPAddressControl customer control returns the string "..." when no value is entered by user. Need to really return an empty string.
        /// </summary>
        /// <param name="Control"></param>
        /// <returns></returns>
        internal static string IPToString(IPAddressControl Control)
        {
            return (Control.Blank ? "" : Control.Text);
        }

        internal static void ResetValues(Navigator.DialogName dialog)
        {
            DialogResult res = DialogResult.Cancel;

            if (AppSession.ClusterConfig.IsSuccessfulAvailable)
            {
                res = MessageBox.Show("Select 'Yes' to load values from the last successful configuration.\r\n\r\n" +
                    "Select 'No' to reset values to system defaults.\r\n\r\nSelect 'Cancel' to make no changes.",
                    "Reset config values", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            }
            else
            {
                res = MessageBox.Show("Select 'OK' to reset values to system defaults.\r\n\r\nSelect 'Cancel' to make no changes.",
                    "Reset config values", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }

            if (res != DialogResult.Cancel)
            {
                if (res == DialogResult.Yes)
                {
                    AppSession.ClusterConfig.LoadLastSuccessful();
                    AppSession.ClusterConfig.Save();
                }
                else if (res == DialogResult.No || res == DialogResult.OK)
                {
                    AppSession.ClusterConfig.LoadNewProvisional();
                    AppSession.ClusterConfig.Save();
                }

                AppSession.IsConfigureClusterBridged = AppSession.ClusterConfig.Net2_Enabled;

                Navigator.GoTo(dialog, Navigator.DialogName.ConfigureCluster1);
            }
        }
    }
}
