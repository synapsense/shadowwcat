﻿namespace shadowwcat.Dialogs
{
    partial class PasswordPrompt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasswordPrompt));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtAppliance1Password = new System.Windows.Forms.TextBox();
            this.lblAppliance1Password = new System.Windows.Forms.Label();
            this.lblAppliance2Password = new System.Windows.Forms.Label();
            this.txtAppliance2Password = new System.Windows.Forms.TextBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.chkSamePwd = new System.Windows.Forms.CheckBox();
            this.chkRememberPwd = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(268, 242);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(15, 15, 0, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(153, 242);
            this.btnOK.Margin = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 20;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtAppliance1Password
            // 
            this.txtAppliance1Password.Location = new System.Drawing.Point(43, 69);
            this.txtAppliance1Password.Margin = new System.Windows.Forms.Padding(30, 0, 0, 20);
            this.txtAppliance1Password.Name = "txtAppliance1Password";
            this.txtAppliance1Password.PasswordChar = '*';
            this.txtAppliance1Password.Size = new System.Drawing.Size(200, 22);
            this.txtAppliance1Password.TabIndex = 1;
            this.txtAppliance1Password.UseSystemPasswordChar = true;
            this.txtAppliance1Password.WordWrap = false;
            this.txtAppliance1Password.Enter += new System.EventHandler(this.txtAppliance1Password_Enter);
            // 
            // lblAppliance1Password
            // 
            this.lblAppliance1Password.AutoSize = true;
            this.lblAppliance1Password.Location = new System.Drawing.Point(40, 47);
            this.lblAppliance1Password.Margin = new System.Windows.Forms.Padding(25, 0, 0, 5);
            this.lblAppliance1Password.Name = "lblAppliance1Password";
            this.lblAppliance1Password.Size = new System.Drawing.Size(82, 17);
            this.lblAppliance1Password.TabIndex = 0;
            this.lblAppliance1Password.Text = "Appliance 1";
            this.lblAppliance1Password.Click += new System.EventHandler(this.lblAppliance1Password_Click);
            // 
            // lblAppliance2Password
            // 
            this.lblAppliance2Password.AutoSize = true;
            this.lblAppliance2Password.Location = new System.Drawing.Point(40, 111);
            this.lblAppliance2Password.Margin = new System.Windows.Forms.Padding(25, 0, 0, 5);
            this.lblAppliance2Password.Name = "lblAppliance2Password";
            this.lblAppliance2Password.Size = new System.Drawing.Size(82, 17);
            this.lblAppliance2Password.TabIndex = 0;
            this.lblAppliance2Password.Text = "Appliance 2";
            this.lblAppliance2Password.Click += new System.EventHandler(this.lblAppliance2Password_Click);
            // 
            // txtAppliance2Password
            // 
            this.txtAppliance2Password.Location = new System.Drawing.Point(43, 159);
            this.txtAppliance2Password.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.txtAppliance2Password.Name = "txtAppliance2Password";
            this.txtAppliance2Password.PasswordChar = '*';
            this.txtAppliance2Password.Size = new System.Drawing.Size(200, 22);
            this.txtAppliance2Password.TabIndex = 3;
            this.txtAppliance2Password.UseSystemPasswordChar = true;
            this.txtAppliance2Password.WordWrap = false;
            this.txtAppliance2Password.Enter += new System.EventHandler(this.txtAppliance2Password_Enter);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // chkSamePwd
            // 
            this.chkSamePwd.AutoSize = true;
            this.chkSamePwd.Location = new System.Drawing.Point(43, 133);
            this.chkSamePwd.Margin = new System.Windows.Forms.Padding(0, 15, 0, 5);
            this.chkSamePwd.Name = "chkSamePwd";
            this.chkSamePwd.Size = new System.Drawing.Size(128, 21);
            this.chkSamePwd.TabIndex = 2;
            this.chkSamePwd.Text = "Same as above";
            this.chkSamePwd.UseVisualStyleBackColor = true;
            this.chkSamePwd.CheckedChanged += new System.EventHandler(this.chkSamePwd_CheckedChanged);
            // 
            // chkRememberPwd
            // 
            this.chkRememberPwd.AutoSize = true;
            this.chkRememberPwd.Location = new System.Drawing.Point(18, 206);
            this.chkRememberPwd.Margin = new System.Windows.Forms.Padding(0, 10, 0, 15);
            this.chkRememberPwd.Name = "chkRememberPwd";
            this.chkRememberPwd.Size = new System.Drawing.Size(259, 21);
            this.chkRememberPwd.TabIndex = 4;
            this.chkRememberPwd.Text = "Remember password during session";
            this.chkRememberPwd.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "Enter passwords for";
            // 
            // PasswordPrompt
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(381, 285);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkRememberPwd);
            this.Controls.Add(this.chkSamePwd);
            this.Controls.Add(this.lblAppliance2Password);
            this.Controls.Add(this.txtAppliance2Password);
            this.Controls.Add(this.lblAppliance1Password);
            this.Controls.Add(this.txtAppliance1Password);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PasswordPrompt";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Appliance passwords";
            this.Activated += new System.EventHandler(this.PasswordPrompt_Activated);
            this.Load += new System.EventHandler(this.PasswordPrompt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtAppliance1Password;
        private System.Windows.Forms.Label lblAppliance1Password;
        private System.Windows.Forms.Label lblAppliance2Password;
        private System.Windows.Forms.TextBox txtAppliance2Password;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.CheckBox chkRememberPwd;
        private System.Windows.Forms.CheckBox chkSamePwd;
        private System.Windows.Forms.Label label1;
    }
}