﻿using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace shadowwcat.Dialogs
{
    class CommandRunner
    {
        internal enum RunStatus
        {
            Ready,
            Running,
            Successful,
            Failed
        }
        //UI control references used by this class. Must be set by caller.
        internal Label Status { get; set; }
        internal ProgressBar Progress { get; set; }
        internal Label ProgressFail { get; set; }
        internal ListBox CommandList { get; set; }
        internal TextBox Details { get; set; }
        internal Label ElapsedTime { get; set; }
        internal Button StartExecution { get; set; }
        internal Button Done { get; set; }
        internal Button Back { get; set; }
        internal BackgroundWorker bw { get; set; }

        //Dialog specific text used by this class. Must be set by caller.
        internal string TaskName { get; set; }
        internal string TaskDetails { get; set; }
        internal string StartButtonName { get; set; }
        internal string CustomSuccessInstructions { get; set; }
        internal string CustomErrorInstructions { get; set; }


        internal bool PerformTask(Command cmdAppliance1, Command cmdAppliance2)
        {
            string Result = string.Empty;
            bool ErrorOccurred = false;
            int i = 0;
            bw.WorkerSupportsCancellation = true;

            StartExecution.Enabled = false;
            Done.Enabled = false;
            Back.Enabled = false;
            SetDisplayNormal();
            ClearStatusArea();
            SetStatus(RunStatus.Running);

            Logger.Log(Logger.Levels.Info, string.Format("Starting '{0} - {1}'.", TaskName, TaskDetails));
            Stopwatch stopWatch = new Stopwatch();

            int numCommands = CommandList.Items.Count;
            int ProgressIncrement = (100 / (numCommands * 2));
            stopWatch.Start();
            Progress.Value = 10;

            for (i = 0; i < numCommands; i++)
            {
                Result = string.Empty;
                CommandList.SetSelected(i, true);
                cmdAppliance1.MethodToRun = ((CommandInfo)CommandList.Items[i]).Action1;
                if (cmdAppliance1.MethodToRun != null)
                {
                    UpdateDetailsText("Executing command on " + cmdAppliance1.ApplianceHostname);                    
                    bw.RunWorkerAsync(cmdAppliance1);                    
                    waitfortask();
                    Result += string.Format("Appliance: {0} -- {1}\r\n\r\n", cmdAppliance1.ApplianceHostname, cmdAppliance1.CommandResults);
                    if (cmdAppliance1.ErrorOccurred)
                    {
                        ErrorOccurred = true;
                        break;
                    }
                }
                Progress.Increment(ProgressIncrement);
                cmdAppliance2.MethodToRun = ((CommandInfo)CommandList.Items[i]).Action2;
                bw.CancelAsync();// cancel any operation before starting new one.
                if (cmdAppliance2.MethodToRun != null)
                {
                    UpdateDetailsText("Executing command on " + cmdAppliance2.ApplianceHostname);
                    bw.RunWorkerAsync(cmdAppliance2);
                    waitfortask();
                    Result += string.Format("Appliance: {0} -- {1}\r\n", cmdAppliance2.ApplianceHostname, cmdAppliance2.CommandResults);
                    if (cmdAppliance2.ErrorOccurred)
                    {
                        ErrorOccurred = true;
                        break;
                    }
                }
                ((CommandInfo)CommandList.Items[i]).Results = Result;
                Progress.Increment(ProgressIncrement);
                UpdateCommandWithStatus(CommandList, i, "Successful");
            }

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
            ElapsedTime.Text = string.Format("Elapsed time: {0}", elapsedTime);

            if (ErrorOccurred)
            {
                UpdateCommandWithStatus(CommandList, i, "Failed");

                ((CommandInfo)CommandList.Items[i]).Results = string.Format("{0}.\r\n\r\n{1}", Result, CustomErrorInstructions);
                UpdateDetailsText(((CommandInfo)CommandList.Items[i]).Results);
                SetStatus(RunStatus.Failed);
                SetDisplayError();
                Logger.Log(Logger.Levels.Error, string.Format("{0} failed: {1}. Elapsed time: {2}", TaskName, Result, elapsedTime));
            }
            else
            {
                CommandList.ClearSelected();
                UpdateDetailsText("Operation successful.");
                Progress.Value = 100;
                SetStatus(RunStatus.Successful);
                Logger.Log(Logger.Levels.Info, string.Format("{0} completed successfully. Elapsed time: {1}", TaskName, elapsedTime));
            }
            StartExecution.Enabled = true;
            Done.Enabled = true;
            Back.Enabled = true;
            //lblViewLog.Enabled = true;
            Done.Focus();

            return !ErrorOccurred;
        }
        private void UpdateCommandWithStatus(ListBox lb, int ItemNum, string Status)
        {
            string oldCmdName = ((CommandInfo)lb.Items[ItemNum]).CommandName;
            CommandInfo ci = new CommandInfo(oldCmdName + PadIt(oldCmdName.Length) + Status, null, null);
            ci.Results = ((CommandInfo)lb.Items[ItemNum]).Results;
            lb.Items[ItemNum] = ci;
        }
        private string PadIt(int p)
        {
            int numTabs = (64 - p) / 8;
            if (p + (numTabs * 8) > 64) numTabs--;
            return new string('\t', numTabs);
        }

        private void waitfortask()
        {
            while (bw.IsBusy)
            {
                // Keep UI messages moving, so the form remains  
                // responsive during the asynchronous operation.
                Application.DoEvents();
            }
        }

        internal void SetStatus(RunStatus status)
        {
            switch (status)
            {
                case RunStatus.Ready:
                    Status.ForeColor = System.Drawing.SystemColors.ControlText;
                    Status.Text = "Ready";
                    break;
                case RunStatus.Running:
                    Status.ForeColor = Color.Green;
                    Status.Text = "Running...";
                    break;
                case RunStatus.Successful:
                    Status.ForeColor = Color.Green;
                    Status.Text = "Successful";
                    break;
                case RunStatus.Failed:
                    Status.ForeColor = Color.Red;
                    Status.Text = "Failed";
                    break;
                default:
                    Status.ForeColor = System.Drawing.SystemColors.ControlText;
                    Status.Text = "Ready";
                    break;
            }
        }
        internal void ClearRunData()
        {
            Progress.Value = 0;
            ClearStatusArea();
            SetDisplayNormal();
            CommandList.Items.Clear();
            UpdateDetailsText(string.Format("\r\nPress {0} to begin.", StartButtonName));
            StartExecution.Focus();
            //lblViewLog.Enabled = false;
        }
        internal void UpdateDetailsText(string msg)
        {
            Details.Text = msg;
        }
        private void ClearStatusArea()
        {
            SetStatus(RunStatus.Ready);
            ElapsedTime.Text = string.Empty;
            UpdateDetailsText(string.Empty);
        }

        private void SetDisplayError()
        {
            Progress.Value = 0;
            Progress.SendToBack();
            ProgressFail.Visible = true;
            ProgressFail.Show();
        }
        private void SetDisplayNormal()
        {
            ProgressFail.SendToBack();
            Progress.Visible = true;
            Progress.Show();
        }
    }
}
