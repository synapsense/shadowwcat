﻿namespace shadowwcat.Dialogs
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.rtfText = new System.Windows.Forms.RichTextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.lnk3rdparty = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // rtfText
            // 
            this.rtfText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtfText.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rtfText.Location = new System.Drawing.Point(15, 15);
            this.rtfText.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.rtfText.Name = "rtfText";
            this.rtfText.ReadOnly = true;
            this.rtfText.Size = new System.Drawing.Size(503, 307);
            this.rtfText.TabIndex = 0;
            this.rtfText.TabStop = false;
            this.rtfText.Text = "";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(216, 337);
            this.btnOK.Margin = new System.Windows.Forms.Padding(0);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 28);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lnk3rdparty
            // 
            this.lnk3rdparty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lnk3rdparty.AutoSize = true;
            this.lnk3rdparty.Location = new System.Drawing.Point(15, 343);
            this.lnk3rdparty.Margin = new System.Windows.Forms.Padding(0);
            this.lnk3rdparty.Name = "lnk3rdparty";
            this.lnk3rdparty.Size = new System.Drawing.Size(92, 17);
            this.lnk3rdparty.TabIndex = 2;
            this.lnk3rdparty.TabStop = true;
            this.lnk3rdparty.Text = "3rd party info";
            this.lnk3rdparty.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnk3rdparty_LinkClicked);
            // 
            // About
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(532, 380);
            this.ControlBox = false;
            this.Controls.Add(this.lnk3rdparty);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.rtfText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "About";
            this.Padding = new System.Windows.Forms.Padding(15, 15, 15, 15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About SHADOWW-CAT";
            this.Load += new System.EventHandler(this.About_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtfText;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.LinkLabel lnk3rdparty;
    }
}