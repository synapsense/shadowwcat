﻿using IPAddressControlLib;
using Microsoft.Win32;
using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ConfigureCluster2 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ConfigureCluster2;
        private bool populatingLists = false;

        public ConfigureCluster2()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = cmboESIPAddress;
                this.Text = "Configure Cluster Setup - " + (AppSession.IsConfigureClusterBridged ? "Bridged" : "Single");

                SetFormValues();
            }
        }

        private void SetFormValues()
        {
            txtESHostname.Text = AppSession.ClusterConfig.ES_Hostname;

            cmboESIPAddress.Items.Clear();
            ESHostInfo.NetworkInfoDictionary dict = AppSession.ClusterConfig.NetworkInfo;
            bool found = false;
            foreach (string ip in dict.Keys)
            {
                cmboESIPAddress.Items.Add(ip);
                if (ip.Equals(AppSession.ClusterConfig.ES_IPAddress))
                {
                    found = true;
                }
            }

            if (!found)
            {
                cmboESIPAddress.Items.Add(AppSession.ClusterConfig.ES_IPAddress);
            }

            cmboESIPAddress.Text = AppSession.ClusterConfig.ES_IPAddress;

            populatingLists = true;
            populateNameServerList();
            populateTimeServerList();
            populatingLists = false;
        }

        private void populateNameServerList()
        {
            List<ListViewItem> l = new List<ListViewItem>();
            ListViewItem lvi;
            ESHostInfo.NetworkInfoDictionary dict = AppSession.ClusterConfig.NetworkInfo;
            ESHostInfo.NETWORK_INFO ni;
            bool foundIP = dict.TryGetValue(AppSession.ClusterConfig.ES_IPAddress, out ni);

            if (foundIP)
            {
                foreach (string dns in ni.dnsServerIPs)
                {
                    lvi = new ListViewItem(dns);
                    lvi.Checked = AppSession.ClusterConfig.Nameserver.Contains(dns);
                    l.Add(lvi);
                }
            }

            foreach (string dns in AppSession.ClusterConfig.Nameserver)
            {
                if (!foundIP || !ni.dnsServerIPs.Contains(dns))
                {
                    lvi = new ListViewItem(dns);
                    lvi.Checked = true;
                    l.Add(lvi);
                }
            }

            lstDNS2.Items.Clear();
            lstDNS2.Items.AddRange(l.ToArray());
        }

        private void populateTimeServerList()
        {
            List<ListViewItem> l = new List<ListViewItem>();
            ESHostInfo.HOST_IP[] ntpServers = AppSession.ClusterConfig.HostInfo.NTPServers;
            ListViewItem lvi;
            foreach (ESHostInfo.HOST_IP item in ntpServers)
            {
                lvi = new ListViewItem(item.name);
                lvi.Checked = AppSession.ClusterConfig.Timeserver.Contains(item.name) ||
                              AppSession.ClusterConfig.Timeserver.Contains(item.ip);
                lvi.SubItems.Add(item.ip);
                l.Add(lvi);
            }

            lvi = new ListViewItem(AppSession.ClusterConfig.ES_Hostname);
            lvi.Checked = AppSession.ClusterConfig.Timeserver.Contains(AppSession.ClusterConfig.ES_Hostname) ||
                          AppSession.ClusterConfig.Timeserver.Contains(AppSession.ClusterConfig.ES_IPAddress);
            lvi.SubItems.Add(AppSession.ClusterConfig.ES_IPAddress);
            l.Add(lvi);

            lstNTP.Items.Clear();
            lstNTP.Items.AddRange(l.ToArray());
        }

        private string[] GetCheckedNameServers()
        {
            List<string> items = new List<string>();
            foreach (ListViewItem lvi in lstDNS2.CheckedItems)
            {
                items.Add(lvi.Text);
            }

            return items.ToArray();
        }

        private string[] GetCheckedTimeServers()
        {
            int index = (lstDNS2.CheckedItems.Count == 0) ? 1 : 0;

            List<string> items = new List<string>();
            foreach (ListViewItem lvi in lstNTP.CheckedItems)
            {
                items.Add(lvi.SubItems[index].Text);
            }

            return items.ToArray();
        }

        private bool ValidFormData()
        {
            errorProvider1.Clear();

            // Including this for completeness.
            //AppSession.ClusterConfig.ES_Hostname - never overridden by user.

            ESHostInfo.NetworkInfoDictionary dict = AppSession.ClusterConfig.NetworkInfo;
            ESHostInfo.NETWORK_INFO ni;
            dict.TryGetValue(cmboESIPAddress.Text, out ni);
            // If user changed ES ip address, go ahead and change default gateway and subnet mask
            if (!cmboESIPAddress.Text.Equals(AppSession.ClusterConfig.ES_IPAddress))
            {
                try
                {
                    AppSession.ClusterConfig.ES_IPAddress = cmboESIPAddress.Text;
                    AppSession.ClusterConfig.Net1_Gateway = ni.defaultGateway;
                    AppSession.ClusterConfig.Net1_Netmask = ni.netmask;
                    AppSession.ClusterConfig.Net2_Netmask = string.IsNullOrEmpty(AppSession.ClusterConfig.Net2_Netmask) ? ni.netmask : AppSession.ClusterConfig.Net2_Netmask;
                }
                catch (InvalidOperationException)
                {
                    errorProvider1.SetError(cmboESIPAddress, "Environment Server IP address is not a valid IP address.");
                    return false;
                }
            }

            AppSession.UseDNS = (lstDNS2.CheckedItems.Count > 0);

            try
            {
                AppSession.ClusterConfig.Nameserver = GetCheckedNameServers();
            }
            catch (InvalidOperationException)
            {
                errorProvider1.SetError(lstDNS2, "Name server is empty, or not a valid IP address.");
                return false;
            }

            try
            {
                AppSession.ClusterConfig.Timeserver = GetCheckedTimeServers();
            }
            catch (InvalidOperationException)
            {
                errorProvider1.SetError(lstNTP, "Time server is empty, or not an IP address or a fully qualified domain name.");
                return false;
            }

            foreach (string ntp in AppSession.ClusterConfig.Timeserver)
            {
                if (ntp.Equals(AppSession.ClusterConfig.ES_IPAddress) || ntp.Equals(AppSession.ClusterConfig.ES_Hostname))
                {
                    if (!StartTimeService())
                    {
                        errorProvider1.SetError(btnNext, "Couldn't start Windows Time Service");
                        return false;
                    }
                }
            }

            return true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (ValidFormData())
            {
                Navigator.Next(me);
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.Back(me);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        /// <summary>
        /// The IPAddressControl customer control returns the string "..." when no value is entered by user. Need to really return an empty string.
        /// </summary>
        /// <param name="Control"></param>
        /// <returns></returns>
        private string IPToString(IPAddressControl Control)
        {
            return (Control.Blank ? "" : Control.Text);
        }

        private void lnkResetValues_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Utilities.ResetValues(this.me);
        }

        private bool StartTimeService()
        {
            string WMIPath = @"\\" + Dns.GetHostName() + @"\root\cimv2";
            ConnectionOptions WMIOptions = new ConnectionOptions();
            ManagementScope WMIScope = new ManagementScope(WMIPath, WMIOptions);
            ObjectQuery WMIQuery = new ObjectQuery();
            ManagementObjectSearcher WMISearcher = new ManagementObjectSearcher();
            uint ret = 0;

            try
            {
                WMIScope.Connect();

                WMIQuery.QueryString = "SELECT * FROM Win32_Service WHERE Name = 'W32Time'";
                WMISearcher.Scope = WMIScope;
                WMISearcher.Query = WMIQuery;
                ManagementObjectCollection w32TimeServices = WMISearcher.Get();
                Boolean isStarted = false;
                foreach (ManagementObject w32Time in w32TimeServices)
                {
                    isStarted = w32Time["Started"].ToString().Equals("True");
                    Logger.Log(Logger.Levels.Info, "W32Time service is " + (isStarted ? "started." : "not started."));
                    if (isStarted)
                    {
                        ret = (uint)w32Time.InvokeMethod("StopService", null);
                        if (ret != 0)
                        {
                            Logger.Log(Logger.Levels.Info, "Unable to stop W32Time service. Error code: " + ret.ToString());
                            MessageBox.Show("Unable to stop W32Time service. Error code: " + ret.ToString());
                            return false;
                        }
                    }

                    RegistryKey myKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\services\\W32Time", true);
                    if (myKey == null)
                    {
                        MessageBox.Show("Unable to configure Time Service.");
                        return false;
                    }

                    myKey.SetValue("Start", "2", RegistryValueKind.DWord);
                    myKey.SetValue("DelayedAutostart", "0", RegistryValueKind.DWord);
                    myKey.Close();

                    myKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\services\\W32Time\\Config", true);
                    if (myKey == null)
                    {
                        MessageBox.Show("Unable to configure Time Service.");
                        return false;
                    }

                    myKey.SetValue("AnnounceFlags", "5", RegistryValueKind.DWord);
                    myKey.Close();

                    myKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\services\\W32Time\\TimeProviders\\NtpServer", true);
                    if (myKey == null)
                    {
                        MessageBox.Show("Unable to configure Time Service.");
                        return false;
                    }

                    myKey.SetValue("Enabled", "1", RegistryValueKind.DWord);
                    myKey.Close();

                    ret = (uint)w32Time.InvokeMethod("StartService", null);
                    Logger.Log(Logger.Levels.Info, "Started W32Time service. ret = " + ret.ToString());
                    if (ret != 0 && ret != 10)
                    {
                        Logger.Log(Logger.Levels.Info, "Unable to start W32Time service. Error code: " + ret.ToString());
                        MessageBox.Show("Unable to start W32Time service. Error code: " + ret.ToString());
                        return false;
                    }
                }

                Logger.Log(Logger.Levels.Info, "Verifying Time Service was started.");
                isStarted = false;
                int retries = 0;
                while (!isStarted && retries < 3)
                {
                    w32TimeServices = WMISearcher.Get();
                    foreach (ManagementObject w32Time in w32TimeServices)
                    {
                        isStarted = w32Time["Started"].ToString().Equals("True");
                        Logger.Log(Logger.Levels.Info, "W32Time service is " + (isStarted ? "started." : "not started."));
                    }
                    retries++;
                }

                if (!isStarted)
                {
                    Logger.Log(Logger.Levels.Info, "Unable to start W32Time service.");
                    MessageBox.Show("Was unable to start the Time service. Please use the Windows Task Manager or Control Panel to Start the Windows Time service, then configure the cluster.");
                    return false;
                }
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Could not configure Time Service. Reason: {0}", e.Message));
            }

            return true;
        }

        private void lstNTP_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!populatingLists && lstNTP.CheckedIndices.Count == 0)
            {
                MessageBox.Show("There must be at least one time server checked.");
                e.Item.Checked = true;
            }
        }

        private void lstDNS2_ItemChecked(object sender, ItemCheckedEventArgs e)
        {

        }
    }
}
