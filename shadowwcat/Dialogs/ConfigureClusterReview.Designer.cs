﻿namespace shadowwcat.Dialogs
{
    partial class ConfigureClusterReview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureClusterReview));
            this.chkAcknowledgeRisks = new System.Windows.Forms.CheckBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtSummary = new System.Windows.Forms.RichTextBox();
            this.lblHeader = new System.Windows.Forms.Label();
            this.lnkResetValues = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // chkAcknowledgeRisks
            // 
            this.chkAcknowledgeRisks.AutoSize = true;
            this.chkAcknowledgeRisks.Location = new System.Drawing.Point(382, 493);
            this.chkAcknowledgeRisks.Margin = new System.Windows.Forms.Padding(0, 0, 0, 18);
            this.chkAcknowledgeRisks.Name = "chkAcknowledgeRisks";
            this.chkAcknowledgeRisks.Size = new System.Drawing.Size(166, 21);
            this.chkAcknowledgeRisks.TabIndex = 3;
            this.chkAcknowledgeRisks.Text = "I understand the risks";
            this.chkAcknowledgeRisks.UseVisualStyleBackColor = true;
            this.chkAcknowledgeRisks.CheckedChanged += new System.EventHandler(this.chkAcknowledgeRisks_CheckedChanged);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(502, 532);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(382, 532);
            this.btnBack.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 11;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 532);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtSummary
            // 
            this.txtSummary.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtSummary.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSummary.Location = new System.Drawing.Point(18, 107);
            this.txtSummary.Margin = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.txtSummary.Name = "txtSummary";
            this.txtSummary.ReadOnly = true;
            this.txtSummary.Size = new System.Drawing.Size(587, 366);
            this.txtSummary.TabIndex = 1;
            this.txtSummary.Text = "";
            this.txtSummary.WordWrap = false;
            // 
            // lblHeader
            // 
            this.lblHeader.Location = new System.Drawing.Point(15, 15);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(587, 77);
            this.lblHeader.TabIndex = 24;
            this.lblHeader.Text = "Ready to configure the cluster.\r\n\r\nThe cluster configuration process typically ta" +
    "kes 7 to 8 minutes.";
            // 
            // lnkResetValues
            // 
            this.lnkResetValues.Location = new System.Drawing.Point(262, 538);
            this.lnkResetValues.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.lnkResetValues.Name = "lnkResetValues";
            this.lnkResetValues.Size = new System.Drawing.Size(92, 17);
            this.lnkResetValues.TabIndex = 25;
            this.lnkResetValues.TabStop = true;
            this.lnkResetValues.Text = "Reset Values";
            this.lnkResetValues.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkResetValues_LinkClicked);
            // 
            // ConfigureClusterReview
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(617, 575);
            this.ControlBox = false;
            this.Controls.Add(this.lnkResetValues);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.txtSummary);
            this.Controls.Add(this.chkAcknowledgeRisks);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ConfigureClusterReview";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure cluster - Review configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox chkAcknowledgeRisks;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RichTextBox txtSummary;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.LinkLabel lnkResetValues;
    }
}