﻿namespace shadowwcat.Dialogs
{
    partial class ConfigureCluster3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureCluster3));
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnDone = new System.Windows.Forms.Button();
            this.lbCommands = new System.Windows.Forms.ListBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.txtDetails = new System.Windows.Forms.TextBox();
            this.bw = new System.ComponentModel.BackgroundWorker();
            this.lblError = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblViewLog = new System.Windows.Forms.LinkLabel();
            this.lblElapsedTime = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.progressBar.Location = new System.Drawing.Point(19, 43);
            this.progressBar.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(584, 17);
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 17;
            // 
            // btnDone
            // 
            this.btnDone.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDone.Location = new System.Drawing.Point(502, 532);
            this.btnDone.Margin = new System.Windows.Forms.Padding(0);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(100, 28);
            this.btnDone.TabIndex = 10;
            this.btnDone.Text = "&Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // lbCommands
            // 
            this.lbCommands.Font = new System.Drawing.Font("Courier New", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCommands.HorizontalScrollbar = true;
            this.lbCommands.ItemHeight = 16;
            this.lbCommands.Location = new System.Drawing.Point(21, 75);
            this.lbCommands.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lbCommands.Name = "lbCommands";
            this.lbCommands.Size = new System.Drawing.Size(581, 132);
            this.lbCommands.TabIndex = 21;
            this.lbCommands.TabStop = false;
            this.lbCommands.Click += new System.EventHandler(this.lbCommands_Click);
            this.lbCommands.SelectedIndexChanged += new System.EventHandler(this.lbCommands_SelectedIndexChanged);
            this.lbCommands.DoubleClick += new System.EventHandler(this.lbCommands_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(382, 532);
            this.btnBack.Margin = new System.Windows.Forms.Padding(20, 18, 20, 18);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 11;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // txtDetails
            // 
            this.txtDetails.AcceptsReturn = true;
            this.txtDetails.AcceptsTab = true;
            this.txtDetails.BackColor = System.Drawing.SystemColors.Window;
            this.txtDetails.CausesValidation = false;
            this.txtDetails.Location = new System.Drawing.Point(21, 238);
            this.txtDetails.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.txtDetails.Multiline = true;
            this.txtDetails.Name = "txtDetails";
            this.txtDetails.ReadOnly = true;
            this.txtDetails.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDetails.Size = new System.Drawing.Size(581, 221);
            this.txtDetails.TabIndex = 3;
            // 
            // bw
            // 
            this.bw.WorkerReportsProgress = true;
            this.bw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_DoWork);
            this.bw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw_RunWorkerCompleted);
            // 
            // lblError
            // 
            this.lblError.BackColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(19, 43);
            this.lblError.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(584, 17);
            this.lblError.TabIndex = 26;
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblStatus.Location = new System.Drawing.Point(13, 15);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(0, 0, 15, 7);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(588, 20);
            this.lblStatus.TabIndex = 28;
            this.lblStatus.Text = "Ready";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblViewLog
            // 
            this.lblViewLog.Location = new System.Drawing.Point(14, 538);
            this.lblViewLog.Margin = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.lblViewLog.Name = "lblViewLog";
            this.lblViewLog.Size = new System.Drawing.Size(88, 22);
            this.lblViewLog.TabIndex = 4;
            this.lblViewLog.TabStop = true;
            this.lblViewLog.Text = "View Log";
            this.lblViewLog.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblViewLog_LinkClicked);
            // 
            // lblElapsedTime
            // 
            this.lblElapsedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblElapsedTime.Location = new System.Drawing.Point(15, 468);
            this.lblElapsedTime.Margin = new System.Windows.Forms.Padding(0, 0, 11, 15);
            this.lblElapsedTime.Name = "lblElapsedTime";
            this.lblElapsedTime.Size = new System.Drawing.Size(139, 25);
            this.lblElapsedTime.TabIndex = 30;
            this.lblElapsedTime.Text = "Elapsed time: 00:00";
            // 
            // ConfigureCluster3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnDone;
            this.ClientSize = new System.Drawing.Size(617, 575);
            this.ControlBox = false;
            this.Controls.Add(this.lblElapsedTime);
            this.Controls.Add(this.lblViewLog);
            this.Controls.Add(this.txtDetails);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.lbCommands);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.lblError);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ConfigureCluster3";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Control cluster - Apply configuration";
            this.Load += new System.EventHandler(this.ConfigureCluster3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.ListBox lbCommands;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.TextBox txtDetails;
        private System.ComponentModel.BackgroundWorker bw;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.LinkLabel lblViewLog;
        private System.Windows.Forms.Label lblElapsedTime;
    }
}