﻿namespace shadowwcat.Dialogs
{
    partial class ChangeDefaultIPAddress4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeDefaultIPAddress4));
            this.btnStartExecution = new System.Windows.Forms.Button();
            this.btnCancelExecution = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.listStatusItems = new System.Windows.Forms.ListView();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStartExecution
            // 
            this.btnStartExecution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStartExecution.Location = new System.Drawing.Point(15, 426);
            this.btnStartExecution.Margin = new System.Windows.Forms.Padding(0, 0, 15, 15);
            this.btnStartExecution.Name = "btnStartExecution";
            this.btnStartExecution.Size = new System.Drawing.Size(100, 28);
            this.btnStartExecution.TabIndex = 1;
            this.btnStartExecution.Text = "&Start";
            this.btnStartExecution.UseVisualStyleBackColor = true;
            // 
            // btnCancelExecution
            // 
            this.btnCancelExecution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelExecution.Location = new System.Drawing.Point(130, 426);
            this.btnCancelExecution.Margin = new System.Windows.Forms.Padding(0, 0, 15, 15);
            this.btnCancelExecution.Name = "btnCancelExecution";
            this.btnCancelExecution.Size = new System.Drawing.Size(100, 28);
            this.btnCancelExecution.TabIndex = 2;
            this.btnCancelExecution.Text = "Sto&p";
            this.btnCancelExecution.UseVisualStyleBackColor = true;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(15, 15);
            this.progressBar.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(500, 17);
            this.progressBar.TabIndex = 0;
            // 
            // listStatusItems
            // 
            this.listStatusItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listStatusItems.Location = new System.Drawing.Point(16, 47);
            this.listStatusItems.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.listStatusItems.Name = "listStatusItems";
            this.listStatusItems.Size = new System.Drawing.Size(499, 364);
            this.listStatusItems.TabIndex = 0;
            this.listStatusItems.TabStop = false;
            this.listStatusItems.UseCompatibleStateImageBehavior = false;
            this.listStatusItems.View = System.Windows.Forms.View.List;
            // 
            // btnDone
            // 
            this.btnDone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDone.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDone.Location = new System.Drawing.Point(417, 487);
            this.btnDone.Margin = new System.Windows.Forms.Padding(0);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(100, 28);
            this.btnDone.TabIndex = 3;
            this.btnDone.Text = "&Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Location = new System.Drawing.Point(302, 487);
            this.btnBack.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 4;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // ChangeDefaultIPAddress4
            // 
            this.AcceptButton = this.btnStartExecution;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnDone;
            this.ClientSize = new System.Drawing.Size(532, 530);
            this.ControlBox = false;
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.btnStartExecution);
            this.Controls.Add(this.btnCancelExecution);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.listStatusItems);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ChangeDefaultIPAddress4";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change default IP address ";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStartExecution;
        private System.Windows.Forms.Button btnCancelExecution;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.ListView listStatusItems;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnBack;
    }
}