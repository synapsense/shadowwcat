﻿using shadowwcat.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ConfigureClusterReview : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ConfigureClusterReview;

        public ConfigureClusterReview()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnNext;
                this.Text = "Review " + (AppSession.IsConfigureClusterBridged ? "Bridged" : "Single") + " Cluster Setup";

                chkAcknowledgeRisks.Checked = false;
                btnNext.Enabled = false;
                SetFormValues();
            }
        }

        private void AddLine(StringBuilder builder, string label, string value)
        {
            builder.Append(@"\b ");
            builder.Append(label);
            builder.Append(@"\b0 ");
            builder.Append(value);
            builder.Append(@"\line ");
        }

        private void SetFormValues()
        {
            //txtSummary.Rtf = @"{\rtf1\ansi this word is \b bold \b0 " + @"\line " + @"This line isn't.}";
            StringBuilder builder = new StringBuilder();
            builder.Append(@"{\rtf1\ansi ");

            AddLine(builder, "ES Hostname:\t\t", AppSession.ClusterConfig.ES_Hostname);
            int line = 0;
            foreach (string server in AppSession.ClusterConfig.Timeserver)
            {
                if (line == 0)
                {
                    AddLine(builder, "Time Servers:\t\t", server);
                }
                else
                {
                    AddLine(builder, "\t\t\t", server);
                }
                line++;
            }
            if (AppSession.ClusterConfig.Nameserver.Length > 0)
            {
                line = 0;
                foreach (string server in AppSession.ClusterConfig.Nameserver)
                {
                    if (line == 0)
                    {
                        AddLine(builder, "DNS Servers:\t\t", server);
                    } else
                    {
                        AddLine(builder, "\t\t\t", server);
                    }
                    line++;
                }
            }

            AddLine(builder, "", "");
            AddLine(builder, "Default Gateway:\t", AppSession.ClusterConfig.Net1_Gateway);
            if (!AppSession.ClusterConfig.Net1_DHCP)
            {
                AddLine(builder, "Port 1 Subnet Mask:\t", AppSession.ClusterConfig.Net1_Netmask);
            }

            if (AppSession.ClusterConfig.Net2_Enabled)
            {
                if (!AppSession.ClusterConfig.Net2_DHCP)
                {
                    AddLine(builder, "Port 2 Subnet Mask:\t", AppSession.ClusterConfig.Net2_Netmask);
                }
            }

            AddLine(builder, "", "");
            AddLine(builder, "Controller 1", "");
            AddLine(builder, "\tHostname:\t", AppSession.ClusterConfig.Appliance_1_Hostname);
            if (!AppSession.ClusterConfig.Net1_DHCP && ! AppSession.UseDNS)
            {
                AddLine(builder, "\tPort 1 IP:\t", AppSession.ClusterConfig.Appliance_1_Port1_IP);
            }
            if (AppSession.ClusterConfig.Net2_Enabled)
            {
                if (!AppSession.ClusterConfig.Net2_DHCP)
                {
                    AddLine(builder, "\tPort 2 IP:\t", AppSession.ClusterConfig.Appliance_1_Port2_IP);
                }
            }

            AddLine(builder, "", "");
            AddLine(builder, "Controller 2", "");
            AddLine(builder, "\tHostname:\t", AppSession.ClusterConfig.Appliance_2_Hostname);
            if (!AppSession.ClusterConfig.Net1_DHCP && !AppSession.UseDNS)
            {
                AddLine(builder, "\tPort 1 IP:\t", AppSession.ClusterConfig.Appliance_2_Port1_IP);
            }
            if (AppSession.ClusterConfig.Net2_Enabled)
            {
                if (!AppSession.ClusterConfig.Net2_DHCP)
                {
                    AddLine(builder, "\tPort 2 IP:\t", AppSession.ClusterConfig.Appliance_2_Port2_IP);
                }
            }

            builder.Append("}");

            txtSummary.Rtf = builder.ToString();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!Navigator.Execute(this, Navigator.TaskRequest.EnterAppliancePasswords))
            {
                return;
            }

            Navigator.GoTo(me, Navigator.DialogName.ConfigureCluster3);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (AppSession.ClusterConfig.Nameserver.Length > 0)
            {
                Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterReviewHosts);
            }
            else if (AppSession.IsConfigureClusterBridged)
            {
                Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterPort2);
            }
            else
            {
                Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterPort1);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        private void chkAcknowledgeRisks_CheckedChanged(object sender, EventArgs e)
        {
            btnNext.Enabled = chkAcknowledgeRisks.Checked;
        }

        private void lnkResetValues_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Utilities.ResetValues(this.me);
        }

    }
}
