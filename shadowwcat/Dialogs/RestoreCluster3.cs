﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System.Threading;

namespace shadowwcat.Dialogs
{
    public partial class RestoreCluster3 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.RestoreCluster3;
        private BackgroundWorker bwStartClusterCheck = new System.ComponentModel.BackgroundWorker();

        internal struct CheckClusterProgress
        {
            internal bool worked;
            internal string status;
            internal string cluster1ConnectStatus;
            internal string cluster1Status;
            internal string cluster1Version;
            internal string cluster2ConnectStatus;
            internal string cluster2Status;
            internal string cluster2Version;
        }

        public RestoreCluster3()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                if (btnCancel.Enabled)
                {
                    Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
                }
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnNext;

                btnNext.Enabled = false;
                btnCancel.Enabled = false;
                btnBack.Enabled = false;

                txtStatus.Text = "Retrieving cluster information..." + Environment.NewLine;

                this.bwStartClusterCheck.WorkerReportsProgress = true;
                this.bwStartClusterCheck.DoWork += new DoWorkEventHandler(bwStartClusterCheck_DoWork);
                this.bwStartClusterCheck.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwStartClusterCheck_RunWorkerCompleted);
                this.bwStartClusterCheck.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwStartClusterCheck_ProgressChanged);
                this.bwStartClusterCheck.RunWorkerAsync();

            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Navigator.Next(me);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearRunData();
            btnNext.Enabled = false;
            Navigator.Back(me);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearRunData();
            btnNext.Enabled = false;
            Navigator.Cancel(me);
        }

        void bwStartClusterCheck_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            Thread.Sleep(100);

            string[] output = new string[2];
            CheckClusterProgress progress = new CheckClusterProgress();
            progress.worked = false;

            Command c1 = new Command();
            Command c2 = new Command();
            c1.ApplianceHostname = AppSession.ClusterConfig.Appliance_1_Hostname;
            c1.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_1_Port1_IP;
            c1.AppliancePassword = AppSession.Appliance1Password;

            c2.ApplianceHostname = AppSession.ClusterConfig.Appliance_2_Hostname;
            c2.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_2_Port1_IP;
            c2.AppliancePassword = AppSession.Appliance2Password;

            progress.status = "Retrieving appliance 1 information... (up to 60 seconds)" + Environment.NewLine;
            worker.ReportProgress(0, progress);

            c1.MethodToRun = (Action)c1.GetVersionAndRunStatus;
            bw.RunWorkerAsync(c1);
            waitfortask();
            if (c1.ErrorOccurred)
            {
                progress.status = string.Format("Controller 1 - failed to connect: {0} - {1}", c1.ApplianceHostname, c1.FormattedErrorMessage);
                worker.ReportProgress(100, progress);
            }
            else
            {
                Logger.Log(Logger.Levels.Info, "shadow rcver output" + output[0] + output[1]);

                output = c1.CommandData.Split('|');

                progress.status = "Retrieving appliance 2 information... (up to 60 seconds)" + Environment.NewLine;
                progress.cluster1ConnectStatus = "OK";
                progress.cluster1Version = output[0];
                progress.cluster1Status = (output[1].Contains("_enable=\"Y") ? "Running" : "Provisional");
                worker.ReportProgress(50, progress);

                //Store upgrade info - we'll need it downstream.
                AppSession.c1UpgradeInfo.ApplianceHostName = c1.ApplianceHostname;
                AppSession.c1UpgradeInfo.RunningVersion = progress.cluster1Version;
                AppSession.c1UpgradeInfo.IsRestorationTarget = (progress.cluster1Status == "Provisional");

                c2.MethodToRun = (Action)c2.GetVersionAndRunStatus;
                bw.RunWorkerAsync(c2);
                waitfortask();
                if (c2.ErrorOccurred)
                {
                    progress.status = string.Format("Controller 2 - failed to connect: {0} - {1}", c2.ApplianceHostname, c2.FormattedErrorMessage);
                    worker.ReportProgress(100, progress);
                }
                else
                {
                    output = c2.CommandData.Split('|');

                    progress.status = null;
                    progress.cluster2ConnectStatus = "OK";
                    progress.cluster2Version = output[0];
                    progress.cluster2Status = (output[1].Contains("_enable=\"Y") ? "Running" : "Provisional");

                    //Store upgrade info - we'll need it downstream.
                    AppSession.c2UpgradeInfo.ApplianceHostName = c2.ApplianceHostname;
                    AppSession.c2UpgradeInfo.RunningVersion = progress.cluster2Version;
                    AppSession.c2UpgradeInfo.IsRestorationTarget = (progress.cluster2Status == "Provisional");

                    //Determine the version needed for upgrading the restored appliance.
                    if (AppSession.c1UpgradeInfo.IsRestorationTarget)
                    {
                        AppSession.c1UpgradeInfo.UpgradeToVersion = AppSession.c2UpgradeInfo.RunningVersion;
                    }
                    if (AppSession.c2UpgradeInfo.IsRestorationTarget)
                    {
                        AppSession.c2UpgradeInfo.UpgradeToVersion = AppSession.c1UpgradeInfo.RunningVersion;
                    }

                    //if neither appliance is a restoration target, report error here.
                    if (!(AppSession.c1UpgradeInfo.IsRestorationTarget || AppSession.c2UpgradeInfo.IsRestorationTarget))
                    {
                        progress.status = Environment.NewLine + "Neither control appliance appears to need restoration." + Environment.NewLine + "Press Cancel to exit.";
                        worker.ReportProgress(100, progress);
                    }
                    else
                    {
                        progress.status = Environment.NewLine + "Command completed successfully." + Environment.NewLine + "Press Next to continue.";
                        progress.worked = true;
                        worker.ReportProgress(100, progress);
                    }
                }
            }

        }

        private void bwStartClusterCheck_RunWorkerCompleted(
            object sender,
            RunWorkerCompletedEventArgs e)
        {
            btnCancel.Enabled = true;
            btnBack.Enabled = true;
        }

        private void bwStartClusterCheck_ProgressChanged(
            object sender,
            ProgressChangedEventArgs e)
        {
            CheckClusterProgress progress = (CheckClusterProgress)e.UserState;
            if (e.ProgressPercentage == 100)
            {
                if (progress.worked)
                {
                    btnNext.Enabled = true;
                    btnNext.Focus();
                } else
                {
                    btnCancel.Enabled = true;
                    btnCancel.Focus();
                }
            }

            if (progress.status != null)
            {
                txtStatus.Text += progress.status;
            }

            if (progress.cluster1ConnectStatus != null)
            {
                lblCluster1ConnectStatus.Text = progress.cluster1ConnectStatus;
            }

            if (progress.cluster1Status != null)
            {
                lblCluster1RunStatus.Text = progress.cluster1Status;
            }

            if (progress.cluster1Version != null)
            {
                lblCluster1RunVersion.Text = progress.cluster1Version;
            }

            if (progress.cluster2ConnectStatus != null)
            {
                lblCluster2ConnectStatus.Text = progress.cluster2ConnectStatus;
            }

            if (progress.cluster2Status != null)
            {
                lblCluster2RunStatus.Text = progress.cluster2Status;
            }

            if (progress.cluster2Version != null)
            {
                lblCluster2RunVersion.Text = progress.cluster2Version;
            }
        }

        private void RestoreCluster3_Load(object sender, EventArgs e)
        {
            ClearRunData();
            btnNext.Enabled = false;
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            Command command = (Command)e.Argument;
            Action arg = command.Run;
            try
            {
                arg();
                e.Result = !command.ErrorOccurred;
            }
            catch (Exception ce)
            {
                Logger.Log(Logger.Levels.Info, "task threw exception: " + ce.Message);
                e.Result = false;
            }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logger.Log(Logger.Levels.Info, "task failed. e.Error = " + e.Error.Message);
            }
        }
        private void waitfortask()
        {
            while (bw.IsBusy)
            {
                // Keep UI messages moving, so the form remains  
                // responsive during the asynchronous operation.
                Application.DoEvents();
            }
        }
        private void ClearRunData()
        {
            txtStatus.Text = "Press Check Cluster to retrieve appliance information.";
            lblCluster1ConnectStatus.Text = string.Empty;
            lblCluster2ConnectStatus.Text = string.Empty;
            lblCluster1RunVersion.Text = string.Empty;
            lblCluster2RunVersion.Text = string.Empty;
            lblCluster1RunStatus.Text = string.Empty;
            lblCluster2RunStatus.Text = string.Empty;
        }
    }
}
