﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Timers;
using System.IO;
using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System.Diagnostics;

namespace shadowwcat.Dialogs
{
    public partial class ClusterHealth : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ClusterHealth;
        CommandRunner cr = new CommandRunner();        
        private BackgroundWorker bwPauseBeforeStarting = new System.ComponentModel.BackgroundWorker();
        private bool passwordCheck { get; set; }
        private bool executingCommands{ get; set; }

        public ClusterHealth()
        {
            InitializeComponent();
            cr.TaskName = "Cluster Health";
            cr.TaskDetails = "Get Cluster Health Report";
            cr.Progress = progressBar;
            cr.ProgressFail = lblError;
            cr.CommandList = lbCommands;
            cr.Details = detailsTxt;
            cr.StartExecution = btnDone;
            cr.ElapsedTime = lblElapsedTime;
            cr.bw = backgroundWorker1;
            cr.Status = lblStatus;
            cr.Done = btnDone;
            cr.Back = btnDone;
            btnSave.Enabled = false;
            
            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);

            this.passwordCheck = true;
            this.executingCommands = false;
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                if (!this.executingCommands)
                {
                    Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
                }
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e) {
            if (this.Visible && this.passwordCheck)
            {
                startExecution(sender, e);
            } else
            {
                this.passwordCheck = true; // for next run.
                Navigator.Close(me);
            }         
        }

        private void btnDone_Click(object sender, EventArgs e)
        {            
            cr.ClearRunData();
            Navigator.Close(me);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog.OpenFile()) != null)
                {
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(detailsTxt.Text);                        
                    long offset = 0;
                    long remainigBytes = bytes.Length;

                    while (offset  < bytes.Length)
                    {
                        myStream.Write(bytes, (int)offset, (int)remainigBytes);
                        offset += myStream.Position;
                        remainigBytes -= offset;
                    }
                    myStream.Close();
                }
            }

        }

        public void startExecution(object sender, System.EventArgs e)
        {            
            this.Show();
            this.Focus();            
            if (!Navigator.Execute(this, Navigator.TaskRequest.EnterAppliancePasswords))
            {
                this.Hide();
                this.passwordCheck = false;
                Navigator.Close(me);
                return;
            }

            this.executingCommands = true;

            var command1 = new Command();
            command1.ApplianceHostname = AppSession.ClusterConfig.Appliance_1_Hostname;
            command1.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_1_Port1_IP;
            command1.AppliancePassword = AppSession.Appliance1Password;
            

            var command2 = new Command();
            command2.ApplianceHostname = AppSession.ClusterConfig.Appliance_2_Hostname;
            command2.ApplianceIPAddress = AppSession.ClusterConfig.Appliance_2_Port1_IP;
            command2.AppliancePassword = AppSession.Appliance2Password;
            

            lbCommands.BeginUpdate();
            lbCommands.Items.Clear();
            lbCommands.Items.Add(new CommandInfo("1. Test appliance(s) connectivity...", new Action(command1.TestConnection), new Action(command2.TestConnection)));
            lbCommands.Items.Add(new CommandInfo("2. Check Cluster health on Appliance 1 & Appliance 2...", new Action(command1.ApplianceHealth), new Action(command2.ApplianceHealth)));
            lbCommands.EndUpdate();
            lbCommands.DisplayMember = "CommandName"; //this property gets displayed in the listbox.

            bool result = cr.PerformTask(command1, command2);

            this.executingCommands = false;
            
            if (result)
            {
                lbCommands.SetSelected(1, true);
                detailsTxt.Text = ((CommandInfo)(lbCommands.SelectedItem)).Results;
                btnSave.Enabled = true;
            }
            else
            {
                detailsTxt.Text = cr.ProgressFail.Text;
                btnSave.Enabled = false;
            }
            detailsTxt.Update();         
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            Command command = (Command)e.Argument;
            Action arg = command.Run;
            try
            {
                arg();
                e.Result = !command.ErrorOccurred;
            }
            catch (Exception ce)
            {
                Logger.Log(Logger.Levels.Info, "task threw exception: " + ce.Message);
                e.Result = false;
            }
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logger.Log(Logger.Levels.Info, "task failed. e.Error = " + e.Error.Message);
            }
        }

        private void lblElapsedTime_Click(object sender, EventArgs e)
        {

        }

        private void lbCommands_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                cr.UpdateDetailsText(((CommandInfo)(lbCommands.SelectedItem)).Results);
                detailsTxt.Text = cr.Details.Text;
                detailsTxt.Update();
            }
        }

        private void lbCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                if (lbCommands.SelectedIndex != -1)
                {
                    cr.UpdateDetailsText(((CommandInfo)(lbCommands.SelectedItem)).Results);
                }
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {            
            cr.ClearRunData();
            Navigator.Close(me);
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblViewLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("notepad", Logger.LogFilename);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "View log file failed");
                Logger.Log(Logger.Levels.Error, exc.Message);
            }
        }
    }
}
