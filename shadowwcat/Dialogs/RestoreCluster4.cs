﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using shadowwcat.Data;
using System.IO;

namespace shadowwcat.Dialogs
{
    public partial class RestoreCluster4 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.RestoreCluster4;

        public RestoreCluster4()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = txtFirmwareImageFilename;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.Back(me);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            AppSession.ApplianceFirmwareFile = txtFirmwareImageFilename.Text;
            Navigator.Next(me);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtFirmwareImageFilename.Clear();
            lblVersionInfo.Text = "Select a firmware image file, then press Next.";
            pictureBoxStop.Visible = false;
            Navigator.Cancel(me);
        }

        private void RestoreCluster4_Load(object sender, EventArgs e)
        {
            pictureBoxStop.Visible = false;
            lblVersionInfo.Text = "Select a firmware image file, then press Next.";
        }

        private void btnBrowseFirmware_Click(object sender, EventArgs e)
        {
            if (openFileDialogFirmware.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFirmwareImageFilename.Text = openFileDialogFirmware.FileName;
                if (!File.Exists(txtFirmwareImageFilename.Text)) //This is redundant for now since we're only taking return values from the file chooser. Could change, though.
                {
                    lblVersionInfo.Text = string.Format("Name '{0}' is invalid. Try again.", txtFirmwareImageFilename.Text);
                    btnNext.Enabled = false;
                }
                else
                {
                    VersionCheck();
                }
            }

        }
        private void VersionCheck()
        {
            btnNext.Enabled = false;
            string FirmwareFileVersion = string.Empty;
            pictureBoxStop.Visible = false;

            if (String.IsNullOrEmpty(txtFirmwareImageFilename.Text))
            {
                lblVersionInfo.Text = "Select a firmware image file, then press Next.";
            }
            else
            {
                if (Utils.VersionOps.DoesFWVersionMatch(txtFirmwareImageFilename.Text, GetTargetVersion()))
                {
                    lblVersionInfo.Text = string.Format("Firmware image v{0} is correct version for restoration. Press Next to continue.",
                        Utils.VersionOps.FirmwareVersion, AppSession.EnvironmentalServerVersion);
                    btnNext.Enabled = true;
                }
                else
                {
                    pictureBoxStop.Visible = true;
                    lblVersionInfo.Text = string.Format("{0}.\r\n\r\nChoose a different firmware image file.", Utils.VersionOps.ErrorMessage);
                }
            }
        }
        private string GetTargetVersion()
        {
            string targetVersion = string.Empty;

            if (AppSession.c1UpgradeInfo.IsRestorationTarget)
            {
                targetVersion = AppSession.c1UpgradeInfo.UpgradeToVersion;
            }
            else if (AppSession.c2UpgradeInfo.IsRestorationTarget)
            {
                targetVersion = AppSession.c2UpgradeInfo.UpgradeToVersion;
            }
            return targetVersion;
        }

    }
}
