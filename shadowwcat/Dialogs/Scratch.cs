﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class Scratch : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.Scratch;

        public Scratch()
        {
            InitializeComponent();
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            btnCancel.Enabled = false;
        }

        private void btnDoStuff_Click(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                progressBar.Value = 0;
                lblDoStuffResults.Text = "Working...";
                btnCancel.Enabled = true;
                bw.RunWorkerAsync();
            }
            else
            {
                btnCancel.Enabled = false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (bw.WorkerSupportsCancellation)
            {
                bw.CancelAsync();
            }
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            for (int i = 1; i <= 100; i++)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(100);
                    worker.ReportProgress(i);
                }
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblDoStuffResults.Text = string.Format("progress change is {0}", e.ProgressPercentage);
            progressBar.Value = e.ProgressPercentage;
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnCancel.Enabled = false;
            progressBar.Value = 100;
            if (e.Cancelled)
            {
                lblDoStuffResults.Text = "Cancelled!";
            }
            else if (e.Error != null)
            {
                lblDoStuffResults.Text = "Error: " + e.Error.Message;
            }
            else
            {
                lblDoStuffResults.Text = "Finished.";
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            Navigator.ExitApp();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {

            Navigator.Next(me); 
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Navigator.Back(me);
        }

        private void Scratch_Load(object sender, EventArgs e)
        {
        }

        private void mtxtIPAddress_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            //MessageBox.Show("Enter a valid IP Address");
        }

        private void mtxtIPAddress_Leave(object sender, EventArgs e)
        {
            //validate ip address
            MessageBox.Show("Enter a valid IP Address");
        }




    }
}
