﻿using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ChangePasswordResults : Form
    {
        delegate void SetTextCallback(TextBox txtBox, string text);

        private Navigator.DialogName me = Navigator.DialogName.ChangePasswordResults;

        public ChangePasswordResults()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                ClearFormData();
                if (btnDone.Enabled)
                {
                    Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
                }
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnDone;

                btnBack.Enabled = false;
                btnDone.Enabled = false;

                this.bw.RunWorkerAsync();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ClearFormData();
            Navigator.Back(me);
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            ClearFormData();
            Navigator.Close(me);
        }

        private void ClearFormData()
        {
            txtResult.Clear();
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            Thread.Sleep(100);

            worker.ReportProgress(50, "Connecting to appliance and changing password...");

            var command = new Command();
            command.ApplianceHostname = AppSession.changePasswordInfo.hostName;
            command.ApplianceIPAddress = AppSession.changePasswordInfo.IPAddress;
            command.AppliancePassword = AppSession.changePasswordInfo.oldPassword;
            command.ChangePassword(AppSession.changePasswordInfo.oldPassword, AppSession.changePasswordInfo.newPassword);
            

            string text = "Password changed!";
            if (command.ErrorOccurred)
            {
                text = "Password change failed!" + "\r\n" + command.CommandResults;
            }
            
            worker.ReportProgress(100, text);
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txtResult.Text += e.UserState as string + Environment.NewLine;
            txtResult.Refresh();

            if (e.ProgressPercentage == 100)
            {
                btnBack.Enabled = true;
                btnDone.Enabled = true;
                btnDone.Focus();
            }
        }

        private void txnResult_TextChanged(object sender, EventArgs e)
        {
            //
        }

    }
}
