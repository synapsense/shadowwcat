﻿using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace shadowwcat.Dialogs
{
    public partial class ConfigureClusterReviewHosts : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ConfigureClusterReviewHosts;
        private bool hostsFileUpdated = false;

        public ConfigureClusterReviewHosts()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnNext;
                hostsFileUpdated = false;

                SetFormValues();
            }
        }

        private void SetFormValues()
        {
            if (File.Exists(HostFileGen.NewHostsFile))
            {
                txtHostsFile.Text = File.ReadAllText(HostFileGen.NewHostsFile);

            } else if (File.Exists(HostFileGen.HostsFile))
            {
                txtHostsFile.Text = File.ReadAllText(HostFileGen.HostsFile);
            } else
            {
                txtHostsFile.Text = "";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (AppSession.IsConfigureClusterBridged)
            {
                Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterPort2);
            } else
            {
                Navigator.GoTo(me, Navigator.DialogName.ConfigureClusterPort1);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            hostsFileUpdated = hostsFileUpdated ? hostsFileUpdated : IsHostFileUpdated();
            if (hostsFileUpdated)
            {
                Navigator.Next(me);
            }
        }

        private bool IsHostFileUpdated()
        {
            bool HostChangesCommitted = false;

            if (HostFileGen.IsHostsUpToDate())
            {
                HostChangesCommitted = true;
            }
            else
            {
                MessageBox.Show("FIRST STEP: Before configuring the control appliances, updates must be made to the system hosts file.\r\n" +
                "Otherwise, data connections between the Environmental server and the appliances will fail.\r\n\r\n" +
                "Click OK to attempt an automatic update. This might generate an error or warning from your Antivirus software. If the update fails, you will receive\r\n" +
                "instructions for manually editing the hosts file.", "Attempting to update system hosts file");
                if (HostFileGen.CommitHostsFileChanges())
                {
                    HostChangesCommitted = true;
                }
                else
                {
                    MessageBox.Show(HostFileGen.ErrorMessage, "Warning: Shadoww CAT update of hosts file failed");
                    if (MessageBox.Show("I have updated the hosts file manually. Continue cluster configuration.", "OK to continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        HostChangesCommitted = true;
                    }
                }
            }

            return HostChangesCommitted;
        }
    }
}
