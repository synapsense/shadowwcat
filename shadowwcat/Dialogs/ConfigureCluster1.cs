﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using shadowwcat.Data;

namespace shadowwcat.Dialogs
{
    public partial class ConfigureCluster1 : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.ConfigureCluster1;

        public ConfigureCluster1()
        {
            InitializeComponent();

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnNext;
                SetFormValues();
            }
        }

        private void SetFormValues()
        {
            rbBridged.Checked = AppSession.IsConfigureClusterBridged;
            rbSingle.Checked = !AppSession.IsConfigureClusterBridged;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Navigator.Cancel(me);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            AppSession.IsConfigureClusterBridged = rbBridged.Checked;
            AppSession.ClusterConfig.Net2_Enabled = AppSession.IsConfigureClusterBridged;
            Navigator.Next(me);
        }

        private void lnkResetValues_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Utilities.ResetValues(this.me);
        }
    }
}
