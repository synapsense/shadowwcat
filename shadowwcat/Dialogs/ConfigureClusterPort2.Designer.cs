﻿namespace shadowwcat.Dialogs
{
    partial class ConfigureClusterPort2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigureClusterPort2));
            this.label1 = new System.Windows.Forms.Label();
            this.mtxtPort2Netmask = new IPAddressControlLib.IPAddressControl();
            this.mtxtController1Port2IPAddress = new IPAddressControlLib.IPAddressControl();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mtxtController2Port2IPAddress = new IPAddressControlLib.IPAddressControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lnkResetValues = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(114, 70);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Subnet mask:";
            // 
            // mtxtPort2Netmask
            // 
            this.mtxtPort2Netmask.AllowInternalTab = false;
            this.mtxtPort2Netmask.AutoHeight = true;
            this.mtxtPort2Netmask.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtPort2Netmask.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtPort2Netmask.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtPort2Netmask.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mtxtPort2Netmask.Location = new System.Drawing.Point(218, 67);
            this.mtxtPort2Netmask.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.mtxtPort2Netmask.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtPort2Netmask.Name = "mtxtPort2Netmask";
            this.mtxtPort2Netmask.ReadOnly = false;
            this.mtxtPort2Netmask.Size = new System.Drawing.Size(115, 22);
            this.mtxtPort2Netmask.TabIndex = 2;
            this.mtxtPort2Netmask.Text = "...";
            // 
            // mtxtController1Port2IPAddress
            // 
            this.mtxtController1Port2IPAddress.AllowInternalTab = false;
            this.mtxtController1Port2IPAddress.AutoHeight = true;
            this.mtxtController1Port2IPAddress.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtController1Port2IPAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtController1Port2IPAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtController1Port2IPAddress.Location = new System.Drawing.Point(129, 39);
            this.mtxtController1Port2IPAddress.Margin = new System.Windows.Forms.Padding(0);
            this.mtxtController1Port2IPAddress.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtController1Port2IPAddress.Name = "mtxtController1Port2IPAddress";
            this.mtxtController1Port2IPAddress.ReadOnly = false;
            this.mtxtController1Port2IPAddress.Size = new System.Drawing.Size(115, 22);
            this.mtxtController1Port2IPAddress.TabIndex = 4;
            this.mtxtController1Port2IPAddress.Text = "...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 42);
            this.label2.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Port 2 IP:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtxtController1Port2IPAddress);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(89, 156);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0, 15, 15, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(20, 18, 20, 0);
            this.groupBox1.Size = new System.Drawing.Size(393, 113);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Controller 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 0, 10, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Port 2 IP:";
            // 
            // mtxtController2Port2IPAddress
            // 
            this.mtxtController2Port2IPAddress.AllowInternalTab = false;
            this.mtxtController2Port2IPAddress.AutoHeight = true;
            this.mtxtController2Port2IPAddress.BackColor = System.Drawing.SystemColors.Window;
            this.mtxtController2Port2IPAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mtxtController2Port2IPAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.mtxtController2Port2IPAddress.Location = new System.Drawing.Point(129, 39);
            this.mtxtController2Port2IPAddress.Margin = new System.Windows.Forms.Padding(0);
            this.mtxtController2Port2IPAddress.MinimumSize = new System.Drawing.Size(111, 22);
            this.mtxtController2Port2IPAddress.Name = "mtxtController2Port2IPAddress";
            this.mtxtController2Port2IPAddress.ReadOnly = false;
            this.mtxtController2Port2IPAddress.Size = new System.Drawing.Size(115, 22);
            this.mtxtController2Port2IPAddress.TabIndex = 6;
            this.mtxtController2Port2IPAddress.Text = "...";
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.mtxtController2Port2IPAddress);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(89, 285);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(20, 18, 20, 0);
            this.groupBox2.Size = new System.Drawing.Size(393, 113);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Controller 2";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(502, 532);
            this.btnNext.Margin = new System.Windows.Forms.Padding(0);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(100, 28);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "&Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(382, 532);
            this.btnBack.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(100, 28);
            this.btnBack.TabIndex = 11;
            this.btnBack.Text = "&Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(15, 532);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lnkResetValues
            // 
            this.lnkResetValues.Location = new System.Drawing.Point(262, 538);
            this.lnkResetValues.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.lnkResetValues.Name = "lnkResetValues";
            this.lnkResetValues.Size = new System.Drawing.Size(92, 17);
            this.lnkResetValues.TabIndex = 13;
            this.lnkResetValues.TabStop = true;
            this.lnkResetValues.Text = "Reset Values";
            this.lnkResetValues.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkResetValues_LinkClicked);
            // 
            // ConfigureClusterPort2
            // 
            this.AcceptButton = this.btnNext;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(617, 575);
            this.ControlBox = false;
            this.Controls.Add(this.lnkResetValues);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mtxtPort2Netmask);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ConfigureClusterPort2";
            this.Padding = new System.Windows.Forms.Padding(15);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configure cluster - Facility network settings (controller port 2)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private IPAddressControlLib.IPAddressControl mtxtPort2Netmask;
        private IPAddressControlLib.IPAddressControl mtxtController1Port2IPAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private IPAddressControlLib.IPAddressControl mtxtController2Port2IPAddress;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.LinkLabel lnkResetValues;
    }
}