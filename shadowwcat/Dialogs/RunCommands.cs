﻿using shadowwcat.ApplianceOps;
using shadowwcat.Data;
using shadowwcat.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;

namespace shadowwcat.Dialogs
{
    public partial class RunCommands : Form
    {
        private Navigator.DialogName me = Navigator.DialogName.RunCommands;
        CommandRunner cr = new CommandRunner();
        private BackgroundWorker bwPauseBeforeStarting = new System.ComponentModel.BackgroundWorker();
        private bool startBackgroundThread = true;

        public RunCommands()
        {
            InitializeComponent();

            cr.Status = lblStatus;
            cr.Progress = progressBar;
            cr.ProgressFail = lblError;
            cr.CommandList = lbCommands;
            cr.Details = txtDetails;
            cr.ElapsedTime = lblElapsedTime;
            cr.StartExecution = btnDone;
            cr.Done = btnDone;
            cr.Back = btnBack;
            cr.bw = bw;

            this.VisibleChanged += new System.EventHandler(this.Form_VisibleChanged);
            this.FormClosing += new FormClosingEventHandler(this.Form_Closing);

            this.bwPauseBeforeStarting.DoWork += new DoWorkEventHandler(bwPauseBeforeStarting_DoWork);
            this.bwPauseBeforeStarting.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwPauseBeforeStarting_RunWorkerCompleted);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                if (btnDone.Enabled)
                {
                    Navigator.GoTo(this.me, Navigator.DialogName.MainForm);
                }
            }
        }

        private void Form_VisibleChanged(object sender, System.EventArgs e)
        {
            if (this.Visible)
            {
                this.ActiveControl = btnDone;

                switch (AppSession.verifyType)
                {
                    case AppSession.VERIFY_TYPE.RESTORE_CLUSTER:
                        this.Text = "Restore cluster";
                        break;
                    case AppSession.VERIFY_TYPE.FULL_UPGRADE:
                        this.Text = "Upgrade firmware - Full upgrade";
                        break;
                    case AppSession.VERIFY_TYPE.FIRMWARE_ONLY_UPGRADE:
                        this.Text = "Upgrade firmware - Firmware only";
                        break;
                    case AppSession.VERIFY_TYPE.RESUME_FULL_UPGRADE:
                        this.Text = "Upgrade firmware - Resume upgrade";
                        break;
                }

                if (this.startBackgroundThread)
                {
                    this.bwPauseBeforeStarting.RunWorkerAsync();
                    this.startBackgroundThread = false;
                }
            }
        }

        private void bwPauseBeforeStarting_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (CreateAndRunTask())
            {
                //Set the resume upgrade flag in the registry.
                AppSession.IsResumeAfterUpgrade = AppSession.verifyType == AppSession.VERIFY_TYPE.FULL_UPGRADE;
            }
        }

        private void bwPauseBeforeStarting_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(100);
        }

        private bool CreateAndRunTask()
        {
            cr.TaskName = AppSession.commandRunnerInfo.taskName;
            cr.TaskDetails = AppSession.commandRunnerInfo.taskDetails;
            cr.StartButtonName = AppSession.commandRunnerInfo.startButtonName;
            cr.Done = btnDone;
            cr.Back = btnBack;

            cr.CustomSuccessInstructions = AppSession.commandRunnerInfo.customSuccessInstructions;
            cr.CustomErrorInstructions = AppSession.commandRunnerInfo.customErrorInstructions;

            lbCommands.BeginUpdate();
            lbCommands.Items.Clear();
            foreach (var cmd in AppSession.commandRunnerInfo.commands)
            {
                lbCommands.Items.Add(cmd);
            }
            lbCommands.EndUpdate();
            lbCommands.DisplayMember = "CommandName"; //this property gets displayed in the listbox.

            return cr.PerformTask(AppSession.commandRunnerInfo.ctrl1, AppSession.commandRunnerInfo.ctrl2);
        }

        private void UpgradeFirmware3Full_Load(object sender, EventArgs e)
        {
            cr.ClearRunData();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.startBackgroundThread = true;
            cr.ClearRunData();
            Navigator.Close(me);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.startBackgroundThread = true;
            AppSession.IsResumeAfterUpgrade = false;
            cr.ClearRunData();

            Navigator.DialogName previous = Navigator.DialogName.VerifyClusterChange;

            Navigator.GoTo(Navigator.DialogName.RunCommands, previous);
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            Command command = (Command)e.Argument;
            Action arg = command.Run;
            try
            {
                arg();
                e.Result = !command.ErrorOccurred;
            }
            catch (Exception ce)
            {
                Logger.Log(Logger.Levels.Info, "task threw exception: " + ce.Message);
                e.Result = false;
            }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Logger.Log(Logger.Levels.Info, "task failed. e.Error = " + e.Error.Message);
            }
        }

        private void lbCommands_Click(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                cr.UpdateDetailsText(((CommandInfo)(lbCommands.SelectedItem)).Results);
            }
        }

        private void lbCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bw.IsBusy)
            {
                if (lbCommands.SelectedIndex != -1)
                {
                    cr.UpdateDetailsText(((CommandInfo)(lbCommands.SelectedItem)).Results);
                }
            }
        }

        private void lblViewLog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start("notepad", Logger.LogFilename);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "View log file failed");
                Logger.Log(Logger.Levels.Error, exc.Message);
            }
        }

    }
}
