﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using shadowwcat.Data;
using shadowwcat.Dialogs;
using System.Net;
using System.Threading;
using shadowwcat.Utils;


namespace shadowwcat
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.Automatic);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Logger.Init(Application.CommonAppDataPath);
            Logger.Log(Logger.Levels.Info, "==================== SHADOWW CAT started. ====================");

            // Check user perms - must be admin? Code Access Security...
            // Put try/catch around this for startup errors?
            try
            {
                AppSession.Init(Application.CommonAppDataPath, Application.CommonAppDataRegistry);
            }
            catch (System.UnauthorizedAccessException)
            {
                MessageBox.Show("Please re-run SHADOWW CAT with Administrator privileges.");
                return;
            }
            catch (NotSupportedException e)
            {
                MessageBox.Show(string.Format("The SynapSense ContolAdmin tool cannot be run. {0}", e.Message));
                return;
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Exception: {0}", e.ToString()));
                return;
            }
            AppSession.ShadowwCatVersion = Application.ProductVersion;

            Logger.Log(Logger.Levels.Info, "Application initialized.");

            if (AppSession.IsEnvironmentalServer)
            {
                Logger.Log(Logger.Levels.Info, "Environmental server detected.");
            }

            Application.Run(Navigator.Run());

            Navigator.ExitApp();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(string.Format("SHADOWW CAT experienced an unhandled domain exception. {0}.\r\nDetails: {1}", e.ToString(), e.ExceptionObject.ToString()));
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show(string.Format("SHADOWW CAT experienced an unhandled thread exception. {0}.\r\nDetails: {1}", e.ToString(), e.Exception.Message));
        }
    }
}
