﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.CompilerServices;

namespace shadowwcat.Utils
{
    /// <summary>
    /// Homegrown logging utility for Shadoww Cat.
    /// </summary>
    internal static class Logger
    {
        internal enum Levels
        {
            Info,
            Warning,
            Error
        }

        private static string logFilename;
        private static readonly string logFileShortName = @"shadowwcat.log";

        internal static string LogFilename
        {
            get { return logFilename; }
        }

        internal static void Init(string LogFilePath)
        {
            logFilename = LogFilePath + @"\" + logFileShortName;
            if (!File.Exists(logFilename))
            {
                using (StreamWriter fs = File.CreateText(logFilename))
                {
                    fs.WriteLine(String.Format("Shadoww Cat log file created: {0}\r\n", DateTime.Now));
                }
            }
        }
        internal static void Log(Levels Level, string Message,
                                 [CallerFilePath] string filePath = "",
                                 [CallerLineNumber] int lineNumber = 0)
        {
            try
            {
                using (StreamWriter fs = File.AppendText(logFilename))
                {
                    string finalMessage = (Message == null) ? "NULL" : Message.Replace("\n", "\r\n");
#if DEBUG
                    string[] pathArr = filePath.Split('\\');
                    string fileName = pathArr.Last().ToString();
                    fs.WriteLine(String.Format("{0}  [{1}] ({2}:{3})  {4}", DateTime.Now, Enum.GetName(typeof(Levels), Level).ToString().ToUpper(), fileName, lineNumber, finalMessage));
#else
                    fs.WriteLine(String.Format("{0}  [{1}] {2}", DateTime.Now, Enum.GetName(typeof(Levels), Level).ToString().ToUpper(), finalMessage));
#endif
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
