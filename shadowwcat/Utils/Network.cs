﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;

namespace shadowwcat.Utils
{
    /// <summary>
    /// General purpose routines for validating network related items, such as IP addresses.
    /// </summary>
    internal static class Network
    {
        internal static bool IsValidIPFormat(string ip)
        {
            IPAddress foo;
            IPAddress.TryParse(ip, out foo);
            return (foo != null);
        }
        internal static bool IsPingable(string ip)
        {
            int numWorked = 0;
            bool[] pings = {false, false, false, false, false, false, false, false};

            Ping pinger = new Ping();
            for (var i = 0; i < pings.Length; i++)
            {
                PingReply reply = pinger.Send(ip, 1000);
                pings[i] = (reply.Status == IPStatus.Success);
            }
            numWorked = pings.Count(e => e == true);
            return (numWorked > (pings.Length/2));
        }
    }
}
