﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using shadowwcat.Data;
using tar_cs;
using System.IO;

namespace shadowwcat.Utils
{
    /// <summary>
    /// Routines for retrieving and comparing version attributes for appliance firmware, Environment Server, and Shadoww Cat itself.
    /// </summary>
    internal static class VersionOps
    {
        internal enum VersionedItem
        {
            ES_Server,
            FirmwareImage,
            ShadowwCatInstaller,
            RunningAppliance
        }
        internal enum FirmwareInfoItem
        {
            installer_min_version,
            environmental_min_version,
            version
        }
        internal static Dictionary<string, string> fwinfo = new Dictionary<string, string>()
        { 
            {"installer_min_version", ""},
            {"environmental_min_version", ""},
            {"version", ""}
        };

        internal static string FirmwareVersion
        {
            get { return fwinfo["version"]; }
        }
        internal static string FirmwareInstallerMinVersion
        {
            get { return fwinfo["installer_min_version"]; }
        }
        internal static string FirmwareESMinVersion
        {
            get { return fwinfo["environmental_min_version"]; }
        }
        internal static string ErrorMessage { get; private set; }

        internal static bool DoesFWVersionMatch(string FirmwareFile, string VersionToCompare)
        {
            bool status = false;
            ErrorMessage = string.Empty;

            //Clear all first
            fwinfo["installer_min_version"] = string.Empty;
            fwinfo["environmental_min_version"] = string.Empty;
            fwinfo["version"] = string.Empty;

            getFirmwareVersionfromFile(FirmwareFile);

            if (string.IsNullOrEmpty(fwinfo["version"]))
            {
                status = false;
                ErrorMessage = "No version information found in file.";
            }
            else
            {
                if (fwinfo["version"] == VersionToCompare)
                {
                    status = true;
                }
                else
                {
                    status = false;
                    ErrorMessage = string.Format("Firmware version of selected file ({0}) does not match {1}.", fwinfo["version"], VersionToCompare);
                }
            }
            return status;
        }
        internal static bool IsFWVersionCompatible(string FirmwareFile)
        {
            int MajorMin = 0;
            int MinorMin = 0;
            int MajorCompare = 0;
            int MinorCompare = 0;
            bool status = false;
            bool compare = false;
            ErrorMessage = string.Empty;

            //Clear all first
            fwinfo["installer_min_version"] = string.Empty;
            fwinfo["environmental_min_version"] = string.Empty;
            fwinfo["version"] = string.Empty;

            getFirmwareVersionfromFile(FirmwareFile);

            if (string.IsNullOrEmpty(fwinfo["version"]))
            {
                status = false;
                ErrorMessage = "No version information found in file.";
            }
            else
            {
                //Compare against ES minimum
                string ESVersion = AppSession.EnvironmentalServerVersion;

                int.TryParse(fwinfo["environmental_min_version"].Substring(0, 1), out MajorMin);
                int.TryParse(fwinfo["environmental_min_version"].Substring(2, 1), out MinorMin);

                int.TryParse(ESVersion.Substring(0, 1), out MajorCompare);
                int.TryParse(ESVersion.Substring(2, 1), out MinorCompare);

                compare = ((MajorCompare >= MajorMin) && (MinorCompare >= MinorMin));
                if (!compare)
                {
                    ErrorMessage = string.Format("This firmware requires a later version of Environmental Server (version {0} or higher).", fwinfo["environmental_min_version"]);
                }

                if (compare)
                {
                    //Compare against installer version
                    string InstallerVersion = AppSession.ShadowwCatVersion;

                    int.TryParse(fwinfo["installer_min_version"].Substring(0, 1), out MajorMin);
                    int.TryParse(fwinfo["installer_min_version"].Substring(2, 1), out MinorMin);

                    int.TryParse(InstallerVersion.Substring(0, 1), out MajorCompare);
                    int.TryParse(InstallerVersion.Substring(2, 1), out MinorCompare);

                    compare = ((MajorCompare >= MajorMin) && (MinorCompare >= MinorMin));
                    if (!compare)
                    {
                        ErrorMessage = string.Format("This firmware requires a later version of the Cluster Control tool (version {0} or higher).", fwinfo["installer_min_version"]);
                    }
                }

                status = compare;
            }
            return status;
        }

        private static void getFirmwareVersionfromFile(string FirmwareFile)
        {
            string version = string.Empty;
            string manifestFile = string.Empty;
            string trimmedValue = string.Empty;
            char[] charsToTrim = { '"', '\\' };

            if (File.Exists(FirmwareFile))
            {
                using (var examiner = File.OpenRead(FirmwareFile))
                {
                    TarReader tar = new TarReader(examiner);
                    while (tar.MoveNext(true))
                    {
                        if (tar.FileInfo.FileName == "./manifest")
                        {
                            manifestFile = Path.Combine(AppSession.CommonDataPath, tar.FileInfo.FileName);
                            using (FileStream file = File.Create(manifestFile))
                            {
                                tar.Read(file);
                            }
                            break;
                        }
                    }
                    if (File.Exists(manifestFile))
                    {
                        //parse the version info
                        Logger.Log(Logger.Levels.Info, string.Format("Parsing firmware image's manifest file '{0}'.", manifestFile));
                        string[] lines = File.ReadAllLines(manifestFile);
                        foreach (string aLine in lines)
                        {
                            aLine.Trim();
                            if (aLine.IndexOf('#') == 0) //Throw out all commented lines.
                            {
                                continue;
                            }
                            else if (aLine.Contains("="))
                            {
                                var key = aLine.Substring(0, aLine.IndexOf('='));
                                var value = aLine.Substring(aLine.IndexOf('=') + 1);
                                if (!String.IsNullOrEmpty(value))
                                {
                                    trimmedValue = value.Trim(charsToTrim);
                                }
                                if (key == "installer_min_version")
                                {
                                    fwinfo[key] = trimmedValue;
                                }
                                else if (key == "environmental_min_version")
                                {
                                    fwinfo[key] = trimmedValue;
                                }
                                else if (key == "version")
                                {
                                    fwinfo[key] = trimmedValue;
                                }
                            }
                        }
                        Logger.Log(Logger.Levels.Info, string.Format("Version info found: installer_min_version:{0}, environmental_min_version:{1}, version:{2}.",
                            fwinfo["installer_min_version"], fwinfo["environmental_min_version"], fwinfo["version"]));
                        File.Delete(manifestFile);
                    }
                }
            }
        }
    }
}
