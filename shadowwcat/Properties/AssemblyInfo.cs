﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Net;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ShadowwCat")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("SynapSense Corporation")]
[assembly: AssemblyProduct("ShadowwCat")]
[assembly: AssemblyCopyright("Copyright © SynapSense, a Panduit Company 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a50b53b8-95f6-45b8-99aa-b7287395c2e7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//IMPORTANT: The attribute below overrides the versions above for constructing both Application.ProductVersion and Application.CommonAppDataPath.
//Both values might be tied to upgrade behaviors, so use caution when changing this number. For example, using the setting
//below, Application.CommonAppDataPath would be C:\ProgramData\SynapSense Corporation\shadowwcat\1.0. The cluster config file resides in this location,
//so changing the setting could cause SHADOWWCAT to not find an existing cluster config file.
[assembly: AssemblyInformationalVersion("1.0")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif