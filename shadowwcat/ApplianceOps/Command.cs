﻿using Renci.SshNet;
using Renci.SshNet.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using shadowwcat.Data;
using System.Security.Cryptography;
using shadowwcat.Utils;

namespace shadowwcat.ApplianceOps
{
    /// <summary>
    /// Issue one or more commands to a control appliance. All file transfers use SCP, remote commands use SSH.
    /// The behavior of all of these commands is described in the control_appliance design spec..
    /// </summary>
    class Command
    {
        private static readonly bool IgnoreConnectFailures = true;

        //init related.
        internal static readonly string RemoteConfigFilename = @"/var/tmp/cluster_config.rc";

        internal static readonly string CmdValidateConfiguration = @"/etc/appliance/validate_config.sh {0} {1}";
        internal static readonly string CmdTestConfiguration = @"/etc/appliance/test_config.sh {0} {1}";
        internal static readonly string CmdGenerateConfiguration = @"/etc/appliance/gen_config.sh {0} {1}";
        //upgrade related.
        internal static readonly string CmdApplyUpgrade = @"/etc/appliance/apply_upgrade.sh {0}";
        internal static readonly string CmdPauseESSync = @"/etc/appliance/shadoww_control.sh pause_es_sync";
        internal static readonly string CmdResumeESSync = @"/etc/appliance/shadoww_control.sh resume_es_sync";
        internal static readonly string CmdPauseClustering = @"/etc/appliance/shadoww_control.sh pause_clustering {0} {1}";
        internal static readonly string CmdResumeClustering = @"/etc/appliance/shadoww_control.sh resume_clustering {0} {1}";
        internal static readonly string CmdProfileIsolated = @"/etc/appliance/shadoww_control.sh profile isolated";
        internal static readonly string CmdProfileClustered = @"/etc/appliance/shadoww_control.sh profile clustered";
        internal static readonly string CmdStopShadoww = @"/etc/appliance/shadoww_control.sh stop";
        internal static readonly string CmdApplianceHealth = @"/etc/appliance/health.sh";
        internal static readonly string CmdChangePassword = @"password";
        internal static readonly string CmdSavePassword = @"/etc/appliance/shadoww_control.sh save_passwd";

        //status
        internal static readonly string CmdShadowwRunStatus = @"service shadoww rcvar";
        internal static readonly string CmdClusterStable = @"/etc/appliance/shadoww_control.sh cluster_stable";

        //misc
        internal static readonly string CmdGetRunningVersion = @"cat /etc/appliance/ver";

        //upgrade/firmware related.
        internal static readonly string RemoteFirmwareFilename = @"/logs/firmware.update";

        //connect vars
        internal static readonly string Username = "shadoww"; //ssh always authenticates on this user.
        internal static readonly int SSHPort = 22;
        internal static readonly int TimeoutInSecs = 60; //ssh connect timeout.
        internal static readonly string SSHPasswordPrompt = "Password"; //this is part of what sshd presents for login. See GetConnectionInfo().

        #region PropertiesReturnedToCaller

        //These properties are set by all public (internal) methods.
        internal string FormattedErrorMessage { get; private set; }
        internal bool ErrorOccurred { get; private set; }
        internal BaseClient baseClient { get; private set; }

        #endregion

        internal string CommandResults { get; private set; }
        internal string CommandData { get; private set; }

        #region PropertiesSetByCaller

        //Most public methods use one or more of these properties. Caller must set them prior to method invocation.
        internal string ApplianceHostname { get; set; }
        internal string SiblingHostname { get; set; }
        internal string ApplianceIPAddress { get; set; }
        internal string AppliancePassword { get; set; }
        internal string FirmwareImageFilename { get; set; }
        internal Action MethodToRun { get; set; }

        #endregion

        /// <summary>
        /// Use to run any other method in this class. This indirection is needed in order to use background worker threads in the UI so that the GUI is responsive
        /// while the method is running. A background worker object cannot run a parameterized method; hence, the caller sets the method to be invoked by setting
        /// the MethodToRun property, then invokes the Run method.
        /// </summary>
        internal void Run()
        {
            MethodToRun();
        }

        // All of the methods below with 'internal' access are user callable, either directly or via the Run method. Note that all methods
        // require that one or more public properties are set prior to invocation. These properties include ApplianceHostname, AppliancePassword, etc..
        // Refer to the control_appliance design spec. for descriptions of what each of these commands is doing.
        internal void TestConnection()
        {
            ConnectTest();
        }
        internal void CopyClusterConfigurationFile()
        {
            RemoteCopy(AppSession.ClusterConfig.ClusterConfigFilename, RemoteConfigFilename);
        }

        internal void CopyFirmwareImage()
        {
            RemoteCopy(FirmwareImageFilename, RemoteFirmwareFilename);
        }

        internal void ValidateApplianceConfiguration()
        {
            RemoteCommand(String.Format(CmdValidateConfiguration, ApplianceHostname, RemoteConfigFilename));
        }

        internal void TestApplianceConfiguration()
        {
            RemoteCommand(String.Format(CmdTestConfiguration, ApplianceHostname, RemoteConfigFilename));
        }
        internal void GenerateApplianceConfiguration()
        {
            RemoteCommand(String.Format(CmdGenerateConfiguration, ApplianceHostname, RemoteConfigFilename));
        }
        internal void ApplianceHealth()
        {
            RemoteCommand(CmdApplianceHealth);
        }
        internal void ApplyFirmwareUpgrade()
        {
            RemoteCommand(string.Format(CmdApplyUpgrade, RemoteFirmwareFilename));
        }
        internal void CommandClusterStable()
        {
            RemoteCommand(CmdClusterStable);
        }
        internal void CommandPauseESSync()
        {
            RemoteCommand(CmdPauseESSync);
        }
        internal void CommandResumeESSync()
        {
            RemoteCommand(CmdResumeESSync);
        }
        
        internal void CommandPauseClustering()
        {
            RemoteCommand(String.Format(CmdPauseClustering, ApplianceHostname, SiblingHostname));
        }
        internal void CommandResumeClustering()
        {
            RemoteCommand(String.Format(CmdResumeClustering, ApplianceHostname, SiblingHostname));
        }
        internal void CommandProfileIsolated()
        {
            RemoteCommand(CmdProfileIsolated);
        }
        internal void CommandProfileClustered()
        {
            RemoteCommand(CmdProfileClustered);
        }
        internal void CommandStopShadoww()
        {
            RemoteCommand(CmdStopShadoww);
        }
        internal void GetRunningVersion()
        {
            RemoteCommand(CmdGetRunningVersion);
        }
        internal void GetShadowwRunStatus()
        {
            RemoteCommand(CmdShadowwRunStatus);
        }

        internal void GetVersionAndRunStatus()
        {
            RemoteCommand(new string[] { CmdGetRunningVersion, CmdShadowwRunStatus });
        }
        
        internal void WaitForApplianceRestart()
        {
            int numRetries = 12;
            int waitInterval = 30; //seconds
            int i = 1;
            ErrorOccurred = false;

            CommandResults = string.Format("Waiting for appliance '{0}' to reboot. (trying reconnect every {1} seconds and ignoring errors until retry limit of {2} is reached.)...",
                ApplianceHostname, waitInterval, numRetries);

            Logger.Log(Logger.Levels.Info, CommandResults);
            while (i++ <= numRetries)
            {
                Thread.Sleep(waitInterval * 1000);
                this.ConnectTest(IgnoreConnectFailures);
                if (!ErrorOccurred)
                {
                    break;
                }                
            }
            if (ErrorOccurred)
            {
                CommandResults = string.Format("{0} ({1}): {2}", MapErrorCode(-1), -1, "Timeout exceeded waiting for appliance to reboot.");
            }
            else
            {
                CommandResults = string.Format("{0} ({1}): {2}", MapErrorCode(0), 0, CommandResults);
            }
            //wait an additional ten seconds per design spec.
            Thread.Sleep(10 * 1000);
        }


        /// <summary>
        /// Perform a SCP file copy.
        /// This method does not use a persistent connection.
        /// </summary>
        /// <param name="SrcFile"></param>
        /// <param name="DestFile"></param>
        private void RemoteCopy(string SrcFile, string DestFile)
        {
            bool Connected = false;
            int ReturnCode = 0;
            CommandResults = String.Empty;
            ErrorOccurred = true;
            baseClient = null;

            var connectionInfo = GetConnectionInfo();
            
            using (var client = new ScpClient(connectionInfo))
            {                
                ErrorOccurred = false;
                try
                {
                    Logger.Log(Logger.Levels.Info, string.Format("====SCP copy of {0} to {1}====", SrcFile, DestFile));
                    Thread.Sleep(5000);
                    client.Connect();
                    baseClient = client;
                    Connected = true;

                    client.Upload(new FileInfo(SrcFile), DestFile);
                    CommandResults = string.Format("{0} ({1}): SCP copy of {2} to {3}.", MapErrorCode(ReturnCode), ReturnCode, SrcFile, DestFile);
                }
                catch (Exception e)
                {
                    ReturnCode = -1;
                    CommandResults = string.Format("{0} ({1}): {2}", MapErrorCode(ReturnCode), ReturnCode, e.Message);
                    ErrorOccurred = true;
                }
                finally
                {
                    if (Connected)
                    {
                        client.Disconnect();
                        Thread.Sleep(5000);
                    }
                }
            }
            if (ErrorOccurred)
            {
                Logger.Log(Logger.Levels.Error, CommandResults);
            }
            else
            {
                Logger.Log(Logger.Levels.Info, CommandResults);
            }
        }

        internal void ChangePassword(string oldPassword, string newPassword)
        {
            int ReturnCode = 0;
            string RunningResults = String.Empty;
            bool Connected = false;
            baseClient = null;
            ConnectionInfo conn = GetConnectionInfo();

            using (var client = new SshClient(conn))
            {
                try {                    
                    client.Connect();
                    baseClient = client;

                    using (var stream = client.CreateShellStream("dumb", 80, 24, 800, 600, 1024))
                    {
                        string line = "";
                        stream.Flush();
                        stream.WriteLine("passwd");
                        stream.Flush();
                        Thread.Sleep(1000);
                        line = stream.ReadLine();
                        Thread.Sleep(1000);
                         line = stream.ReadLine();
                        stream.WriteLine(oldPassword); // old password
                        stream.Flush();
                        Thread.Sleep(1000);
                         line = stream.ReadLine();
                        stream.WriteLine(newPassword); // new  password                    
                        stream.Flush();
                        Thread.Sleep(1000);
                        line = stream.ReadLine();
                        stream.WriteLine(newPassword); // new  password again
                        stream.Flush();
                        Thread.Sleep(1000);
                        line = stream.ReadLine();                    
                    }

                }
                catch (Exception e)
                {
                    ReturnCode = -1;
                    CommandResults = string.Format("{0} ({1}): {2}\r\n{3}", MapErrorCode(ReturnCode), ReturnCode, e.Message, RunningResults);
                    ErrorOccurred = true;
                }
                finally
                {
                    ReturnCode = -1;
                    if (Connected)
                    {
                        client.Disconnect();
                        Thread.Sleep(5000);
                    }
                }

                // if change password failed, do not do step 2. 
                if (ErrorOccurred)
                {
                    Logger.Log(Logger.Levels.Error, CommandResults);
                    return;
                }
                else
                {
                    CommandResults = string.Format("{0} ({1}): {2}", MapErrorCode(ReturnCode), ReturnCode, RunningResults);
                    Logger.Log(Logger.Levels.Info, CommandResults);
                }
            }

            // step 2: /etc/appliance/shadoww_control.sh save_passwd
            this.AppliancePassword = newPassword;
            string saveCommandResults = CommandResults;
            RemoteCommand(CmdSavePassword);
            
            if (!ErrorOccurred)
            {
                if (AppSession.RememberAppliancePasswordForSession)
                {
                    if ((conn.Host == AppSession.ClusterConfig.Appliance_1_Hostname) ||
                    (conn.Host == AppSession.ClusterConfig.Appliance_1_Port1_IP) ||
                    (conn.Host == AppSession.ClusterConfig.Appliance_1_Port2_IP))
                    {
                        AppSession.Appliance1Password = newPassword;
                    }
                    else if ((conn.Host == AppSession.ClusterConfig.Appliance_2_Hostname) ||
                      (conn.Host == AppSession.ClusterConfig.Appliance_2_Port1_IP) ||
                      (conn.Host == AppSession.ClusterConfig.Appliance_2_Port2_IP))
                    {
                        AppSession.Appliance2Password = newPassword;
                    }
                }

            } else
            {
                saveCommandResults = " Could not save changed password on appliance. Please re-run the" +
                    " change password command with the new password or Change the password on the appliance using the serial port.";
            }

            CommandResults = saveCommandResults + "\r\n" + CommandResults;

        }
        /// <summary>
        /// Execute a single comand via ssh and return the command's output text. It is up to the caller to parse the text.
        /// </summary>
        /// <param name="Command"></param>
        /// <returns></returns>
        private void RemoteCommand(string Command)
        {
            Logger.Log(Logger.Levels.Info, "RemoteCommand: " + Command);
            string[] SingleCommand = new string[1] { Command };
            this.RemoteCommand(SingleCommand);
        }
        /// <summary>
        /// Execute one or more commands via ssh and return the command's output text. It is up to the caller to parse the text.
        /// </summary>
        /// <param name="Commands"></param>
        /// <returns></returns>
        private void RemoteCommand(string[] Commands)
        {
            int ReturnCode = 0;
            string RunningResults = String.Empty;
            bool Connected = false;
            CommandResults = string.Empty;
            CommandData = string.Empty;
            ErrorOccurred = true;

            var connectionInfo = GetConnectionInfo();
            baseClient = null;
            using (var client = new SshClient(connectionInfo))
            {
                ErrorOccurred = false;

                try
                {
                    Thread.Sleep(5000);
                    client.Connect();
                    baseClient = client;
                    Connected = true;

                    foreach (var c in Commands)
                    {
                        Logger.Log(Logger.Levels.Info, string.Format("====Running '{0}'====", c));
                        var command = client.RunCommand(c);
                        Thread.Sleep(2000);
                        ReturnCode = command.ExitStatus;
                        RunningResults += string.Format("Command: {0}. Result: {1} ({2}) - '{3}'\r\n", c, MapErrorCode(ReturnCode), ReturnCode, command.Result.Replace("\n", "\r\n"));
                        ErrorOccurred = (ReturnCode != 0);
                        if (ErrorOccurred)
                        {
                            CommandResults = RunningResults;
                            break;
                        }
                        else
                        {
                            Logger.Log(Logger.Levels.Info, "Command output = " + command.Result);
                            CommandData += command.Result + "|";
                        }
                    }
                    CommandData.Replace("\n", "\r\n");
                }
                catch (Exception e)
                {
                    ReturnCode = -1;
                    CommandResults = string.Format("{0} ({1}): {2}\r\n{3}", MapErrorCode(ReturnCode), ReturnCode, e.Message, RunningResults);
                    ErrorOccurred = true;
                }
                finally
                {
                    if (Connected)
                    {
                        client.Disconnect();
                        Thread.Sleep(5000);
                    }
                }
            }
            if (ErrorOccurred)
            {
                Logger.Log(Logger.Levels.Error, CommandResults);
            }
            else
            {
                CommandResults = string.Format("{0} ({1}): {2}", MapErrorCode(ReturnCode), ReturnCode, RunningResults);
                Logger.Log(Logger.Levels.Info, CommandResults);
            }
        }

        

        private void ConnectTest(bool IgnoreFail = false)
        {
            int ReturnCode = 0;
            string AddedErrorInfo = String.Empty;
            bool Connected = false;
            FormattedErrorMessage = String.Empty;
            ErrorOccurred = true;

            var connectionInfo = GetConnectionInfo();
            
            CommandResults = string.Format("Trying ssh connection to {0}.", ApplianceHostname);
            Logger.Log(Logger.Levels.Info, CommandResults);            

            using (var client = new SshClient(connectionInfo))
            {
                ErrorOccurred = false;
                try
                {
                    client.Connect();
                    baseClient = client;
                    Connected = true;
                    ErrorOccurred = false;
                    CommandResults = string.Format("{0} ({1}): {2} -- Successful.", MapErrorCode(ReturnCode), ReturnCode, CommandResults);
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    ReturnCode = -1;
                    if (e.Message.Contains("Permission denied (key"))
                    {
                        ReturnCode = -2;
                        AddedErrorInfo = "Invalid password?";
                    }
                    CommandResults = string.Format("{0} ({1}): {2} {3} {4}", MapErrorCode(ReturnCode), ReturnCode, CommandResults, e.Message, AddedErrorInfo);
                    ErrorOccurred = true;
                }
                finally
                {
                    if (Connected)
                    {
                        client.Disconnect();
                        Thread.Sleep(3000);
                    }
                }
            }
            if (ErrorOccurred)
            {
                if (IgnoreFail)
                {
                    Logger.Log(Logger.Levels.Info, "Ignoring connect failure, waiting for appliance reboot to finish...");
                }
                else
                {
                    Logger.Log(Logger.Levels.Error, CommandResults);
                }
            }
            else
            {
                Logger.Log(Logger.Levels.Info, CommandResults);
            }
        }
        /// <summary>
        /// Map the error code returned by ssh.net calls to a displayable string.
        /// </summary>
        /// <param name="ErrorCode"></param>
        /// <returns></returns>
        private string MapErrorCode(int ErrorCode)
        {
            return (ErrorCode == 0 ? "OK" : "ERROR");
        }

        void HandleKeyEvent(Object sender, AuthenticationPromptEventArgs e)
        {
            foreach (AuthenticationPrompt prompt in e.Prompts)
            {
                if (prompt.Request.IndexOf("Password", StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    prompt.Response = AppliancePassword;
                }
            }
        }


        /// <summary>
        /// Wires up keyboard password authentication by responding to the SSHD password prompt.
        /// Note that SSHPasswordPrompt must partially match the "password prompt string" presented by SSHD. Otherwise,
        /// logins will fail.
        /// </summary>
        /// <returns></returns>
        private ConnectionInfo GetConnectionInfo()
        {
            KeyboardInteractiveAuthenticationMethod handler =  new KeyboardInteractiveAuthenticationMethod(Username);
            handler.AuthenticationPrompt += new EventHandler<AuthenticationPromptEventArgs>(HandleKeyEvent);

            string system = string.IsNullOrEmpty(ApplianceIPAddress) ? ApplianceHostname : ApplianceIPAddress;
            var connectionInfo = new KeyboardInteractiveConnectionInfo(system, SSHPort, Username, handler);
            connectionInfo.Timeout = TimeSpan.FromSeconds(TimeoutInSecs);            
          
            return connectionInfo;
        }

        public void cancelCommand()
        {
            try
            {
                if (baseClient != null)
                {
                    Logger.Log(Logger.Levels.Info, "Cancelling command..." + baseClient.ToString());
                    baseClient.Disconnect();
                }
            }
            catch (Exception e)
            {
                Logger.Log(Logger.Levels.Error, e.Message);
            }            
        }
    }
}
