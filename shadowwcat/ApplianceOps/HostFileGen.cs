﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using shadowwcat.Data;
using System.IO;
using shadowwcat.Utils;

namespace shadowwcat.ApplianceOps
{
    internal static class HostFileGen
    {
        private static string Marker1 = "#### Added by SynapSense Active Control";
        private static string Marker2 = "#### Do not modify these SynapSense comment lines.";
        private static string SystemDirectory = Environment.SystemDirectory;
        private static string HostsDirectory = Path.Combine(SystemDirectory, @"drivers\etc");
        internal static string HostsFile = Path.Combine(HostsDirectory, "hosts");
        private static string BackupHostsFile = Path.Combine(HostsDirectory, "hosts.backedUpBySynapSense");

        internal static string ErrorMessage { get; private set; }
        internal static string NewHostsFile { get; private set; }
        
        /// <summary>
        /// Replaces the existing system hosts file with NewHostsFile.
        /// </summary>
        /// <returns></returns>
        internal static bool CommitHostsFileChanges()
        {
            bool worked = false;
            ErrorMessage = string.Empty;
            StringBuilder sb = new StringBuilder();
            try
            {
                BackUpExistingHostsFile();
                ReplaceHostsFile();
                worked = true;
            }
            catch (Exception e)
            {
                sb.AppendFormat("Could not add control appliance entries to the current system hosts file ({0}).\r\n\r\n", HostsFile);
                sb.Append("Typical causes include anti-virus software protection and file/directory permissions. You will either have to add the control appliance entries manually,");
                sb.AppendFormat(" or replace the current hosts file with the generated one ({0}).\r\n\r\n", NewHostsFile);
                sb.AppendFormat("Error details: {0}", e.Message);
                ErrorMessage = sb.ToString();
                Logger.Log(Logger.Levels.Error, ErrorMessage);
            }
            return worked;
        }
        /// <summary>
        /// Creates a new (staged) hosts file containing entries for the control appliances. The file is accessible via the NewHostsFile property for previewing, etc..
        /// </summary>
        /// <param name="Host1">The user supplied host name for appliance 1</param>
        /// <param name="IP1">The user supplied IPv4 address for appliance 1. Note that if DNS was selected, we wouldn't be utilizing this class.</param>
        /// <param name="Host2">Ditto for appliance 2.</param>
        /// <param name="IP2">Ditto for appliance 2.</param>
        /// <returns></returns>
        internal static bool CreateUpdatedHostsFile()
        {
            NewHostsFile = Path.Combine(HostsDirectory, "hosts.newSynapSense");
            ErrorMessage = string.Empty;
            StringBuilder sb = new StringBuilder();
            string IpEntry1 = string.Format("{0}\t{1}", AppSession.ClusterConfig.Appliance_1_Port1_IP, AppSession.ClusterConfig.Appliance_1_Hostname);
            string IpEntry2 = string.Format("{0}\t{1}", AppSession.ClusterConfig.Appliance_2_Port1_IP, AppSession.ClusterConfig.Appliance_2_Hostname);
            bool worked = false;

            if (File.Exists(NewHostsFile))
            {
                File.Delete(NewHostsFile);
            }
            Logger.Log(Logger.Levels.Info, "Creating new staged hosts file named " + NewHostsFile);
            try
            {
                AddEntries(NewHostsFile, IpEntry1, IpEntry2);
                worked = true;
            }
            catch (Exception e)
            {
                sb.AppendFormat("Could not create a pre-staged replacement file named {0} for the current system hosts file.\r\n\r\n", NewHostsFile);
                sb.Append("Typical causes include anti-virus software protection and insufficient file/directory permissions. You will have to manually add control appliance entries");
                sb.Append(" to the current hosts file.\r\n\r\n");
                sb.Append("The new entries are:\r\n");
                sb.AppendFormat("\t{0}\r\n", Marker1);
                sb.AppendFormat("\t{0}\r\n", IpEntry1);
                sb.AppendFormat("\t{0}\r\n", IpEntry2);
                sb.AppendFormat("\t{0}\r\n\r\n", Marker2);
                sb.AppendFormat("Error details: {0}", e.Message);
                ErrorMessage = sb.ToString();
                Logger.Log(Logger.Levels.Error, ErrorMessage);
            }
            return worked;
        }
        /// <summary>
        /// Determine if the live hosts file already contains the requested entries.
        /// </summary>
        /// <returns></returns>
        internal static bool IsHostsUpToDate()
        {
            string IpEntry1 = string.Format("{0}\t{1}", AppSession.ClusterConfig.Appliance_1_Port1_IP, AppSession.ClusterConfig.Appliance_1_Hostname);
            string IpEntry2 = string.Format("{0}\t{1}", AppSession.ClusterConfig.Appliance_2_Port1_IP, AppSession.ClusterConfig.Appliance_2_Hostname);

            return EntriesMatch(IpEntry1, IpEntry2);
        }
        private static void BackUpExistingHostsFile()
        {
            if (!File.Exists(BackupHostsFile))
            {
                File.Copy(HostsFile, BackupHostsFile);
            }
        }
        private static void ReplaceHostsFile()
        {
            try
            {
                File.Delete(HostsFile);
                File.Move(NewHostsFile, HostsFile);
            }
            catch (Exception fileExc)
            {
                if (!File.Exists(HostsFile))
                {
                    Logger.Log(Logger.Levels.Warning, "Could not modify/replace current system hosts file. Attempt to restore system hosts file with backup.");
                    File.Copy(BackupHostsFile, HostsFile);
                }
                throw new Exception(fileExc.Message);
            }
        }
        private static void AddEntries(string hostsFile, string NewEntry1, string NewEntry2)
        {
            bool SkipLine = false;

            string[] lines = File.ReadAllLines(HostsFile);
            using (StreamWriter fs = new StreamWriter(hostsFile))
            {
                foreach (string aLine in lines)
                {
                    if (aLine.Contains(Marker1)) //faucet off
                    {
                        SkipLine = true;
                    }
                    if (!SkipLine)
                    {
                        fs.WriteLine(aLine);
                    }
                    if (aLine.Contains(Marker2)) //faucet back on
                    {
                        SkipLine = false;
                    }
                }
                fs.WriteLine(Marker1);
                fs.WriteLine(NewEntry1);
                fs.WriteLine(NewEntry2);
                fs.WriteLine(Marker2);
            }
        }
        /// <summary>
        /// Are these entries already present in the system hosts file? Remove all whitespace before comparing, just in case user added entries manually.
        /// </summary>
        /// <param name="NewEntry1"></param>
        /// <param name="NewEntry2"></param>
        /// <returns></returns>
        private static bool EntriesMatch(string NewEntry1, string NewEntry2)
        {
            bool EntriesExist = false;
            string OldSection = string.Empty;
            string NewSection = string.Format("{0}{1}{2}{3}", Marker1, NewEntry1, NewEntry2, Marker2).Replace("\t", "").Replace("\r\n", "").Replace(" ", "");

            string[] lines = File.ReadAllLines(HostsFile);
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(Marker1) && i < (lines.Length-3))
                {
                    OldSection = string.Format("{0}{1}{2}{3}", lines[i], lines[i + 1], lines[i + 2], lines[i + 3]).Replace("\t", "").Replace("\r\n", "").Replace(" ", "");
                    EntriesExist = OldSection == NewSection;
                }
            }
            if (EntriesExist)
            {
                Logger.Log(Logger.Levels.Info, HostsFile + " already has correct entries. No update needed.");
            }
            return EntriesExist;
        }
    }
}
